package de.tudresden.inf.st.hybridpps.starter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.tudresden.inf.st.hybridpps.extend.ExtensionProcess;
import de.tudresden.inf.st.hybridpps.jastadd.model.ConcreteSolution;
import de.tudresden.inf.st.hybridpps.jastadd.model.DeterministicSolution;
import de.tudresden.inf.st.hybridpps.jastadd.model.Mapping;
import de.tudresden.inf.st.hybridpps.jastadd.model.StochasticSolution;
import de.tudresden.inf.st.hybridpps.parser.data.MultiMapping;
import de.tudresden.inf.st.hybridpps.parser.data.StochasticMultiMapping;
import de.tudresden.inf.st.hybridpps.simulation.SolutionGenerator;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.CombinedPriorityRule;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.MetaRuleScheduling;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.SchedulingStrategy;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.SimplePriorityRule;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.SimplePriorityRulePool;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.parameters.CPRLoader;
import de.tudresden.inf.st.hybridpps.solutionjson.DataSolution;
import de.tudresden.inf.st.hybridpps.solutionjson.DataStrategyInstance;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import picocli.CommandLine;
import picocli.CommandLine.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.Callable;

/**
 * Starts the simulator. Needs a path to a production system to work. The priority rule to use can be customized.
 *
 * @author Erik Schönherr
 */
@SuppressWarnings("unused")
class HybridPPSStarter implements Callable<Integer> {
	private static final Logger logger = LogManager.getLogger(HybridPPSStarter.class);

	@Parameters(description = "path to a .json file defining a production system or a directory with such .json files")
	private String psPath;
	
	@Option(names = {"-o", "--out"}, description = "define the path to the output directory")
	private String outputPath = "";

	@ArgGroup(multiplicity = "1")
	Composite composite;
	static class Composite {
		@ArgGroup(exclusive = false)
		SimulationArguments simulationArguments;

		@Option(names = {"--extend"}, description = "if set, perform only problem extension")
		private boolean extend;
	}

	static class SimulationArguments {
		@Option(names = {"-s", "--spr"}, description = "define which simple priority rule to use for scheduling")
		private String spr = "fifo";

		@Option(names = {"-c", "--cpr"}, description = "define the path to a parameter file or a directory containing parameter files if you want to use a combined priority rule for scheduling")
		private File cpr;

		@Option(names = {"-m", "--metarules"}, description = "define the path to a JSON file containing a meta-ruleset. If given, the use of the MetaRuleSchedulingStrategy is assumed.")
		private String metarules;

		@Option(names = {"-d", "--deterministic"}, description = "if set, use deterministic simulation")
		boolean deterministic;

		@Option(names = {"-r", "--report"}, description = "if set, create the desmo-j report for each run")
		boolean report;

		@Option(names = {"-v", "--verbose"}, description = "if set, show info and debug level log messages")
		boolean verbose;

		@Option(names = {"-p", "--print"}, description = "print output mapping(s)")
		boolean print = false;

		@Option(names = {"-e", "--persist-model"}, description = "Persist extended model")
		boolean persistModel = false;
	}

	/**
	 * Start point of the application. Proceses the command line arguments and exits with the exit code given from the
	 * simulations.
	 *
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		int exitCode = new CommandLine(new HybridPPSStarter()).execute(args);
		System.exit(exitCode);
	}

	/**
	 * This method evaluates the command line arguments and then executes the simulation. Finally it writes the solution
	 * file.
	 *
	 * @return exit code of the program
	 */
	@Override
	public Integer call() throws IOException {
		final String jsonFilename;

		File psFile = Paths.get(psPath).toFile();
		System.out.println(psFile.getAbsoluteFile());
		File[] psFiles;
		if (psFile.isDirectory()) {
			psFiles = psFile.listFiles((dir, name) -> name.toLowerCase().endsWith(".json"));
		} else {
			psFiles = new File[]{psFile};
		}

		assert psFiles != null && psFiles.length > 0 :
				"No production system found with the given path! Aborting.";

		File outputDir = Paths.get(outputPath).toFile();
		assert outputDir.isDirectory() : "Output path has to point to a directory!";

		if (!composite.extend) {
			List<SchedulingStrategy> schedulingStrategies = new ArrayList<>();

			if (composite.simulationArguments.metarules != null) {
			/*
			schedulingStrategies contains all strategies that should be executed on the given problem.
			Here if a meta-ruleset is defined, we assume that we want to use the MetaRuleSchedulingStrategy.
			Therefore we create one and add it to the List.
			 */
				File metaRuleFile = Paths.get(composite.simulationArguments.metarules).toFile();
				System.out.println("metarules found " + metaRuleFile.exists());
				schedulingStrategies.add(new MetaRuleScheduling(metaRuleFile.toPath()));
			} else if (composite.simulationArguments.cpr != null) {
				File[] strategyFiles;
				if (composite.simulationArguments.cpr.isDirectory()) {
					strategyFiles = composite.simulationArguments.cpr.listFiles((dir, name) -> name.toLowerCase().
							endsWith(".json"));
				} else {
					strategyFiles = new File[]{composite.simulationArguments.cpr};
				}

				assert strategyFiles != null && strategyFiles.length > 0 :
						"No Scheduling strategy found with the given path! Aborting.";

				Arrays.stream(strategyFiles).forEach(f -> {
					try {
						schedulingStrategies.add(CPRLoader.load(f.toPath()));
					} catch (IOException e) {
						e.printStackTrace();
					}
				});
			} else {
				schedulingStrategies.add(SimplePriorityRulePool.getRule(composite.simulationArguments.spr));
			}

			List<DataSolution> solutions = new ArrayList<>();

			if (!composite.simulationArguments.verbose) {
				Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.WARN);
			}

			int currentTask = 0;
			int totalTasks = psFiles.length * schedulingStrategies.size();
			printProgress(currentTask, totalTasks);

			for (File instance : psFiles) {
				DataSolution solutionReport = new DataSolution();
				solutionReport.problem = instance.getName().split("\\.")[0];
				List<DataStrategyInstance> strategyInstances = new ArrayList<>();
				for (SchedulingStrategy strategy : schedulingStrategies) {
					DataStrategyInstance dsi;
					if (composite.simulationArguments.deterministic) {
						dsi = generateDeterministicSolution(instance, strategy, outputDir);
					} else {
						dsi = generateStochasticSolution(instance, strategy, outputDir);
					}
					strategyInstances.add(dsi);
					printProgress(++currentTask, totalTasks);
				}
				solutionReport.strategies = strategyInstances;
				solutions.add(solutionReport);
			}

			ObjectMapper mapper = new ObjectMapper();
			ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();

			DateTimeFormatter formatter = DateTimeFormatter
					.ofPattern("yyyy-MM-dd-HH-mm-ss")
					.withZone(ZoneId.systemDefault());
			String now = formatter.format(Instant.now());
			String type;

			if (composite.simulationArguments.deterministic) {
				type = "Deterministic";
			} else {
				type = "Stochastic";
			}

			writer.writeValue(outputDir.toPath().resolve("Solution-" + type + "-" + now + ".json").toFile(), solutions);
		} else {
			for (File instance : psFiles) {
				ExtensionProcess process = new ExtensionProcess();
				process.setPathToInputJson(instance.toPath());
				process.setOutputDirectory(outputDir.getPath());
				process.run();
			}
		}

		return 0;
	}

	/**
	 * Generate the solution file for the deterministic case.
	 *
	 * @param instance the problem instance
	 * @param strategy the {@link SchedulingStrategy} to use
	 * @return results of the simulation
	 */
	public DataStrategyInstance generateDeterministicSolution(File instance, SchedulingStrategy strategy, File outDir) throws IOException {
		DeterministicSolution deterministicSolution =
				SolutionGenerator.generateDeterministicSolution(instance.toPath(), strategy,
						composite.simulationArguments.persistModel, composite.simulationArguments.report);

		if(composite.simulationArguments.print){
			LinkedList<String> mappings = new LinkedList<>();
			for(Mapping m : deterministicSolution.getConcreteSolution().getMappingList()){
				mappings.add(m.extendedPrint());
			}
			MultiMapping mm = new MultiMapping();
			mm.mapping = mappings;
			ObjectMapper mapper = new ObjectMapper();
			ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();
			DateTimeFormatter formatter = DateTimeFormatter
					.ofPattern("yyyy-MM-dd-HH-mm-ss")
					.withZone(ZoneId.systemDefault());
			String now = formatter.format(Instant.now());
			writer.writeValue(outDir.toPath().resolve("Mapping-" + "-" + now + ".json").toFile(), mm);
		}
		/*
		 * getDataSolution is a function of AbstractSolution and defined in the base module.
		 * Here, the deterministic solution is transformed to its data representation (solution.json)
		 */
		return deterministicSolution.getDataSolution(generateStrategyName(strategy));
	}

	/**
	 * Generate the solution file for the stochastic case.
	 *
	 * @param instance the problem instance
	 * @param strategy the {@link SchedulingStrategy} to use
	 * @return results of the simulation
	 */
	public DataStrategyInstance generateStochasticSolution(File instance, SchedulingStrategy strategy, File outDir) throws IOException {
		StochasticSolution stochasticSolution =
				SolutionGenerator.generateStochasticSolution(instance.toPath(), strategy,
						composite.simulationArguments.persistModel, composite.simulationArguments.report);

		if(composite.simulationArguments.print) {
			StochasticMultiMapping smm = new StochasticMultiMapping();
			smm.stochasticMappings = new LinkedList<>();
			for (ConcreteSolution solution : stochasticSolution.getConcreteSolutions()) {
				LinkedList<String> mappings = new LinkedList<>();
				for (Mapping m : solution.getMappingList()) {
					mappings.add(m.extendedPrint());
				}
				MultiMapping mm = new MultiMapping();
				mm.mapping = mappings;
				smm.stochasticMappings.add(mm);
			}
			ObjectMapper mapper = new ObjectMapper();
			ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();
			DateTimeFormatter formatter = DateTimeFormatter
					.ofPattern("yyyy-MM-dd-HH-mm-ss")
					.withZone(ZoneId.systemDefault());
			String now = formatter.format(Instant.now());
			writer.writeValue(outDir.toPath().resolve("Mapping-" + "-" + now + ".json").toFile(), smm);
		}
		/*
		 * getDataSolution is a function of AbstractSolution and defined in the base module.
		 * Here, the stochastic solution is transformed to its data representation (solution.json)
		 */
		return stochasticSolution.getDataSolution(generateStrategyName(strategy));
	}

	/**
	 * Generates a name for a scheduling strategy.
	 *
	 * @param strategy the {@link SchedulingStrategy} used
	 * @return name for the scheduling strategy
	 */
	private String generateStrategyName(SchedulingStrategy strategy) {
		String name = null;
		if(strategy instanceof CombinedPriorityRule){
			name = "cpr";
		}else if(strategy instanceof MetaRuleScheduling){
			name = "metarule";
		}else{
			name = ((SimplePriorityRule) strategy).getName();
		}
		return name.split("\\.")[0];
	}

	/**
	 * Prints a progress bar showing how many of the simulations are finished. If --verbose is set or
	 * only one production systems is being looked at, the functionality of this method is skipped.
	 *
	 * @param tasksDone amount of finished simulations
	 * @param totalTasks amount of total simulations
	 */
	private void printProgress(int tasksDone, int totalTasks) {
		if (composite.simulationArguments.verbose || totalTasks <= 1) {
			return;
		}
		int maxBarSize = 20;
		float percent = (float) tasksDone / totalTasks;
		int numBars = Math.round(percent * maxBarSize);

		String icon = "=";

		StringBuilder bar = new StringBuilder();
		bar.append("[");
		for (int i = 0; i < maxBarSize; i++) {
			if (i < numBars) {
				bar.append(icon);
			} else if (i == numBars) {
				bar.append(">");
			} else {
				bar.append(" ");
			}
		}
		bar.append("]");
		System.out.print("\r" + bar + " " + Math.round(percent * 10000) / 100.0 + "% (" + tasksDone + "/" + totalTasks + ")");
		if (tasksDone == totalTasks) {
			System.out.print("\n");
		}
	}
}
