package de.tudresden.inf.st.hybridpps.extend;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudresden.inf.st.hybridpps.jastadd.model.ProductionSystem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testing ExtensionProcess.
 *
 * @author rschoene - Initial contribution
 */
public class ExtensionProcessTest {

  static final Path inputBase = Paths.get("src", "test", "resources", "input");
  static final Path outputBase = Paths.get("src", "test", "resources", "output");
  static Logger logger = LogManager.getLogger(ExtensionProcessTest.class);

  @ParameterizedTest
  @MethodSource("getInputJsonList")
  public void test(Path inputJson) {
    String testCaseName = inputJson.getParent().getFileName().toString();
    File expectedDir = inputJson.getParent().resolve("expected").toFile();
    assertTrue(expectedDir.exists() && expectedDir.isDirectory(),
        "No expected directory found at " + expectedDir);
    ObjectMapper mapper = new ObjectMapper();

    ExtensionProcess extensionProcess = new ExtensionProcess();
    extensionProcess.setPathToInputJson(inputJson);
    // Attention: Output path will have the name of the system as directory. need to be set in input.json
    extensionProcess.setOutputDirectory(outputBase.toString());
    extensionProcess.setMetaFileName("meta.json");
    extensionProcess.setAddTimestampForOutput(false);

    ExtensionProcess.Processor<Void> testProcessor = new ExtensionProcess.Processor<Void>() {

      @Override
      protected Void process(ProductionSystem productionSystem) throws Exception {
        File[] files = expectedDir.listFiles(file -> !file.isDirectory());
        if (files == null) {
          // maybe we should not fail here, if we expect the process to not generate files for some reason
          fail("No expected files");
        }
        for (File file : files) {
          Path actualFile = outputBase.resolve(productionSystem.getName()).resolve(file.getName());
          assertTrue(actualFile.toFile().exists(), "Corresponding file " + file + " not found in " + outputBase + " at " + actualFile);

          if (file.getName().equals("meta.json")) {
//            DataMetaFile expected = mapper.readValue(file, DataMetaFile.class);
//            DataMetaFile actual = mapper.readValue(actualFile.toFile(), DataMetaFile.class);
//            assertEquals(expected.horizon, actual.horizon);
//            assertEquals(expected.projects.size(), actual.projects.size());
//            for (int i = 0; i < expected.projects.size(); i++) {
//              assertEquals(expected.projects.get(i).name, actual.projects.get(i).name);
//              assertEquals(expected.projects.get(i).filename, actual.projects.get(i).filename);
//            }
            String expected = readFile(file.toPath());
            String actual = readFile(actualFile);
            assertEquals(expected, actual, "Content of meta.json in " + testCaseName + " differs");
          } else if (file.getName().endsWith(".emm")) {
            String expected = readFile(file.toPath());
            String actual = readFile(actualFile);
            assertEquals(expected, actual, "Content of " + file.getName() + " in " + testCaseName + " differs");
          } else {
            fail("Unknown file extension for " + file);
          }
        }
        return null;
      }
    };
    extensionProcess.addGeneralProcessor(testProcessor);

    boolean overallSuccess = extensionProcess.run();
    assertTrue(overallSuccess, "overallSuccess");
  }

  private String readFile(Path path) throws IOException {
    return Files.readAllLines(path).stream()
        .map(ExtensionProcessTest::rtrim)
        .collect(Collectors.joining("\n"));
  }

  public static String rtrim(String s) {
    int i = s.length()-1;
    while (i >= 0 && Character.isWhitespace(s.charAt(i))) {
      i--;
    }
    return s.substring(0,i+1);
  }

  public static Stream<Path> getInputJsonList() {
    File[] listOfFiles = inputBase.toFile().listFiles(File::isDirectory);
    if (listOfFiles == null) {
      return Stream.empty();
    }
    return Arrays.stream(listOfFiles)
        .sorted()
        .map((File f) -> f.toPath().resolve("input.json"))
        .filter((Path p) -> p.toFile().exists());
  }
}
