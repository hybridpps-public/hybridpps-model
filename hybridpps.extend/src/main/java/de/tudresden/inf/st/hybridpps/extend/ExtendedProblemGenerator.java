package de.tudresden.inf.st.hybridpps.extend;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import de.tudresden.inf.st.hybridpps.extend.data.inputjson.*;
import de.tudresden.inf.st.hybridpps.extend.mpsplibxml.DataMultiProjectList;
import de.tudresden.inf.st.hybridpps.jastadd.model.ProductionSystem;
import de.tudresden.inf.st.hybridpps.parser.KolischInstanceParser;
import picocli.CommandLine;
import picocli.CommandLine.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

public class ExtendedProblemGenerator implements Callable<Integer> {

    private static Random rand = new Random();

    @Option(names = {"-x", "--xml"}, description = "Path to a xml file from the mpsplib or to a directory with such files.")
    private File xml = null;

    @Option(names = {"-k", "--kolisch"}, description = "Add a kolisch instance to the problem model. Can be used multiple times. Can't be used together with --xml")
    private List<File> kolischInstances = null;

    @Option(names = {"-s", "--seed"}, description = "Seed to use for the random number generator. Default is not using a seed.")
    private Integer seed = null;

    /**
     * Creates extensions for one or more resource constraint multi project scheduling problems from the mpsplib defined
     * by .xml files and Kolisch instances.
     */
    public static void main(String[] args) {
        int exitCode = new CommandLine(new ExtendedProblemGenerator()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public Integer call() throws Exception {
        if (xml == null && kolischInstances == null) {
            System.exit(1);
        }

        if (seed != null) {
            rand.setSeed(seed);
        }

        if (xml != null) {

            Path outputPath;
            if (xml.isDirectory()) {
                outputPath = xml.toPath();
                File[] subFiles = xml.listFiles();
                if (subFiles == null) {
                    throw new IOException("Couldn't get files from specified directory " + xml.toString());
                }
                for (File f : subFiles) {
                    // skip directories
                    if (f.isDirectory()) continue;
                    String filetype = Files.probeContentType(f.toPath());
                    // skip files that are not .xml files
                    if (!filetype.equals("text/xml")) continue;

                    createWithXml(f, outputPath);
                }
            } else {
                outputPath = xml.toPath().getParent();
                createWithXml(xml, outputPath);
            }
        } else {
            Path outputPath = kolischInstances.get(0).toPath().getParent();
            createWithInstances(kolischInstances, outputPath);
        }

        return 0;
    }

    private void createWithXml(File xmlFile, Path outputPath) throws IOException {
        ObjectMapper objectMapper = new XmlMapper();
        DataMultiProjectList mpsp = objectMapper.readValue(xmlFile, DataMultiProjectList.class);

        DataProductionSystem productionSystem =  createProductionSystem(
                mpsp.mp.project.stream().map(p -> xmlFile.toPath().getParent().resolve(p.filename).toFile()).collect(Collectors.toList()),
                mpsp.mp.project.stream().map(p -> p.filename).collect(Collectors.toList()));

        productionSystem.name = xmlFile.getName().split("\\.")[0];
        String outputFileName = xmlFile.getName().split("\\.")[0] + ".json";

        saveJson(productionSystem, outputPath, outputFileName);
    }

    private void createWithInstances(List<File> kolischInstances, Path outputPath) throws IOException {

        DataProductionSystem productionSystem =  createProductionSystem(kolischInstances,
                kolischInstances.stream().map(File::getName).collect(Collectors.toList()));

        productionSystem.name = "ExtendedProblem";

        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd-HH-mm-ss")
                .withZone(ZoneId.systemDefault());
        String now = formatter.format(Instant.now());
        String outputFileName = "Extended_Problem_" + now + ".json";

        saveJson(productionSystem, outputPath, outputFileName);
    }

    private void saveJson(DataProductionSystem dps, Path outputPath, String name) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();
        writer.writeValue(outputPath.resolve(name).toFile(), dps);
    }

    private DataProductionSystem createProductionSystem(List<File> kolischInstances, List<String> instanceNames) {
        DataProductionSystem productionSystem = new DataProductionSystem();
        productionSystem.seed = 0;

        DataUnrolling unrolling = new DataUnrolling();
        unrolling.precision = 2;
        unrolling.count = 100;
        productionSystem.unrolling = unrolling;

        // number of TRs between 1 and 4
        int numTR = rand.nextInt(4) + 1;
        List<Integer> minSetupTimes = new ArrayList<>();
        List<Integer> maxSetupTimes = new ArrayList<>();
        for(int i = 0; i < numTR; i++) {
            minSetupTimes.add(rand.nextInt(5) + 1); // 1 - 5
            maxSetupTimes.add(rand.nextInt(5) + 5); // 5 - 9
        }
        List<DataTypeRepresentative> typeRepresentatives = new ArrayList<>();
        for (int i = 0; i < numTR; i++) {
            DataTypeRepresentative tr = new DataTypeRepresentative();
            tr.name = "TR" + (i + 1);
            tr.setupMin = minSetupTimes.get(i);
            tr.setupMax = maxSetupTimes.get(i);
            typeRepresentatives.add(tr);
        }
        productionSystem.typeRepresentatives = typeRepresentatives;

        // system default cv between 0.1 and 0.5
        double cv = ((double) (rand.nextInt(5) + 1)) / 10;
        DataObjective objective = new DataObjective();
        objective.type = "project delay";
        objective.function = "average";

        DataDefaultJobExtension defaultJobExtension = new DataDefaultJobExtension();
        defaultJobExtension.typeRepresentative = "TR1";

        DataDefaultValues systemDefaults = new DataDefaultValues();
        systemDefaults.objectives = new LinkedList<>();
        systemDefaults.objectives.add(objective);
        systemDefaults.cv = cv;
        systemDefaults.defaultJobExtension = defaultJobExtension;
        productionSystem.defaults = systemDefaults;

        List<DataProjectExtension> projectExtensions = new ArrayList<>();

        for (int i = 0; i < kolischInstances.size(); i++) {
            DataProjectExtension extension = parseKolischInstance(kolischInstances.get(i), instanceNames.get(i), typeRepresentatives);
            if (extension == null) {
                continue;
            }
            extension.project = String.valueOf(projectExtensions.size());
            projectExtensions.add(extension);
        }
        productionSystem.projects = projectExtensions;

        List<DataResourceGroupExtension> globalResourceGroups = new ArrayList<>();
        List<String> resourceNames = productionSystem.projects.get(0).resourceExtensions
                .stream().map(e -> e.resource)
                .collect(Collectors.toList());

        int numGlobalRes = rand.nextInt(resourceNames.size()) + 1;
        for (int i = 0; i < numGlobalRes; i++) {
            DataResourceGroupExtension res = new DataResourceGroupExtension();
            res.resource = resourceNames.get(i);
            res.capacity = new DataNormallyDistributed();
            globalResourceGroups.add(res);
        }

        productionSystem.globalResourceGroups = globalResourceGroups;

        return productionSystem;
    }

    public DataProjectExtension parseKolischInstance(File kolischInstance, String name, List<DataTypeRepresentative> trs) {
        ProductionSystem ps;
        try {
            ps = KolischInstanceParser.parseFromFile(kolischInstance);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Couldn't parse " + kolischInstance.getName() + ". Skipping it!");
            return null;
        }

        int numJobs = ps.getProject(0).getJobList().getNumChild();

        List<Integer> jobTRs = new ArrayList<>();
        for (int i = 0; i < numJobs; i++) {
            jobTRs.add(rand.nextInt(trs.size()) + 1);
        }

        List<DataJobExtension> jobExtensions = new ArrayList<>();
        for (int i = 0; i < numJobs; i++) {
            int trNumber = jobTRs.get(i);
            // no need to specify tr if its the default
            if (trNumber == 1) {
                continue;
            }

            DataJobExtension jobExtension = new DataJobExtension();
            jobExtension.job = String.valueOf(i+1);
            jobExtension.typeRepresentative = trs.get(trNumber-1).name;
            jobExtensions.add(jobExtension);
        }

        List<DataResourceGroupExtension> resourceExtensions = new ArrayList<>();
        for (int i = 0; i < ps.getProject(0).getNumResourceGroup(); i++) {
            DataNormallyDistributed value = new DataNormallyDistributed();
            // between 0.1 and 0.5
            value.cv = ((double) (rand.nextInt(5) + 1)) / 10;

            DataResourceGroupExtension resourceExtension = new DataResourceGroupExtension();
            resourceExtension.resource = ps.getProject(0).getResourceGroup(i).getName();
            resourceExtension.capacity =value;
            resourceExtensions.add(resourceExtension);
        }

        DataProjectExtension project = new DataProjectExtension();
        project.project = "0";
        project.filename = name;
        project.jobExtensions = jobExtensions;
        project.resourceExtensions = resourceExtensions;

        return project;
    }
}
