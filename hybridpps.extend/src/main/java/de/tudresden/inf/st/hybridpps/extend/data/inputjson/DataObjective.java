package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

import de.tudresden.inf.st.hybridpps.jastadd.model.ObjectiveFunction;
import de.tudresden.inf.st.hybridpps.jastadd.model.ObjectiveType;

/**
 * Serialization object for an objective.
 *
 * @author rschoene - Initial contribution
 */
public class DataObjective {
  public String type = "project";
  public String function = "mean";

  public ObjectiveType toType() {
    try {
      return ObjectiveType.valueOf(type);
    } catch (IllegalArgumentException e) {
      // try to be clever
      switch (type) {
        case "project delay":
        case "project":
        case "delay":
          return ObjectiveType.PROJECT_DELAY;
        case "makespan":
        case "project makespan":
          return ObjectiveType.MAKESPAN;
        default:
          throw e;
      }
    }
  }

  public ObjectiveFunction toFunction() {
    try {
      return ObjectiveFunction.valueOf(function);
    } catch (IllegalArgumentException e) {
      switch (function) {
        case "average":
        case "avg":
        case "mean":
          return ObjectiveFunction.AVERAGE;
        case "deviation":
        case "dev":
        case "sd":
          return ObjectiveFunction.DEVIATION;
        case "median":
          return ObjectiveFunction.MEDIAN;
        case "span":
          return ObjectiveFunction.SPAN;
        default:
          return ObjectiveFunction.NONE;
      }
    }
  }
}
