package de.tudresden.inf.st.hybridpps.extend.metajson;

/**
 * Serialization object for a reference to a project file.
 *
 * @author rschoene - Initial contribution
 */
public class DataProjectOutput {
  public String name;
  public String filename;
}
