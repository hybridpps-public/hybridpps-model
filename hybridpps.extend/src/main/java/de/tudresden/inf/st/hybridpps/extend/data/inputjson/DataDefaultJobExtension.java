package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Serialization object for a the default values for all jobs in this project.
 *
 * @author rschoene - Initial contribution
 */
public class DataDefaultJobExtension {
  @JsonProperty("tr")
  public String typeRepresentative;
  public DataModeExtension mode;
}
