package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

/**
 * Serialization object for unrolling options.
 *
 * @author rschoene - Initial contribution
 */
public class DataUnrolling {
  public int count = 10;
  public int precision = 2;
}
