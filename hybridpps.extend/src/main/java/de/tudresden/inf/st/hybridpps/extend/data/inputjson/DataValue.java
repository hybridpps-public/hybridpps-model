package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import de.tudresden.inf.st.hybridpps.jastadd.model.Value;

/**
 * Serialization object for a duration.
 *
 * @author rschoene - Initial contribution
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = DataConcreteValue.class, name = "fixed"),
    @JsonSubTypes.Type(value = DataNormallyDistributed.class, name = "normal")
})
public abstract class DataValue {
  public abstract Value toValue(int previousFixedNumber, double cv);
}
