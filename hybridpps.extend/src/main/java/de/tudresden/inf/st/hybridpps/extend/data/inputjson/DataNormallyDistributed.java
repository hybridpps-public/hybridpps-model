package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

import de.tudresden.inf.st.hybridpps.jastadd.model.ConcreteValue;
import de.tudresden.inf.st.hybridpps.jastadd.model.NormallyDistributedValue;
import de.tudresden.inf.st.hybridpps.jastadd.model.Value;

/**
 * Serialization object for a normally distributed value.
 *
 * @author rschoene - Initial contribution
 */
public class DataNormallyDistributed extends DataValue {
  public Double cv;

  @Override
  public Value toValue(int previousFixedNumber, double cv) {
    // cv = sd / mean. then sd = cv * mean
    return new NormallyDistributedValue(
        new ConcreteValue(previousFixedNumber),  // deterministicValue
        previousFixedNumber,  // mean
        previousFixedNumber * (this.cv != null ? this.cv : cv));  // sd
  }
}
