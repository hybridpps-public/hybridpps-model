package de.tudresden.inf.st.hybridpps.extend.metajson;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.tudresden.inf.st.hybridpps.jastadd.model.Value;

import java.math.BigDecimal;
import java.util.List;

/**
 * Serialization object for a complete type representative.
 *
 * @author rschoene - Initial contribution
 */
public class DataTypeRepresentativeOutput {
  public String name;
  @JsonProperty("deterministic_setup_time")
  public int deterministicSetupTime;

  @JsonProperty("stochastic_setup_times")
  public List<BigDecimal> stochasticSetupTimes;
}
