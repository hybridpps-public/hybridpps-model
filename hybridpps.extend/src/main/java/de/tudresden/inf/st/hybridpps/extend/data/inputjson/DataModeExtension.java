package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Serialization object for a mode and its extensions.
 *
 * @author rschoene - Initial contribution
 */
public class DataModeExtension {
  @JsonProperty(required = true)
  public String mode;
  public DataValue duration;
}
