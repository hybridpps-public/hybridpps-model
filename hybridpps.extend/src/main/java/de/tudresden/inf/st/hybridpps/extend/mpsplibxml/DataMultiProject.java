package de.tudresden.inf.st.hybridpps.extend.mpsplibxml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.util.List;

public class DataMultiProject {
    public String name;
    @JacksonXmlElementWrapper(localName = "project-list")
    public List<DataSingleProject> project;
    @JacksonXmlElementWrapper(localName = "resources")
    public List<Integer> resources;
}
