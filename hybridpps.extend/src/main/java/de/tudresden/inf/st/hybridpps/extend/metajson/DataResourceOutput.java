package de.tudresden.inf.st.hybridpps.extend.metajson;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

/**
 * Serialization object for a global resource.
 *
 * @author rschoene - Initial contribution
 */
public class DataResourceOutput {
  public String name;

  @JsonProperty("deterministic_capacity-value")
  public int deterministicCapacityValue;

  @JsonProperty("stochastic_capacity_values")
  public List<BigDecimal> stochasticCapacityValues;
}
