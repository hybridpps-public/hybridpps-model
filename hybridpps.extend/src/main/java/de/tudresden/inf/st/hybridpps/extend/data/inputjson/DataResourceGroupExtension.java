package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Serialization object for a resource and its extension.
 *
 * @author rschoene - Initial contribution
 */
public class DataResourceGroupExtension {
  @JsonProperty(required = true)
  public String resource;
  public DataValue capacity;
}
