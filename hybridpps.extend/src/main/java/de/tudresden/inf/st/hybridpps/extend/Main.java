package de.tudresden.inf.st.hybridpps.extend;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudresden.inf.st.hybridpps.extend.data.inputjson.DataProductionSystem;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Main entry point for testing extension parsing.
 *
 * @author rschoene - Initial contribution
 */
public class Main {
  public static void main(String[] args) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    File file = Paths.get("src", "main", "resources", "handmade.json").toFile();
    DataProductionSystem dps = mapper.readValue(file, DataProductionSystem.class);
    System.out.println("parsed successful: " + dps);
  }
}
