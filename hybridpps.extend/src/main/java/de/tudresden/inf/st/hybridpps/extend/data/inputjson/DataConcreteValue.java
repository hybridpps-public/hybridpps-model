package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.tudresden.inf.st.hybridpps.jastadd.model.ConcreteValue;
import de.tudresden.inf.st.hybridpps.jastadd.model.Value;

/**
 * Serialization object for a concrete value.
 *
 * @author rschoene - Initial contribution
 */
public class DataConcreteValue extends DataValue {
  @JsonProperty(required = true)
  public int number;

  @Override
  public Value toValue(int previousFixedNumber, double cv) {
    // replace previous value
    return new ConcreteValue(previousFixedNumber);
  }
}
