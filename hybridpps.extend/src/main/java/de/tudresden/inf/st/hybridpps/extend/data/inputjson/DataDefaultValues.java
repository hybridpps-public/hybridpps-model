package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Serialization object for default values.
 *
 * @author rschoene - Initial contribution
 */
public class DataDefaultValues {
  public List<DataObjective> objectives;
  public Double cv;
  @JsonProperty("default_job_extension")
  public DataDefaultJobExtension defaultJobExtension;
}
