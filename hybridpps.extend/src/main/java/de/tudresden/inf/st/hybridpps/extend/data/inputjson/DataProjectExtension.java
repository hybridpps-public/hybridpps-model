package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Serialization object for a project and its extensions.
 *
 * @author rschoene - Initial contribution
 */
public class DataProjectExtension {
  @JsonProperty(required = true)
  public String project;
  @JsonProperty(required = true)
  public String filename;
  @JsonProperty("project_default")
  public DataDefaultValues defaults;
  @JsonProperty("job_extensions")
  public List<DataJobExtension> jobExtensions;
  @JsonProperty("resource_extensions")
  public List<DataResourceGroupExtension> resourceExtensions;
}
