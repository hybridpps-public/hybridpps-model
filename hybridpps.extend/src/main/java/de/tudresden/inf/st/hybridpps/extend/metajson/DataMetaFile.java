package de.tudresden.inf.st.hybridpps.extend.metajson;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Serialization object for the generated file containing meta information.
 *
 * @author rschoene - Initial contribution
 */
public class DataMetaFile {
  public String name;
  public int horizon;
  @JsonProperty("type_representatives")
  public List<DataTypeRepresentativeOutput> typeRepresentatives;
  public List<DataProjectOutput> projects;
  @JsonProperty("global_resources")
  public List<DataResourceOutput> globalResources;
}
