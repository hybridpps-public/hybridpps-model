package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Serialization object for a type representative.
 *
 * @author rschoene - Initial contribution
 */
public class DataTypeRepresentative {
  @JsonProperty(required = true)
  public String name;
  @JsonProperty(required = true)
  public int setupMin;
  @JsonProperty(required = true)
  public int setupMax;
}
