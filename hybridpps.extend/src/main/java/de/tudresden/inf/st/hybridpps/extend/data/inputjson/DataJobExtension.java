package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Serialization object for a job and its extensions.
 *
 * @author rschoene - Initial contribution
 */
public class DataJobExtension {
  @JsonProperty(required = true)
  public String job;
  @JsonProperty(value = "tr", required = true)
  public String typeRepresentative;
  public List<DataModeExtension> modes;
}
