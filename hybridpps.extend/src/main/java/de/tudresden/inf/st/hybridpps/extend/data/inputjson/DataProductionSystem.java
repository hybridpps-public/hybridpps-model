package de.tudresden.inf.st.hybridpps.extend.data.inputjson;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Serialization object for a production system and its extension.
 *
 * @author rschoene - Initial contribution
 */
public class DataProductionSystem {
  public String name = "PS";
  public long seed = 0;
  public DataUnrolling unrolling = new DataUnrolling();
  @JsonProperty("system_default")
  public DataDefaultValues defaults;
  @JsonProperty("type_representatives")
  public List<DataTypeRepresentative> typeRepresentatives;
  public List<DataProjectExtension> projects;
  @JsonProperty("global_resources")
  public List<DataResourceGroupExtension> globalResourceGroups;
}
