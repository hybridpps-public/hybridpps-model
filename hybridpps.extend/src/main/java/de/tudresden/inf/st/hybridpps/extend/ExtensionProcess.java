package de.tudresden.inf.st.hybridpps.extend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.tudresden.inf.st.hybridpps.extend.data.inputjson.*;
import de.tudresden.inf.st.hybridpps.extend.metajson.DataMetaFile;
import de.tudresden.inf.st.hybridpps.extend.metajson.DataProjectOutput;
import de.tudresden.inf.st.hybridpps.extend.metajson.DataResourceOutput;
import de.tudresden.inf.st.hybridpps.extend.metajson.DataTypeRepresentativeOutput;
import de.tudresden.inf.st.hybridpps.jastadd.model.*;
import de.tudresden.inf.st.hybridpps.parser.KolischInstanceParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The extension process.
 *
 * @author rschoene - Initial contribution
 */
public class ExtensionProcess {

  public abstract static class Processor<T> {
    private T result;
    private Exception failure;
    private final String name;

    public Processor(String name) {
      this.name = name;
    }

    public Processor() {
      this("<Anonymous>");
    }

    public void run(ProductionSystem productionSystem) {
      try {
        result = process(productionSystem);
      } catch (Exception e) {
        failure = e;
      }
    }

    protected abstract T process(ProductionSystem productionSystem) throws Exception;

    public String getName() {
      return name;
    }

    public T getResult() {
      return result;
    }

    public Exception getFailure() {
      return failure;
    }

    public boolean wasSuccessful() {
      return failure == null;
    }
  }

  public abstract static class SolutionProcessor extends Processor<AbstractSolution> {

    public SolutionProcessor(String name) {
      super(name);
    }
  }

  private static final Logger logger = LogManager.getLogger(ExtensionProcess.class);
  private static final String DIRECTORY_SUFFIX_PATTERN = "yyyy-MM-dd-HH-mm-ss";

  // input parameters
  private Path pathToInputJson;
  private String outputDirectory = "models";
  private String metaFileName = "meta.json";

  // configuration
  private boolean persistModel = true;
  private boolean addTimestampForOutput = true;
  private final List<Processor<?>> internalGeneralProcessors = new ArrayList<>();
  private final List<Processor<?>> externalGeneralProcessors = new ArrayList<>();
  private final List<SolutionProcessor> solutionProcessors = new ArrayList<>();

  // temporary or output objects created during the process
  private DataProductionSystem dps;
  private ProductionSystem system;

  public void setPathToInputJson(Path pathToInputJson) {
    this.pathToInputJson = pathToInputJson;
  }

  public void setOutputDirectory(String outputDirectory) {
    this.outputDirectory = outputDirectory;
  }

  public void setMetaFileName(String metaFileName) {
    this.metaFileName = metaFileName;
  }

  public void setPersistModel(boolean persistModel) {
    this.persistModel = persistModel;
  }

  public void setAddTimestampForOutput(boolean addTimestampForOutput) {
    this.addTimestampForOutput = addTimestampForOutput;
  }

  public void addGeneralProcessor(Processor<?> processor) {
    this.externalGeneralProcessors.add(processor);
  }

  public void addSolutionProcessor(SolutionProcessor processor) {
    this.solutionProcessors.add(processor);
  }

  public void addSolutionProcessor(String name, Function<ProductionSystem, AbstractSolution> process) {
    this.solutionProcessors.add(new SolutionProcessor(name) {
      @Override
      protected AbstractSolution process(ProductionSystem productionSystem) {
        return process.apply(productionSystem);
      }
    });
  }

  public List<Processor<?>> getGeneralProcessors() {
    return externalGeneralProcessors;
  }

  public List<SolutionProcessor> getSolutionProcessors() {
    return solutionProcessors;
  }

  public boolean run() {
    logger.debug("Using {}", pathToInputJson.toAbsolutePath());
    system = new ProductionSystem();
    // add new processor to add extensions
    Processor<Boolean> addExtensionProcessor = new Processor<Boolean>("addExtensions") {
      @Override
      protected Boolean process(ProductionSystem productionSystem) throws Exception {
        return addExtensions();
      }
    };
    internalGeneralProcessors.add(addExtensionProcessor);
    if (persistModel) {
      // add new processor to persist models
      Processor<Boolean> persistInstances = new Processor<Boolean>("persistExtendedKolischInstances") {
        @Override
        protected Boolean process(ProductionSystem productionSystem) throws Exception {
          return persistExtendedKolischInstances();
        }
      };
      internalGeneralProcessors.add(persistInstances);
    }
    boolean success = true;
    List<Processor<?>> processors = new ArrayList<>();
    processors.addAll(internalGeneralProcessors);
    processors.addAll(externalGeneralProcessors);
    processors.addAll(solutionProcessors);
    for (Processor<?> processor : processors) {
      processor.run(system);
      if (!processor.wasSuccessful()) {
        logger.catching(processor.getFailure());
        logger.error("Aborting process {}", processor.getName());
        success = false;
        break;
      }
    }
    if (success) {
      logger.info(system.prettyPrint());
    } else {
      logger.fatal("Not successful due to previous errors :(");
    }
    return success;
  }

  private boolean addExtensions() throws IOException {
    boolean success = true;
    ObjectMapper mapper = new ObjectMapper();
    dps = mapper.readValue(pathToInputJson.toFile(), DataProductionSystem.class);
    // if there is no defaults object, create an empty one to avoid NPEs
    if (dps.defaults == null) {
      dps.defaults = new DataDefaultValues();
    }

    system.setName(dps.name);
    system.setSeed(dps.seed);
    system.setScenarioSize(dps.unrolling.count);
    system.setRoundingPrecision(dps.unrolling.precision);

    // create type representatives
    for (DataTypeRepresentative dtr : dps.typeRepresentatives) {
      TypeRepresentative t = new TypeRepresentative();
      t.setName(dtr.name);
      double mean = dtr.setupMin + 0.5 * (dtr.setupMax - dtr.setupMin);
      t.setSetupTime(new NormallyDistributedValue(new ConcreteValue((int) mean), mean, mean*dps.defaults.cv));
      t.setMinSetup(dtr.setupMin);
      t.setMaxSetup(dtr.setupMax);
      system.addTypeRepresentative(t);
    }
    Map<DataResourceGroupExtension, ResourceGroup> createdGlobalResourceGroups = new HashMap<>();

    // process projects
    for (DataProjectExtension projectExtension : dps.projects) {
      // if there is no defaults object, create an empty one to avoid NPEs
      if (projectExtension.defaults == null) {
        projectExtension.defaults = new DataDefaultValues();
      }
      // 1) parse kolisch instance
      ProductionSystem innerProductionSystem = KolischInstanceParser.parseFromFile(
          pathToInputJson.getParent().resolve(projectExtension.filename));

      // 2) move project object (an "add" in new tree is sufficient, if resources are all local for project)
      if (innerProductionSystem.getNumResourceGroup() > 0) {
        logger.warn("There are global resources in instance {}. " +
                "They will be ignored for now, but will likely cause trouble later",
            projectExtension.filename);
      }
      Project project = innerProductionSystem.getProject(0);
      system.addProject(project);
      if (project.getName().isEmpty()) {
        project.setName(projectExtension.project);
      }

      // 2.1) check global resources extensions and create a new global resource here, if necessary and not already existing
      for (ResourceGroup projectResource : project.getResourceGroupList()) {
        for (DataResourceGroupExtension dre : safeAccess(dps.globalResourceGroups)) {
          if (Objects.equals(projectResource.getName(), dre.resource)) {
            if (!createdGlobalResourceGroups.containsKey(dre)) {
              ResourceGroup res = new ResourceGroup();
              res.setName(dre.resource);
              res.setCapacity(dre.capacity.toValue(projectResource.getCapacity().deterministicValue(), dps.defaults.cv));
              system.addResourceGroup(res);
              createdGlobalResourceGroups.put(dre, res);
            } else {
              // there is already a global resources, update capacity if needed
              ResourceGroup globalRes = createdGlobalResourceGroups.get(dre);
              Value globalCapacity = globalRes.getCapacity();
              Value projectResCapacity = projectResource.getCapacity();
              if (globalCapacity.deterministicValue() < projectResCapacity.deterministicValue()) {
                globalRes.setCapacity(dre.capacity.toValue(projectResource.getCapacity().deterministicValue(), dps.defaults.cv));
              }
            }
          }
        }
      }

      // flushing is necessary here, because we might have changed resources. and we changed parent of project.
      project.flushTreeCache();
      // use project cv if defined, or fallback to global cv
      double projectCV = projectExtension.defaults.cv != null ? projectExtension.defaults.cv : dps.defaults.cv;

      // 3) set objective
      // if an objective is defined on the project, then use this
      /*
       Even in the case that the list of system objectives is superset of the given project specific objectives,
       as long as at least one project objective is given, the system default is ignored. No merge takes place!
       */
      if (projectExtension.defaults.objectives != null && projectExtension.defaults.objectives.size() > 0) {
        int i = 0;
        for(DataObjective objective : projectExtension.defaults.objectives) {
          project.setObjective(new Objective(
                  objective.toFunction(),
                  objective.toType()), i);
          i++;
        }
      /*
       If no objective is defined on the project, the list of system_default objectives is used.
       Note that always the full list od system objectives is used.
       The following if-statement is marked as redundant by intelliJ which it in my understanding is not.
       */
      } else if(projectExtension.defaults.objectives == null || projectExtension.defaults.objectives.size() == 0){
        if (dps.defaults.objectives != null) {
          int i = 0;
          for(DataObjective objective : dps.defaults.objectives) {
            project.setObjective(new Objective(
                    objective.toFunction(),
                    objective.toType()), i);
            i++;
          }
        }
      } else {
        // no objective found
        logger.warn("No objective found for project {} (file {})", projectExtension.project, projectExtension.filename);
      }

      // 4) extend jobs: add type representatives and change duration
      // 4.1) apply defaults (if given) for all jobs
      final String systemDefaultTypeRepresentative =
          dps.defaults.defaultJobExtension != null ? dps.defaults.defaultJobExtension.typeRepresentative : null;
      final String projectDefaultTypeRepresentative =
          projectExtension.defaults.defaultJobExtension != null ?
              projectExtension.defaults.defaultJobExtension.typeRepresentative :
              null;
      final DataModeExtension systemDefaultModeExtension =
          dps.defaults.defaultJobExtension != null ? dps.defaults.defaultJobExtension.mode : null;
      final DataModeExtension projectDefaultModeExtension =
          projectExtension.defaults.defaultJobExtension != null ?
              projectExtension.defaults.defaultJobExtension.mode :
              null;
      if (systemDefaultTypeRepresentative != null || systemDefaultModeExtension != null ||
          projectDefaultTypeRepresentative != null || projectDefaultModeExtension != null) {
        // only iterate if there is any default to apply at all
        for (Job job : project.getJobList()) {
          // null check is done inside
          applyTypeRepresentativeExtension(job, systemDefaultTypeRepresentative);
          applyTypeRepresentativeExtension(job, projectDefaultTypeRepresentative);
          for (Mode mode : job.getModeList()) {
            // null check is done inside
            applyModeExtension(mode, systemDefaultModeExtension, projectCV);
            applyModeExtension(mode, projectDefaultModeExtension, projectCV);
          }
        }
      }

      // 4.2) apply specific job extension
      for (DataJobExtension jobExtension : safeAccess(projectExtension.jobExtensions)) {
        Job job = project.resolveJob(jobExtension.job);
        applyTypeRepresentativeExtension(job, jobExtension.typeRepresentative);
        for (DataModeExtension modeExtension : safeAccess(jobExtension.modes)) {
          applyModeExtension(job.resolveMode(modeExtension.mode), modeExtension, projectCV);
        }
      }

      // 5) extend resource: change capacity
      for (DataResourceGroupExtension dse : safeAccess(projectExtension.resourceExtensions)) {
        if (dse.capacity != null) {
          ResourceGroup res = project.resolveResourceGroup(dse.resource);
          // check, that we do not accidentally alter a global resource
          if (res.isGlobal()) {
            logger.warn("Resource extension ignored for resource {} in project {}",
                res.getName(), project.getName());
          } else {
            res.setCapacity(dse.capacity.toValue(res.getCapacity().deterministicValue(), projectCV));
          }
        }
      }
    }

    // sanity checks (and repairs) for problem model
    if (system.getNumProject() == 0) {
      logger.fatal("No projects found in {}.", pathToInputJson);
      success = false;
    } else {
      if (createdGlobalResourceGroups.size() < safeAccess(dps.globalResourceGroups).size()) {
        // fewer global resources created than specified
        Set<DataResourceGroupExtension> missingResources = new HashSet<>(createdGlobalResourceGroups.keySet());
        dps.globalResourceGroups.forEach(missingResources::remove);
        logger.warn("Some global resources were not created: {}",
            missingResources.stream().map(dre -> dre.resource).collect(Collectors.joining(", ")));
      }

      for (Project project : system.getProjectList()) {
        if (system.getNumResourceGroup() == 0 && project.getNumResourceGroup() == 0) {
          logger.warn("Neither global nor local resources found in project {}.", project.getName());
        }
        if (project.getNumJob() == 0) {
          logger.fatal("No jobs found for project {}.", project.getName());
          success = false;
        }

        int modesWithFixedDuration = 0;
        int modesWithStochasticDuration = 0;
        for (Job job : project.getJobList()) {
          if (!job.hasTypeRepresentative()) {
            logger.warn("No type representative set for job {} in project {}. Using dummy with setup time of 0.",
                job.getName(), project.getName());
            TypeRepresentative dummy = project.resolveTypeRepresentative("dummy");
            if (dummy == null) {
              // create it first
              logger.info("Creating dummy type representative");
              dummy = new TypeRepresentative("dummy", new NormallyDistributedValue(new ConcreteValue(0), 0, 0), 0, 0);
              system.addTypeRepresentative(dummy);
            }
            job.setTypeRepresentative(dummy);
          }
          for (Mode mode : job.getModeList()) {
            if (mode.getNumResourceRequirement() == 0 &&
                !job.getSuccessors().isEmpty() &&
                !job.getPredecessors().isEmpty()) {
              logger.warn("Non-dummy mode {} of job {} in project {} has no resource requirements",
                  mode.getName(), job.getName(), project.getName());
            }
            if (mode.getDuration().isConcrete()) {
              modesWithFixedDuration += 1;
            } else {
              modesWithStochasticDuration += 1;
            }
          }
        }
        logger.info("Found {} mode(s) with fixed duration, and {} with stochastic valued duration in project {}.",
            modesWithFixedDuration, modesWithStochasticDuration, project.getName());

        int resourceGroupsWithFixedCapacity = 0;
        int resourceGroupsWithStochasticCapacity = 0;
        for (ResourceGroup resourceGroup : project.allResourceGroups()) {
          if (resourceGroup.getCapacity().isConcrete()) {
            resourceGroupsWithFixedCapacity += 1;
          } else {
            resourceGroupsWithStochasticCapacity += 1;
          }
        }
        logger.info("Project {} uses {} resource(s) with fixed capacity, and {} with stochastic valued capacity",
            project.getName(), resourceGroupsWithFixedCapacity, resourceGroupsWithStochasticCapacity);
      }
    }
    system.treeResolveAll();
    return success;
  }

  private static void applyTypeRepresentativeExtension(Job job, String typeRepresentative) {
    if (typeRepresentative != null) {
      job.setTypeRepresentative(TypeRepresentative.createRefDirection(typeRepresentative));
    }
  }

  private static void applyModeExtension(Mode mode, DataModeExtension modeExtension, double projectCV) {
    if (modeExtension != null && modeExtension.duration != null) {
      // construct a new normal distributed value using the existing value as mean
      mode.setDuration(modeExtension.duration.toValue(mode.getDuration().deterministicValue(), projectCV));
    }
  }

  private boolean persistExtendedKolischInstances() throws IOException {
    File outDirectory = Paths.get(outputDirectory).toFile();
    if (outDirectory.exists() || outDirectory.mkdir()) {
      final String outName;
      if (addTimestampForOutput) {
        // create a directory with the current timestamp
        DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern(DIRECTORY_SUFFIX_PATTERN)
            .withZone(ZoneId.systemDefault());
        String now = formatter.format(Instant.now());
        outName = system.getName() + "-" + now;
      } else {
        outName = system.getName();
      }
      Path currentDirectoryPath = outDirectory.toPath().resolve(outName);
      File currentDirectory = currentDirectoryPath.toFile();
      if (currentDirectory.exists()) {
        Files.walk(currentDirectoryPath)
            .sorted(Comparator.reverseOrder())
            .map(Path::toFile)
            .forEach(file -> {
              if (!file.delete()) {
                logger.warn("Could not delete {}", file);
              }
            });
      }
      if (currentDirectory.mkdir()) {
        // create meta file
        DataMetaFile dataMetaFile = createDataMetaFile();
        File metaFile = currentDirectory.toPath().resolve(metaFileName).toFile();
        if (metaFile.createNewFile()) {
          ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();
          writer.writeValue(metaFile, dataMetaFile);
        } else {
          failFile(metaFile);
        }

        // create project files
        for (Project project : system.getProjectList()) {
          String content = project.extendedPrint();
          File projectFile = currentDirectory.toPath().resolve(filenameForProject(project)).toFile();
          if (projectFile.createNewFile()) {
            // write extended content
            logger.info("Writing {}", projectFile);
            try (BufferedWriter writer = Files.newBufferedWriter(projectFile.toPath(), StandardOpenOption.WRITE)) {
              writer.write(content);
            } catch (IOException e) {
              logger.catching(e);
              return false;
            }
          } else {
            return failFile(projectFile);
          }
        }
      } else {
        return failDirectory(currentDirectory);
      }
    } else {
      return failDirectory(outDirectory);
    }
    return true;
  }

  private static boolean failFile(File file) {
    logger.error("Could not create file {}", file.getAbsolutePath());
    return false;
  }

  private DataMetaFile createDataMetaFile() {
    DataMetaFile result = new DataMetaFile();
    result.name = system.getName();
    result.horizon = 0;  // method for calculation missing yet, see #6

    // add resources
    result.globalResources = new ArrayList<>();
    for (ResourceGroup res : system.getResourceGroupList()) {
      DataResourceOutput dataResource = new DataResourceOutput();
      dataResource.name = res.getName();
      dataResource.deterministicCapacityValue = res.getCapacity().deterministicValue();
      dataResource.stochasticCapacityValues = res.getCapacity().unrolledNumbers().getNumbers().stream().map(d -> BigDecimal.valueOf(d).setScale(dps.unrolling.precision, BigDecimal.ROUND_HALF_UP)).collect(Collectors.toList());
      result.globalResources.add(dataResource);
    }

    // add links to projects
    result.projects = new ArrayList<>();
    for (Project project : system.getProjectList()) {
      DataProjectOutput dataProject = new DataProjectOutput();
      dataProject.name = project.getName();
      dataProject.filename = filenameForProject(project);
      result.projects.add(dataProject);
    }

    // add type representatives
    result.typeRepresentatives = new ArrayList<>();
    for (TypeRepresentative tr : system.getTypeRepresentativeList()) {
      DataTypeRepresentativeOutput dataTR = new DataTypeRepresentativeOutput();
      dataTR.name = tr.getName();
      dataTR.deterministicSetupTime = tr.getSetupTime().deterministicValue();
      dataTR.stochasticSetupTimes = tr.getSetupTime().unrolledNumbers().getNumbers().stream().map(d -> BigDecimal.valueOf(d).setScale(dps.unrolling.precision, BigDecimal.ROUND_HALF_UP)).collect(Collectors.toList());
      result.typeRepresentatives.add(dataTR);
    }
    return result;
  }

  private static String filenameForProject(Project project) {
    // create project file with name of project + ".emm" (extended multi mode)
    return project.getName() + ".emm";
  }

  private static boolean failDirectory(File directory) {
    logger.error("Can not access or create directory {}", directory.getAbsolutePath());
    return false;
  }

  private static <T> List<T> safeAccess(List<T> list) {
    if (list == null || list.isEmpty()) {
      return Collections.emptyList();
    }
    return list;
  }

  public ProductionSystem getProductionSystem() {
    return system;
  }
}
