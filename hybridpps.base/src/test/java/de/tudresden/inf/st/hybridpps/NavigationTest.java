package de.tudresden.inf.st.hybridpps;

import de.tudresden.inf.st.hybridpps.jastadd.model.ProductionSystem;
import de.tudresden.inf.st.hybridpps.jastadd.model.Project;
import de.tudresden.inf.st.hybridpps.jastadd.model.ResourceGroup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


/**
 * Testing aspect Navigation.
 *
 * @author rschoene - Initial contribution
 */
public class NavigationTest {

  final String R1 = "R1";
  final String R2 = "R2";
  final String R3 = "R3";
  final String R4 = "R4";

  ProductionSystem ps;
  ResourceGroup global1;
  ResourceGroup global3;
  ResourceGroup global4;
  Project project;
  ResourceGroup local1;
  ResourceGroup local2;
  ResourceGroup local3;

  @BeforeEach
  public void createProductionSystem() {
    ps = new ProductionSystem();
    global1 = TestUtils.createResourceGroup(R1, 5);
    global3 = TestUtils.createResourceGroup(R3, 0);
    global4 = TestUtils.createResourceGroup(R4, 2);
    ps.addResourceGroup(global1);
    ps.addResourceGroup(global3);
    ps.addResourceGroup(global4);
    project = new Project();
    local1 = TestUtils.createResourceGroup(R1, 2);
    local2 = TestUtils.createResourceGroup(R2, 2);
    local3 = TestUtils.createResourceGroup(R3, 2);
    project.addResourceGroup(local1);
    project.addResourceGroup(local2);
    project.addResourceGroup(local3);
    ps.addProject(project);
  }

  @Test
  public void testResolveResource() {
    // global and local resource with same name, global capacity > 0 -> global
    ResourceGroup actual1 = project.resolveResourceGroup(R1);
    assertNotNull(actual1);
    assertEquals(global1, actual1);

    // only local -> local
    ResourceGroup actual2 = project.resolveResourceGroup(R2);
    assertNotNull(actual2);
    assertEquals(local2, actual2);

    // global and local resource with same name, but global capacity == 0 -> local
    ResourceGroup actual3 = project.resolveResourceGroup(R3);
    assertNotNull(actual3);
    assertEquals(local3, actual3);
  }

  @Test
  public void testAllResources() {
    List<ResourceGroup> expectedAllResources = Arrays.asList(global1, local2, local3, global4);
    List<ResourceGroup> actualAllResources = project.allResourceGroups();
    assertNotNull(actualAllResources);
    assertEquals(expectedAllResources, actualAllResources);
  }

}
