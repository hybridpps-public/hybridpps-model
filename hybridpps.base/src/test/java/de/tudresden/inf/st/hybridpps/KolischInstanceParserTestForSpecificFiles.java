package de.tudresden.inf.st.hybridpps;

import de.tudresden.inf.st.hybridpps.jastadd.model.*;
import de.tudresden.inf.st.hybridpps.parser.KolischInstanceParser;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test for {@link KolischInstanceParser} reading all some resource files and checking the parsed result.
 *
 * @author rschoene - Initial contribution
 */
public class KolischInstanceParserTestForSpecificFiles {

  @Test
  public void testJ10_2_single_mode() throws IOException {
    ProductionSystem ps = TestUtils.read("k_j10_2.sm");
    assertEquals(1, ps.getNumProject());

    Project project = ps.getProject(0);
    assertNotNull(project);
    assertEquals(1, project.getNumResourceGroup());
    ResourceGroup res1 = project.getResourceGroup(0);
    assertEquals(10, res1.getCapacity().deterministicValue());
    assertEquals(10 + 2, project.getNumJob());

    // jobnr.    #modes  #successors   successors
    //    1        1          3           2   3  4
    //    7        1          2           8
    Job job1 = project.resolveJob("1");
    Job job2 = project.resolveJob("2");
    Job job3 = project.resolveJob("3");
    Job job4 = project.resolveJob("4");
    Job job7 = project.resolveJob("7");
    Job job8 = project.resolveJob("8");
    assertThat(job1.getSuccessors()).containsExactlyInAnyOrder(job2, job3, job4);
    assertThat(job7.getSuccessors()).containsExactlyInAnyOrder(job8);
    assertEquals(1, job1.getNumMode());
    assertEquals(1, job7.getNumMode());

    //jobnr. mode duration  R 1
    //  1      1     0       0
    //  2      1     5       7
    Mode modeOfJob1 = job1.getMode(0);
    Mode modeOfJob2 = job2.getMode(0);
    assertEquals(0, modeOfJob1.getDuration().deterministicValue());
    assertEquals(5, modeOfJob2.getDuration().deterministicValue());
    assertEquals(0, modeOfJob1.getNumResourceRequirement());
    assertThat(modeOfJob2.getResourceRequirements()).extracting("RequiredResourceGroup", "RequiredCapacity").containsExactlyInAnyOrder(tuple(res1, 7));
  }

  @Test
  public void testJ3037_4_multi_mode() throws IOException {
    ProductionSystem ps = TestUtils.read("j3037_4.mm");
    assertEquals(1, ps.getNumProject());

    Project project = ps.getProject(0);
    assertNotNull(project);
    assertEquals(4, project.getNumResourceGroup());
    ResourceGroup res1 = project.getResourceGroup(0);
    ResourceGroup res2 = project.getResourceGroup(1);
    ResourceGroup res3 = project.getResourceGroup(2);
    ResourceGroup res4 = project.getResourceGroup(3);
    assertEquals(132, res4.getCapacity().deterministicValue());
    assertEquals(30 + 2, project.getNumJob());

    // --- check precedence ---
    // jobnr.    #modes  #successors   successors
    //    3        3          3           5   9  15
    Job job3 = project.resolveJob("3");
    Job job5 = project.resolveJob("5");
    Job job9 = project.resolveJob("9");
    Job job15 = project.resolveJob("15");
    assertNotNull(job3);
    assertNotNull(job5);
    assertNotNull(job9);
    assertNotNull(job15);
    assertEquals(3, job3.getNumMode());
    assertEquals(3, job3.getSuccessors().size());
    assertThat(job3.getSuccessors()).containsExactlyInAnyOrder(job5, job9, job15);

    // --- check resource requirements of different modes ---
    // jobnr. mode duration  R 1  R 2  N 1  N 2
    //   2      1     3       9    8   10    3
    //          2     9       9    7    9    2
    //          3    10       8    7    8    1
    Job job2 = project.resolveJob("2");
    Mode modeOne = job2.getMode(0);
    Mode modeTwo = job2.getMode(1);
    Mode modeThree = job2.getMode(2);

    assertEquals(3, modeOne.getDuration().deterministicValue());
    assertTrue(modeOne.getDuration().isConcrete());
    assertEquals(3, modeOne.getDuration().deterministicValue());
    assertEquals(9, modeTwo.getDuration().deterministicValue());
    assertEquals(10, modeThree.getDuration().deterministicValue());

    assertThat(modeOne.getResourceRequirementList()).extracting("RequiredResourceGroup", "RequiredCapacity").containsExactlyInAnyOrder(
        tuple(res1, 9),
        tuple(res2, 8),
        tuple(res3, 10),
        tuple(res4, 3)
    );
    assertThat(modeTwo.getResourceRequirementList()).extracting("RequiredResourceGroup", "RequiredCapacity").containsExactlyInAnyOrder(
        tuple(res1, 9),
        tuple(res2, 7),
        tuple(res3, 9),
        tuple(res4, 2)
    );
    assertThat(modeThree.getResourceRequirementList()).extracting("RequiredResourceGroup", "RequiredCapacity").containsExactlyInAnyOrder(
        tuple(res1, 8),
        tuple(res2, 7),
        tuple(res3, 8),
        tuple(res4, 1)
    );
  }

}
