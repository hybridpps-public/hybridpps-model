package de.tudresden.inf.st.hybridpps;

import de.tudresden.inf.st.hybridpps.jastadd.model.*;
import de.tudresden.inf.st.hybridpps.parser.KolischInstanceParser;
import de.tudresden.inf.st.hybridpps.parser.SolutionParser;
import de.tudresden.inf.st.hybridpps.parser.data.DataSolution;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Test for {@link SolutionParser}.
 *
 * @author rschoene - Initial contribution
 */
public class SolutionParserTest {

  @Test
  public void testSimpleXMLParse() throws IOException {
    String filename = "3052.xml";
    URL url = SolutionParser.class.getClassLoader().getResource(filename);
    assertNotNull(url);
    DataSolution dataSolution = SolutionParser.parseSolutionXML(url);
    assertNotNull(dataSolution);
    // check some of the data solution content
    assertEquals("mp_j30_a20_nr3.xml", dataSolution.mp.name);
    assertEquals(20, dataSolution.mp.project.size());
    assertEquals("KolischInstanzen/j30/j3012_2.sm", dataSolution.mp.project.get(0).name);
    assertEquals(32, dataSolution.mp.project.get(0).job.size());
    assertEquals(2, dataSolution.mp.project.get(0).job.get(1).nr);
    assertEquals(2, dataSolution.mp.resource.size());
    assertEquals(68, (long) dataSolution.mp.resource.get(0));
  }

  @Test
  public void testOneProjectSolution() throws IOException {
    String problemFileName = "j3012_2.sm";
    String solutionFileName = "3052.xml";
    URL problemUrl = SolutionParser.class.getClassLoader().getResource(problemFileName);
    URL solutionUrl = SolutionParser.class.getClassLoader().getResource(solutionFileName);
    assertNotNull(problemUrl);
    assertNotNull(solutionUrl);
    ProductionSystem problem = KolischInstanceParser.parseFromURL(problemUrl);
    Project firstProject = problem.getProject(0);
    Job job5 = firstProject.resolveJob("5");
    assertNotNull(job5);
    DataSolution dataSolution = SolutionParser.parseSolutionXML(solutionUrl);
    ConcreteSolution solution = SolutionParser.createSolution(dataSolution, problem);
    assertNotNull(solution);
    // check some solution content
    assertEquals(32, solution.getNumMapping());
    assertNotNull(solution.mappingFor(job5));
    assertEquals(8, solution.mappingFor(job5).getStartTime());
    // check solution validity
    ValidationResult validityResult = solution.validate(false);
    assertTrue(validityResult.isValid(), validityResult.getErrors().toString());
  }

}
