package de.tudresden.inf.st.hybridpps;

import de.tudresden.inf.st.hybridpps.jastadd.model.ProductionSystem;
import de.tudresden.inf.st.hybridpps.parser.KolischInstanceParser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Test for {@link KolischInstanceParser} reading all relevant resource files and trying to parse them.
 *
 * @author rschoene - Initial contribution
 */
public class KolischInstanceParserTest {

  @MethodSource("getFiles")
  @ParameterizedTest(name = "\"{0}\" should succeed: {1}")
  public void simpleParse(File kolischInstance, boolean shouldSucceed) throws Exception {
    ProductionSystem ps;
    try {
      ps = KolischInstanceParser.parseFromFile(kolischInstance);
      if (!shouldSucceed) {
        fail("Parsing should have failed for " + kolischInstance);
      }
      assertNotNull(ps);
    } catch (Exception e) {
      if (shouldSucceed) {
        throw e;
      }
    }
  }

  static Stream<Arguments> getFiles() {
    File rootDir = Paths.get("src", "test", "resources").toFile();
    TestUtils.FileSurvey survey = new TestUtils.FileSurvey(rootDir).start();
    if (survey.isEmpty()) {
      fail("Could not find any test files");
    }

    Stream.Builder<Arguments> builder = Stream.builder();
    for (File negative : survey.negativeInputs) {
      builder.add(Arguments.of(negative, false));
    }
    for (File positive : survey.positiveInputs) {
      builder.add(Arguments.of(positive, true));
    }
    return builder.build();
  }

}
