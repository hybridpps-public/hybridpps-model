package de.tudresden.inf.st.hybridpps;

import de.tudresden.inf.st.hybridpps.jastadd.model.*;
import de.tudresden.inf.st.hybridpps.parser.KolischInstanceParser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Utility methods for base tests.
 *
 * @author rschoene - Initial contribution
 */
public class TestUtils {
  static ResourceGroup createResourceGroup(String name, int fixedCapacity) {
    ResourceGroup result = new ResourceGroup();
    result.setName(name);
    result.setCapacity(new ConcreteValue(fixedCapacity));
    return result;
  }

  static void checkMapping(Project project, ConcreteSolution solution, String jobName, int startTime, int earliestFinish, boolean onCriticalPath) {
    Job job = project.resolveJob(jobName);
    assertNotNull(job);
    Mapping m = solution.mappingFor(job);
    assertNotNull(m, "no mapping found for " + jobName);
    assertEquals(startTime, m.getStartTime(), "start time mismatch for " + jobName);
    assertEquals(earliestFinish, m.asCriticalPathMapping().getEarliestFinish(),
        "earliest finish mismatch for " + jobName);
    assertEquals(onCriticalPath, m.isOnCriticalPath(),
        "critical path mismatch for " + jobName);
  }

  static Job makeJob(String name, int duration, String... successors) {
    Job result = new Job();
    result.setName(name);
    for (String successor : successors) {
      result.addSuccessor(Job.createRef(successor));
    }
    Mode mode = new Mode();
    mode.setName(name + "0");
    mode.setDuration(new ConcreteValue(duration));
    result.addMode(mode);
    return result;
  }

  static void addRequirement(Job job, ResourceGroup res, int request) {
    addRequirement(job.getMode(0), res, request);
  }

  static void addRequirement(Mode mode, ResourceGroup res, int request) {
    mode.addResourceRequirement(new ResourceRequirement(request, res));
  }

  static Project makeProjectForSingleProductionSystem(Job... jobs) {
    Project result = new Project();
    for (Job job : jobs) {
      result.addJob(job);
    }
    ProductionSystem ps = new ProductionSystem();
    ps.addProject(result);
    result.treeResolveAll();
    return result;
  }

  static ProductionSystem read(String filename) throws IOException {
    URL url = KolischInstanceParserTestForSpecificFiles.class.getClassLoader().getResource(filename);
    assertNotNull(url);
    return KolischInstanceParser.parseFromURL(url);
  }

  static class FileSurvey {
    List<File> positiveInputs = new ArrayList<>();
    List<File> negativeInputs = new ArrayList<>();
    File root;
    FileSurvey(File root) {
      this.root = root;
    }
    public FileSurvey start() {
      walk(root, true);
      return this;
    }

    private void walk(File dir, boolean shouldSucceed) {
      File[] files = dir.listFiles();
      if (files != null) {
        for (File f : files) {
          if (f.isFile() && filenameMatches(f)) {
            (shouldSucceed ? positiveInputs : negativeInputs).add(f);
          } else if (f.isDirectory()) {
            walk(f, !f.getName().equals("failing") && shouldSucceed);
          }
        }
      }
    }

    private boolean filenameMatches(File f) {
      return f.getName().endsWith(".sm") || f.getName().endsWith(".mm");
    }

    public boolean isEmpty() {
      return positiveInputs.isEmpty() && negativeInputs.isEmpty();
    }
  }
}
