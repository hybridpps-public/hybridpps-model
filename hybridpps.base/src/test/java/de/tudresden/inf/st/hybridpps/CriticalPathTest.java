package de.tudresden.inf.st.hybridpps;

import de.tudresden.inf.st.hybridpps.jastadd.model.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Testing {@link Project#criticalPathMaxModi()}.
 *
 * @author rschoene - Initial contribution
 */
public class CriticalPathTest {
  private static final double DELTA = 1e-5;

/*
           1   3
      /--- b - d ------
     /      \ /        \
 0 (a)       X          |--(g)--(h)
     \      / \        /    2    0
      \---(c)--(e)--(f)
           2    2    4
*/

  @Test
  public void manualTest1() {
    Project project = TestUtils.makeProjectForSingleProductionSystem(
      TestUtils.makeJob("a", 0, "b", "c"),
      TestUtils.makeJob("b", 1, "d", "e"),
      TestUtils.makeJob("c", 2, "d", "e"),
      TestUtils.makeJob("d", 3, "g"),
      TestUtils.makeJob("e", 2, "f"),
      TestUtils.makeJob("f", 4, "g"),
      TestUtils.makeJob("g", 2, "h"),
      TestUtils.makeJob("h", 0));
    ConcreteSolution sol = project.criticalPathMaxModi();
    assertNotNull(sol);
    System.out.println(sol.prettyPrint());
    TestUtils.checkMapping(project, sol, "a", 0, 0, true);
    TestUtils.checkMapping(project, sol, "b", 0, 1, false);
    TestUtils.checkMapping(project, sol, "c", 0, 2, true);
    TestUtils.checkMapping(project, sol, "d", 2, 5, false);
    TestUtils.checkMapping(project, sol, "e", 2, 4, true);
    TestUtils.checkMapping(project, sol, "f", 4, 8, true);
    TestUtils.checkMapping(project, sol, "g", 8, 10, true);
    TestUtils.checkMapping(project, sol, "h", 10, 10, true);
  }

  @Test
  public void minimumCapacityTestToy1() {
    final String R1 = "R1";
    final String R2 = "R2";
    final String R3 = "R3";
    ResourceGroup res1 = TestUtils.createResourceGroup(R1, 4);
    ResourceGroup res2 = TestUtils.createResourceGroup(R2, 5);
    ResourceGroup res3 = TestUtils.createResourceGroup(R3, 8);
    Job a = TestUtils.makeJob("a", 1, "b", "c");
    TestUtils.addRequirement(a, res1, 5);
    TestUtils.addRequirement(a, res2, 5);
    TestUtils.addRequirement(a, res3, 8);
    Job b = TestUtils.makeJob("b", 4, "d");
    TestUtils.addRequirement(b, res1, 5);
    TestUtils.addRequirement(b, res2, 10);
    TestUtils.addRequirement(b, res3, 5);
    Job c = TestUtils.makeJob("c", 1, "d");
    TestUtils.addRequirement(c, res1, 25);
    TestUtils.addRequirement(c, res2, 30);
    TestUtils.addRequirement(c, res3, 25);
    Job d = TestUtils.makeJob("d", 0);
    Project project = TestUtils.makeProjectForSingleProductionSystem(a, b, c, d);
    project.addResourceGroup(res1);
    project.addResourceGroup(res2);
    project.addResourceGroup(res3);

    ConcreteSolution sol = project.criticalPathMaxModi();

    TestUtils.checkMapping(project, sol, "a", 0, 1, true);
    TestUtils.checkMapping(project, sol, "b", 1, 5, true);
    TestUtils.checkMapping(project, sol, "c", 1, 2, false);
    TestUtils.checkMapping(project, sol, "d", 5, 5, true);


    double actualMin1 = res1.minimumCapacity();
    double actualMin2 = res2.minimumCapacity();
    double actualMin3 = res3.minimumCapacity();

    System.out.println("actualMin1 = " + actualMin1);
    System.out.println("actualMin2 = " + actualMin2);
    System.out.println("actualMin3 = " + actualMin3);
  }

  @Test
  public void testMinimumCapacityOneJobTwoModes() {
    final String R1 = "R1";
    final String R2 = "R2";
    ResourceGroup res1 = TestUtils.createResourceGroup(R1, 4);
    ResourceGroup res2 = TestUtils.createResourceGroup(R2, 5);

    Job job = new Job();
    job.setName("job");

    Mode a = new Mode();
    a.setName("a");
    a.setDuration(new ConcreteValue(1));
    TestUtils.addRequirement(a, res1, 0);
    TestUtils.addRequirement(a, res2, 5);
    job.addMode(a);

    Mode b = new Mode();
    b.setName("b");
    b.setDuration(new ConcreteValue(6));
    TestUtils.addRequirement(b, res1, 9);
    TestUtils.addRequirement(b, res2, 0);
    job.addMode(b);
    Project project = TestUtils.makeProjectForSingleProductionSystem(job);
    project.addResourceGroup(res1);
    project.addResourceGroup(res2);

    double actualMin1 = res1.minimumCapacity();
    double actualMin2 = res2.minimumCapacity();

    assertEquals(9, actualMin1, DELTA);
    assertEquals(5, actualMin2, DELTA);
  }

  @Test
  public void testTwoProjects() {
    final String R1 = "R1";
    final String R2 = "R2";

    ResourceGroup res1 = TestUtils.createResourceGroup(R1, 5);  // global
    ResourceGroup res2Project1 = TestUtils.createResourceGroup(R2, 5);
    ResourceGroup res2Project2 = TestUtils.createResourceGroup(R2, 5);

    Job project1Job = TestUtils.makeJob("a", 1);
    TestUtils.addRequirement(project1Job, res1, 7);
    TestUtils.addRequirement(project1Job, res2Project1, 7);
    Project project1 = new Project()
        .addJob(project1Job)
        .addResourceGroup(res2Project1);

    Job project2Job = TestUtils.makeJob("a", 3);
    TestUtils.addRequirement(project2Job, res1, 9);
    TestUtils.addRequirement(project2Job, res2Project2, 9);
    Project project2 = new Project()
        .addJob(project2Job)
        .addResourceGroup(res2Project2);

    ProductionSystem system = new ProductionSystem()
        .addProject(project1)
        .addProject(project2)
        .addResourceGroup(res1);

    double actualMinR1 = res1.minimumCapacity();
    double actualMinR2Project1 = res2Project1.minimumCapacity();
    double actualMinR2Project2 = res2Project2.minimumCapacity();

    /* 16 = max(                  // use maximum of both heuristics
                max(7,9),         // maximum single request
                (7+9)/min(1,3))   // "average" usage, i.e., ratio of sum of usage and shortest critical path
          = max(9, 16) */
    assertEquals(16, actualMinR1, DELTA);
    assertEquals(7, actualMinR2Project1, DELTA);
    assertEquals(9, actualMinR2Project2, DELTA);
  }

}
