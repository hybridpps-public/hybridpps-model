package de.tudresden.inf.st.hybridpps.parser.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.util.List;

/**
 * Serialization object for a job.
 *
 * @author rschoene - Initial contribution
 */
public class DataJob {
  public int nr;
  public int start;
  public int end;
  @JacksonXmlElementWrapper(localName = "resources")
  public List<String> resource;
}
