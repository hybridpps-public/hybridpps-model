package de.tudresden.inf.st.hybridpps.parser;

import de.tudresden.inf.st.hybridpps.jastadd.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Parser for one KolischInstance.
 *
 * @author rschoene - Initial contribution
 */
public class KolischInstanceParser {

  /**
   * Parse a file of a KolischInstance into a problem model
   * @param url location of the file to read in
   * @return a newly create problem model
   * @throws IOException if the file can not be accessed or parsed
   */
  public static ProductionSystem parseFromURL(URL url) throws IOException {
    return parseFromFile(new File(url.getFile()));
  }

  /**
   * Parse a file of a KolischInstance into a problem model
   * @param file the file to read in
   * @return a newly create problem model
   * @throws IOException if the file can not be accessed or parsed
   */
  public static ProductionSystem parseFromFile(File file) throws IOException {
    return parseFromFile(file.toPath());
  }

  /**
   * Parse a file of a KolischInstance into a problem model
   * @param path location of the file to read in
   * @return a newly create problem model
   * @throws IOException if the file can not be accessed or parsed
   */
  public static ProductionSystem parseFromFile(Path path) throws IOException {
    return parseFromString(new String(Files.readAllBytes(path), StandardCharsets.UTF_8));
  }

  /**
   * Parse a String containing a KolischInstance into a problem model
   * @param content the given KolischInstance as text
   * @return a newly create problem model
   */
  public static ProductionSystem parseFromString(String content) {
    return new KolischInstanceParser().parse(content);
  }

  /**
   * Helper data class to hold information during parsing
   */
  private static class ParsingContext {
    public Logger logger;
    public ProductionSystem ps;
    public Project project;
    public String version;
  }

  private static class Phase {
    private Phase nextPhase;
    public final String name;

    Phase(String name) {
      this(name, null);
    }

    Phase(String name, Phase lastPhase) {
      this.name = name;
      if (lastPhase != null) {
        lastPhase.nextPhase = this;
      }
    }

    public Phase getNextPhase() {
      return nextPhase;
    }

    /**
     * Handles a single line of input. Override to specify behaviour. Empty be default.
     * @param pc   Parsing context
     * @param line The line to process
     */
    public void handle(ParsingContext pc, String line) {
      // empty by default
    }

    /**
     * Initialize (or reset) this phase. Override to specify behaviour. Empty by default.
     */
    public void reset() {
      // empty by default
    }
  }

  private static final String WHITESPACE_REGEX = "[\\s]+";
  private static final Phase PHASE_START = new Phase("START");
  private static final Phase PHASE_FILE_INFO = new Phase("FILE_INFO", PHASE_START) {
    @Override
    public void handle(ParsingContext pc, String line) {
      if (line.startsWith("version")) {
        pc.version = line.split(":")[1].trim();
      }
    }
  };
  private static final Phase PHASE_GENERAL_DATA = new Phase("GENERAL_DATA", PHASE_FILE_INFO) {
    @Override
    public void handle(ParsingContext pc, String line) {
      String trimmed = line.trim();
      if (trimmed.startsWith("-")) {
        // parse all three types, treat all as renewable, but warn for other types
        String[] leftAndRight = trimmed.split(":", 2);
        String leftPart = leftAndRight[0];
        String rightPart = leftAndRight[1].trim();
        String[] tokens = rightPart.split(WHITESPACE_REGEX);
        int count = Integer.parseInt(tokens[0]);
        if (count == 0) {
          return;
        }
        if (leftPart.contains("nonrenewable") || leftPart.contains("doubly constrained")) {
          pc.logger.warn("Problem defines other resources {}, treating them as renewable.", trimmed);
        }
        String prefix = tokens[1].trim();
        for (int i = 0; i < count; i++) {
          ResourceGroup res = new ResourceGroup();
          // of course, we want to start counting at 1 :)
          res.setName(prefix + (i + 1));
          pc.project.addResourceGroup(res);
        }
      } else if (trimmed.startsWith("horizon")) {
        pc.project.setHorizon(Integer.parseInt(trimmed.split(":")[1].trim()));
      }
    }
  };
  private static final Phase PHASE_PROJECT_INFO = new Phase("PROJECT_INFO", PHASE_GENERAL_DATA) {
    @Override
    public void handle(ParsingContext pc, String line) {
      if (line.startsWith("PROJECT") || line.startsWith("pronr.")) return;
      /* Example content
       pronr.  #jobs rel.date duedate tardcost  MPM-Time
          1     30      0       46       17       46
       */
      // Assume only one project in the file, and only set due date
      String[] tokens = line.trim().split(WHITESPACE_REGEX);
//      pc.logger.info("project. Split {} into {}", line, tokens);
      pc.project.setEarliestStartTime(0); // TODO maybe this should be rel.date?
      pc.project.setLatestCompletionTime(Integer.parseInt(tokens[3]));
    }
  };
  private static final Phase PHASE_PRECEDENCE = new Phase("PRECEDENCE", PHASE_PROJECT_INFO) {
    Job job;
    @Override
    public void handle(ParsingContext pc, String line) {
      if (line.startsWith("PRECEDENCE") || line.startsWith("jobnr.")) return;
      /* Example content
       jobnr.    #modes  #successors   successors
          1        1          3           2   3   4
       */
      String[] tokens = line.trim().split(WHITESPACE_REGEX);
      String jobName = tokens[0];
//      pc.logger.info("precedence. Split {} into {}", line, tokens);
      pc.project.beQuiet().within(() ->
          job = pc.project.resolveJob(jobName));
      if (job == null) {
        job = new Job();
        job.setName(jobName);
        pc.project.addJob(job);
      }
      // create all mode objects here, with increasing numbers as names
      int numModes = Integer.parseInt(tokens[1]);
      for (int modeNr = 0; modeNr < numModes; modeNr++) {
        Mode mode = new Mode();
        // of course, we want to start counting at 1 here as well :)
        mode.setName(Integer.toString(modeNr + 1));
        job.addMode(mode);
      }
      // do not rely on given "#successors" here, instead use all available successors
      for (int i = 0, successorCount = tokens.length - 3; i < successorCount; i++) {
        job.addSuccessor(Job.createRef(tokens[3 + i]));
      }
    }
  };
  private static final Phase PHASE_RESOURCE_REQ = new Phase("RESOURCE_REQ", PHASE_PRECEDENCE) {
    List<String> resourceIds;
    Job lastJob;

    @Override
    public void reset() {
      resourceIds = null;
    }

    @Override
    public void handle(ParsingContext pc, String line) {
      if (line.startsWith("REQUESTS") || line.startsWith("---")) return;
      if (line.startsWith("jobnr.")) {
        // parse names of resources
        int duration = line.indexOf("duration");
        String[] tokens = line.substring(duration + 8).trim().split(WHITESPACE_REGEX);
        resourceIds = parseResourceIds(tokens);
        return;
      }
      /*
      jobnr. mode duration  R 1  R 2  R 3  R 4
      ------------------------------------------------------------------------
        2      1     3       9    8   10    3
               2     9       9    7    9    2
       */
      String[] tokens = line.trim().split(WHITESPACE_REGEX);
      final int offset;
      final Job job;
      if (lastJob != null && lastJob.getNumMode() > 1) {
        // the last job has multiple modes, thus, the lines will omit the job nr.
        offset = 1;
        job = lastJob;
      } else {
        // a new job begins
        String jobName = tokens[0];
        offset = 0;
        job = pc.project.resolveJob(jobName);
        if (job == null) {
          pc.logger.error("Could not find job '{}'", jobName);
          return;
        }
        lastJob = job;
      }
      // retrieve mode object, take the (n-1)th mode where n is the mode identifier
      int modeIndex = Integer.parseInt(tokens[1 - offset]) - 1;
      Mode mode = job.getMode(modeIndex);
      mode.setDuration(new ConcreteValue(Integer.parseInt(tokens[2 - offset])));
      if (tokens.length - 3 + offset != resourceIds.size()) {
        pc.logger.warn("Expected {} resource field(s), but found {} while parsing line {}",
            resourceIds.size(), tokens.length - 3 - offset, tokens);
      }
      if (modeIndex + 1 == job.getNumMode()) {
        // this is the last mode for this job, so reset lastJob to parse next line correctly
        lastJob = null;
      }
      // if there are no resources, this should work as well (but then the problem is pointless)
      // alternatively: stopIdIndex = tokens.length - 3 + offset
      for (int resIdIndex = 0, stopIdIndex = resourceIds.size(); resIdIndex < stopIdIndex; resIdIndex++) {
        int requiredCapacity = Integer.parseInt(tokens[resIdIndex + 3 - offset]);
        if (requiredCapacity == 0) continue;
        ResourceRequirement req = new ResourceRequirement();
        req.setRequiredCapacity(requiredCapacity);
        req.setRequiredResourceGroup(ResourceGroup.createRefDirection(resourceIds.get(resIdIndex)));
        mode.addResourceRequirement(req);
      }
    }
  };
  private static final Phase PHASE_RESOURCE_AVAIL = new Phase("RESOURCE_AVAIL", PHASE_RESOURCE_REQ) {
    List<String> resourceIds;

    @Override
    public void reset() {
      resourceIds = null;
    }

    @Override
    public void handle(ParsingContext pc, String line) {
      if (line.startsWith("RESOURCE")) return;
      // the next line is expected to contain the header with resource ids
      if (resourceIds == null) {
        resourceIds = parseResourceIds(line.trim().split(WHITESPACE_REGEX));
        if (resourceIds.size() != pc.project.getNumResourceGroup()) {
          // we fail here, because capacities won't be set otherwise
          throw new IllegalStateException("Expected availabilities for " + pc.project.getNumResourceGroup() +
              " resource(s), but found " + resourceIds.size() + ".");
        }
        return;
      }
      // and the line after this contains the actual numbers
      String[] tokens = line.trim().split(WHITESPACE_REGEX);
      for (int i = 0, length = tokens.length; i < length; i++) {
        pc.project.resolveResourceGroup(resourceIds.get(i)).setCapacity(new ConcreteValue(Integer.parseInt(tokens[i])));
      }
    }
  };
  private static final Phase PHASE_END = new Phase("END", PHASE_RESOURCE_AVAIL);

  static List<String> parseResourceIds(String[] tokens) {
    List<String> resourceIds = new ArrayList<>();
    // use symbol and number together for name
    for (int i = 0; i < tokens.length; i+=2) {
      resourceIds.add(tokens[i] + tokens[i+1]);
    }
    return resourceIds;
  }

  private Logger logger = LogManager.getLogger(KolischInstanceParser.class);

  private ProductionSystem parse(String content) {
    ProductionSystem result = new ProductionSystem();
    Project project = new Project();
    result.addProject(project);
    ParsingContext pc = new ParsingContext();
    pc.ps = result;
    pc.project = project;
    pc.logger = logger;
    Phase phase = PHASE_START;
    for (String line : content.split("\n")) {
      if (line.startsWith("***")) {
        // only three stars is a shortcut to not depend on length of those lines
        phase = phase.getNextPhase();
        phase.reset();
        continue;
      }
//      logger.debug("{} handles >{}<", phase.name, line);
      if (line.trim().isEmpty()) continue;
      phase.handle(pc, line);
    }
    if (phase != PHASE_END) {
      logger.warn("Parser ended unexpectedly in phase {}", phase.name);
    }
    result.flushTreeCache();
    return result;
  }
}
