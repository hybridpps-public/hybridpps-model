package de.tudresden.inf.st.hybridpps.parser.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.util.List;

/**
 * Serialization object for a project.
 *
 * @author rschoene - Initial contribution
 */
public class DataProject {
  public String name;
  @JacksonXmlElementWrapper(useWrapping = false)
  public List<DataJob> job;
  @JacksonXmlElementWrapper(localName = "resources")
  public List<Integer> resource;
  public int cpd;
  public int makespan;
}
