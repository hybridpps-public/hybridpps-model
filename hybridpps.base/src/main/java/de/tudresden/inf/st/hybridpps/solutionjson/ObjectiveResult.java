package de.tudresden.inf.st.hybridpps.solutionjson;

import java.util.List;

/**
 * Json class for solution serialisation.
 * <p> This class represents the result of a certain objective of a parent project</p>
 * <p> The name of the objective has to be retrieved from the parent referring to this class</p>
 * @see ProjectResult
 * <p> In a future release, this class may be defined by a jastadd model.</p>
 */
public class ObjectiveResult {

    /**
     * Name of the objective-function.
     * <p>Note that the provided string value does not necessary match the problem description but is the string value
     * of the internal enum representation. See problem.jrag for possible enum values.</p>
     */
    public String function;

    /**
     * Array of the values calculated for the objective.
     * <p>In case that the simulation is stochastic and the objective-function is none, a value per run is returned.</p>
     * <p>In all other cases, this array contains only a single value.</p>
     */
    public List<Integer> values;
}
