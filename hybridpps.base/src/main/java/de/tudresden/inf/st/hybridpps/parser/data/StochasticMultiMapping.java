package de.tudresden.inf.st.hybridpps.parser.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedList;

public class StochasticMultiMapping {

    @JsonProperty("stochastic_mapping")
    public LinkedList<MultiMapping> stochasticMappings;
}
