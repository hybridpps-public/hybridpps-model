package de.tudresden.inf.st.hybridpps.solutionjson;

import java.util.List;

/**
 * Json class for solution serialisation.
 * <p> This class is the entry point for the solution of a single problem.</p>
 * <p> In a future release, this class may be defined by a jastadd model.</p>
 */
public class DataSolution {

    /**
     * Problem name taken directly from the problem specification
     */
    public String problem;

    /**
     * List of results grouped by the scheduling strategy.
     * <p>If only one strategy is used (default case) this list contains a single element.</p>
     * @see DataStrategyInstance
     */
    public List<DataStrategyInstance> strategies;
}
