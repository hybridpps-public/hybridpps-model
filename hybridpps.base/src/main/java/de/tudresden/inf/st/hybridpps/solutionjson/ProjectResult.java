package de.tudresden.inf.st.hybridpps.solutionjson;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

/**
 * Json class for solution serialisation.
 * <p> This class represents the solution of a single project part of a parent problem scheduled with a certain
 * scheduling strategy.</p>
 * <p> In a future release, this class may be defined by a jastadd model.</p>
 */
public class ProjectResult {

    /**
     * In the implemented multi-objective case, a project can possess multiple different objectives with varying
     * objective-functions (accumulators).
     * <p>This mapping maps each combination of objective and objective function to its result.</p>
     * <p>The mapping key has the format "objective_name"_"objective_function".
     * For example "makespan_mean" or "project_delay_average"</p>
     * <p>Note that the names used for the objective and objective function are not necessary the ones specified in
     * the problem file. The implementation uses the string values of internal enum representations, while the problem
     * input allows multiple synonymous options for each string value. The possible output values are provided in problem.jrag</p>
     * @see ObjectiveResult
     */
    @JsonProperty("objective_result")
    public Map<String, ObjectiveResult> objectiveResults;
}
