package de.tudresden.inf.st.hybridpps.solutionjson.objcalculation;

/**
 * Concrete Accumulator to calculate the sum of elements from a double-valued array.
 */
public class Sum implements Accumulator {

    @Override
    public String getName() {
        return "sum";
    }

    /**
     * Calculate the sum of values from a double-valued array. If the array is empty, 0 is returned.
     *
     * @param values array of values to calculate the sum from. Note that signs are kept.
     * @return sum of values
     */
    @Override
    public double calculate(double[] values) {
        double result = 0;
        for (double value : values) {
            result += value;
        }
        return result;
    }

}
