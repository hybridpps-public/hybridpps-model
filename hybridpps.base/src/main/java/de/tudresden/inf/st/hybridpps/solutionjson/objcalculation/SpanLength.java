package de.tudresden.inf.st.hybridpps.solutionjson.objcalculation;

import java.util.Arrays;

/**
 * Concrete Accumulator to get the span of values of a double-valued array.
 */
public class SpanLength implements Accumulator {

    @Override
    public String getName() {
        return "span_length";
    }

    /**
     * Get the value-span. If only one element is present, 0 is returned.
     *
     * @param values array of double values, must contain at least one element
     * @return min-max span of double-values
     */
    @Override
    public double calculate(double[] values) {
        if (values.length == 0) throw new IllegalArgumentException("Cannot calculate span of zero sized array");
        if (values.length == 1) return 0;
        Arrays.sort(values);
        return values[values.length - 1] - values[0];
    }

}
