package de.tudresden.inf.st.hybridpps.parser.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.util.List;

/**
 * Serialization object for the intermediate element mp.
 *
 * @author rschoene - Initial contribution
 */
public class DataMp {
  public String name;
  @JacksonXmlElementWrapper(localName = "project-list")
  public List<DataProject> project;
  @JacksonXmlElementWrapper(localName = "resources")
  public List<Integer> resource;
}
