package de.tudresden.inf.st.hybridpps.solutionjson.objcalculation;

import java.util.Arrays;

/**
 * Concrete Accumulator to calculate the median of a double-valued array.
 */
public class Median implements Accumulator {

    @Override
    public String getName() {
        return "median";
    }

    /**
     * Calculate the median of a double-valued array. The array must at least contain a single element.
     * <p> Note: if the number of elements is uneven, the true median is returned. If the number of elements is even, the
     * mean of the two central elements is returned.
     *
     * @param values double-valued array to calculate the median from
     * @return the median following the above description
     */
    @Override
    public double calculate(double[] values) {
        if (values.length == 0) throw new IllegalArgumentException("Cannot calculate median of zero sized array");
        Arrays.sort(values);
        if (values.length % 2 == 0)
            return (values[values.length / 2] + values[values.length / 2 - 1]) / 2;
        else
            return values[values.length / 2];
    }

}
