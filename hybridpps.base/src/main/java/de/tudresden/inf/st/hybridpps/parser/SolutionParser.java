package de.tudresden.inf.st.hybridpps.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import de.tudresden.inf.st.hybridpps.jastadd.model.*;
import de.tudresden.inf.st.hybridpps.parser.data.DataJob;
import de.tudresden.inf.st.hybridpps.parser.data.DataProject;
import de.tudresden.inf.st.hybridpps.parser.data.DataSolution;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Parser for solution (XML).
 *
 * @author rschoene - Initial contribution
 */
public class SolutionParser {

  private static final Logger logger = LogManager.getLogger(SolutionParser.class);

  /**
   * Parse a XML file at the given URL into data used to construct a solution
   * @param url location of the file to read in
   * @return a representation of the data of the solution
   * @throws IOException if the file can not be accessed or parsed
   * @see #createSolution(DataSolution, int, ProductionSystem) createSolution
   */
  public static DataSolution parseSolutionXML(URL url) throws IOException {
    return parseSolutionXML(new File(url.getFile()));
  }

  /**
   * Parse a XML file into data used to construct a solution
   * @param file the file to read in
   * @return a representation of the data of the solution
   * @throws IOException if the file can not be accessed or parsed
   * @see #createSolution(DataSolution, int, ProductionSystem) createSolution
   */
  public static DataSolution parseSolutionXML(File file) throws IOException {
    ObjectMapper objectMapper = new XmlMapper();
    return objectMapper.readValue(file, DataSolution.class);
  }

  /**
   * Create a solution from the first project in a parsed solution and a given problem model.
   * @param dataSolution data of the solution to process
   * @param problemModel the given problem model
   * @return a newly generated solution
   */
  public static ConcreteSolution createSolution(DataSolution dataSolution, ProductionSystem problemModel) {
    return createSolution(dataSolution, 0, problemModel);
  }

  /**
   * Create a solution from the project with the given index in a parsed solution and a given problem model.
   * @param dataSolution data of the solution to process
   * @param projectIndex index of the project to use within the data of the solution
   * @param problemModel the given problem model
   * @return a newly generated solution
   */
  public static ConcreteSolution createSolution(DataSolution dataSolution, int projectIndex, ProductionSystem problemModel) {
    ConcreteSolution result = new ConcreteSolution();
    result.setProductionSystem(problemModel);

    // TODO process data object
    DataProject dataProject = dataSolution.mp.project.get(projectIndex);
    Project problemProject = problemModel.getProject(0);
    if (!dataProject.name.equals(problemProject.getName())) {
      logger.warn("Project name in solution file ('{}') differs from one in problem model ('{}')",
          dataProject.name, problemProject.getName());
    }
    for (DataJob dataJob : dataProject.job) {
      Mapping m = new Mapping();
      m.setStartTime(dataJob.start);
      // find job in problem model
      Job job = problemProject.resolveJob(Integer.toString(dataJob.nr));
      if (job == null) {
        logger.error("Could not find job '{}', skipping", dataJob.nr);
        continue;
      }
      // use first mode per default
      m.setMode(job.getMode(0));
      result.addMapping(m);
    }
    return result;
  }
}
