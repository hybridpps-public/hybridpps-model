package de.tudresden.inf.st.hybridpps.solutionjson.objcalculation;

/**
 * Concrete Accumulator to calculate the mean of a double-valued array.
 */
public class Mean implements Accumulator {

    @Override
    public String getName() {
        return "mean";
    }

    /**
     * Calculate the mean of a double-valued array.
     * The array must at least contain a single element.
     *
     * @param values double values to calculate the (arithmetic) mean
     * @return mean of all elements after (sum of elements)/(number of elements)
     */
    @Override
    public double calculate(double[] values) {
        if (values.length == 0) throw new IllegalArgumentException("Cannot calculate mean of zero sized array");
        return (new Sum()).calculate(values) / (double) values.length;
    }

}
