package de.tudresden.inf.st.hybridpps.parser.data;

/**
 * Serialization object for a solution.
 *
 * @author rschoene - Initial contribution
 */
public class DataSolution {
  public DataMp mp;
}
