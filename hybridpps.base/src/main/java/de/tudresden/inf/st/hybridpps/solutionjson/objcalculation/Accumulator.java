package de.tudresden.inf.st.hybridpps.solutionjson.objcalculation;

public interface Accumulator {
    String getName();

    double calculate(double[] values);
}
