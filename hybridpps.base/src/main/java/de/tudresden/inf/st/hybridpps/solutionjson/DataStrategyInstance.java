package de.tudresden.inf.st.hybridpps.solutionjson;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

/**
 * Json class for solution serialisation.
 * <p> This class represents the solution of the parent problem simulated with a certain scheduling strategy.</p>
 * <p> In a future release, this class may be defined by a jastadd model.</p>
 */
public class DataStrategyInstance {

    /**
     * Name of the used scheduling strategy to produce the solution.
     * <p>The name is lower-case as taken from the SchedulingStrategy class in case of a simple strategy
     * or "cpr" or "metarule" if one of them is used.</p>
     */
    public String strategy;

    /**
     * Output of the total makespan of the simulated problem.
     * This list of values is not objective-dependent but serves for debugging and verification.
     * <p>If deterministic, the array reports only a single element.</p>
     * <p>If stochastic, the array contains a value for each run.</p>
     */
    @JsonProperty("total_makespan")
    public List<Integer> totalMakespan;

    /**
     * Project-wise mapping of objective-specific results.
     * <p>The mapping is in the form "projectName -> Result" where projectName is taken form the
     * problem description.</p>
     * @see ProjectResult
     */
    public Map<String, ProjectResult> projects;
}
