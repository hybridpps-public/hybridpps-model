package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.jastadd.model.DeterministicSolution;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.WeightedAttributes;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.parameters.CPRLoader;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.*;

public class SchedulingStrategiesTest extends AbstractSimulationTest {

    @Test
    public void fifoTest() {
        DeterministicSolution solution = runDeterministicTest("schedulingStrategiesTests/fifoTest.json", new FIFO());

        assertThatSolution(solution).containsExactly(
                tuple("0", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("0", "3", "1", 3),
                tuple("0", "4", "1", 6)
        );
    }


    @Test
    public void lifoTest() {
        DeterministicSolution solution = runDeterministicTest("schedulingStrategiesTests/lifoTest.json", new LIFO());

        assertThatSolution(solution).containsExactly(
                tuple("0", "1", "1", 0),
                tuple("0", "3", "1", 0),
                tuple("0", "2", "1", 3),
                tuple("0", "4", "1", 6)
        );
    }

    @Test
    public void sptTest() {
        DeterministicSolution solution = runDeterministicTest("schedulingStrategiesTests/sptTest.json", new SPT());

        assertThatSolution(solution).containsExactly(
                tuple("0", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("0", "5", "1", 4),
                tuple("0", "3", "1", 7),
                tuple("0", "4", "1", 11),
                tuple("0", "6", "1", 19)
        );
    }

    @Test
    public void mwkrTest() {
        DeterministicSolution solution = runDeterministicTest("schedulingStrategiesTests/mwkrTest.json", new MWKR());

        assertThatSolution(solution).containsExactly(
                tuple("0", "1", "1", 0),
                tuple("1", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("1", "2", "1", 3),
                tuple("0", "3", "1", 6),
                tuple("0", "4", "1", 8),
                tuple("1", "3", "1", 11),
                tuple("0", "5", "1", 11),
                tuple("1", "4", "1", 14)
        );
    }

    @Test
    public void cprTest() {
        WeightedAttributes weightedAttributes = new WeightedAttributes();
        weightedAttributes.put("direct_successor_count", 0.5);
        weightedAttributes.put("duration", -0.2);
        weightedAttributes.put("setup_needed", -2.0);

        DeterministicSolution solution = runDeterministicTest("schedulingStrategiesTests/cprTest.json", new CombinedPriorityRule(weightedAttributes));

        assertThatSolution(solution).containsExactly(
                tuple("0", "1", "1", 0),
                tuple("0", "6", "1", 0),
                tuple("0", "4", "1", 0),
                tuple("0", "5", "1", 3),
                tuple("0", "2", "1", 5),
                tuple("0", "3", "1", 11),
                tuple("0", "7", "1", 21),
                tuple("0", "8", "1", 22)
        );
    }

    @Test
    public void readCPRTest1() throws IOException {
        /* In this test a scheduling strategy file defining a CPR is used. The loader should throw a
        NullPointerException as no parameters are defined. */
        Path path = Paths.get("src", "test", "resources", "schedulingStrategiesTests/strategies/cpr1.json");
        assertEquals(0, CPRLoader.load(path).getWeightedAttributes().getWeights().size());
    }

    @Test
    public void readCPRTest2() throws IOException {
        /* In this test a scheduling strategy file defining a CPR is used. The test checks if the correct amount of
        parameters is loaded. Additionally it tests that attributes which are not defined in the .json also don't end up
        in the scheduling strategy. */
        Path path = Paths.get("src", "test", "resources", "schedulingStrategiesTests/strategies/cpr2.json");
        SchedulingStrategy schedulingStrategy = CPRLoader.load(path);

        assertNotNull(schedulingStrategy);

        WeightedAttributes weightedAttributes = ((CombinedPriorityRule) schedulingStrategy).getWeightedAttributes();

        assertEquals(3, weightedAttributes.getWeights().size());
        assertEquals(-0.2, weightedAttributes.getWeightForAttribute("duration"));
        assertNull(weightedAttributes.getWeightForAttribute("resources"));
    }

    @Test
    public void cprFromFileTest() {
        DeterministicSolution solution = runDeterministicTest(
                "schedulingStrategiesTests/cprTest.json",
                "schedulingStrategiesTests/strategies/cpr2.json");

        assertThatSolution(solution).containsExactly(
                tuple("0", "1", "1", 0),
                tuple("0", "6", "1", 0),
                tuple("0", "4", "1", 0),
                tuple("0", "5", "1", 3),
                tuple("0", "2", "1", 5),
                tuple("0", "3", "1", 11),
                tuple("0", "7", "1", 21),
                tuple("0", "8", "1", 22)
        );
    }
}
