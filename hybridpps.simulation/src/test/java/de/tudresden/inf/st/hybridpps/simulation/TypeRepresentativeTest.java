package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.jastadd.model.DeterministicSolution;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.tuple;

public class TypeRepresentativeTest extends AbstractSimulationTest {

    @Test
    public void typeRepresentativeTest() {
        /* In this test J2 and J3 run on a different ResourceGroup and have different TRs, so both will have to setup. */
        DeterministicSolution solution = runDeterministicTest("typeRepresentativeTests/typeRepresentativeTest.json");

        assertThatSolution(solution).containsExactlyInAnyOrder(
                tuple("0", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("0", "3", "1", 4),
                tuple("0", "4", "1", 13)
        );
    }

    @Test
    public void typeRepresentativeTest2() {
        /* In this test both J2 and J3 run on the same resources with the same TR, so no setup time is needed for J3*/
        DeterministicSolution solution = runDeterministicTest("typeRepresentativeTests/typeRepresentativeTest2.json");

        assertThatSolution(solution).containsExactlyInAnyOrder(
                tuple("0", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("0", "3", "1", 4),
                tuple("0", "4", "1", 8)
        );
    }

    @Test
    public void typeRepresentativeTest3() {
        /* In this test J3 needs one more of R1 than J2 which means that the setup time is added */
        DeterministicSolution solution = runDeterministicTest("typeRepresentativeTests/typeRepresentativeTest3.json");

        assertThatSolution(solution).containsExactlyInAnyOrder(
                tuple("0", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("0", "3", "1", 4),
                tuple("0", "4", "1", 9)
        );
    }

    @Test
    public void typeRepresentativeTest4() {
        /* In this test J2 and J3 run on the same ResourceGroup but have different TRs, so both Jobs will have to setup. */
        DeterministicSolution solution = runDeterministicTest("typeRepresentativeTests/typeRepresentativeTest4.json");

        assertThatSolution(solution).containsExactlyInAnyOrder(
                tuple("0", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("0", "3", "1", 4),
                tuple("0", "4", "1", 13)
        );
    }

    @Test
    public void typeRepresentativeTest5() {
        /* In this test J2 and J3 have the same TR but run on different ResourceGroups, so both Jobs will have to setup. */
        DeterministicSolution solution = runDeterministicTest("typeRepresentativeTests/typeRepresentativeTest5.json");

        assertThatSolution(solution).containsExactlyInAnyOrder(
                tuple("0", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("0", "3", "1", 4),
                tuple("0", "4", "1", 9)
        );
    }
}
