package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.jastadd.model.*;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.*;
import de.tudresden.inf.st.hybridpps.solutionjson.*;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.*;

public class BasicFunctionalityTest extends AbstractSimulationTest {

    private final int STOCHASTIC_COUNT = 10;

    @Test
    public void modeTest() {
        /* Tests that the Simulation is able to handle multiple modes. J3M1 can't be scheduled as J2M1 takes all
        resources of type R1, so J3M2 is supposed to be scheduled. */
        AbstractSolution solution = runDeterministicTest("basicFunctionalityTests/modeTest.json");

        assertThatSolution(solution).containsExactlyInAnyOrder(
                tuple("0", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("0", "3", "2", 0),
                tuple("0", "4", "1", 5)
        );
    }

    @Test
    public void globalResourceTest() {
        /* Tests that the Simulation is able to handle global resources. Both projects have 5 instances of R1 and each
        job needs 5 resources of R1 to be scheduled, so jobs J2 and J3 can't be scheduled in parallel. */
        AbstractSolution solution = runDeterministicTest("basicFunctionalityTests/globalResourceTest.json");

        assertThatSolution(solution).containsExactlyInAnyOrder(
                tuple("0", "1", "1", 0),
                tuple("1", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("0", "3", "1", 3),
                tuple("1", "2", "1", 6),
                tuple("0", "4", "1", 6),
                tuple("1", "3", "1", 9),
                tuple("1", "4", "1", 12)

        );
    }

    @Test
    public void globalResourceTest2() {
        /* Tests that the Simulation is able to handle global resources. Both projects share the global resource R1. P1
        * has 5 local instances of R1 while P2 has 10, so there should be 10 global instances of R1. Therefore P1J2 and
        * P1J3 can be scheduled at the same time. */
        AbstractSolution solution = runDeterministicTest("basicFunctionalityTests/globalResourceTest2.json");

        assertThatSolution(solution).containsExactlyInAnyOrder(
                tuple("0", "1", "1", 0),
                tuple("1", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("0", "3", "1", 0),
                tuple("1", "2", "1", 3),
                tuple("0", "4", "1", 3),
                tuple("1", "3", "1", 6),
                tuple("1", "4", "1", 9)

        );
    }

    @Test
    public void multipleObjectivesTest() {
        /* Tests that the Simulation can handle multi-objective scenarios. Since the current scenario is detereministic,
        * the number of values for each objective must be 1.*/
        AbstractSolution solution = runDeterministicTest("basicFunctionalityTests/multipleObjectivesTest.json");

        SimplePriorityRule strategy = new FIFO();
        DataStrategyInstance dsi = solution.getDataSolution(strategy.getName());

        ProjectResult randomProject = dsi.projects.values().stream().findAny().get();
        assertTrue(randomProject.objectiveResults.size() > 1);
        assertTrue(randomProject.objectiveResults.values().stream().findAny().get().values.size() == 1);
    }

    @Test
    public void stochasticDurationTest() {
        /* Tests that the model works with stochastic job durations. J2M1 is stochastically distributed with mean
        duration 3. This method checks if there end up being multiple different values for the job duration. */
        Project project = getProject("basicFunctionalityTests/stochasticDurationTest.json", 0);
        List<Double> capacities = project.getJob(1).getMode(0).getDuration().unrolledNumbers().getNumbers();

        assertEquals(STOCHASTIC_COUNT, capacities.size());
        assertTrue(new HashSet<>(capacities).size() > 1);
    }

    @Test
    public void stochasticDurationTest2() {
        /* Tests that the model works with stochastic job durations. J2M1 is stochastically distributed with mean
        duration 3. In the end there should be multiple different end times of the simulation based on the mode
        duration. */
        StochasticSolution solution = runStochasticTest("basicFunctionalityTests/stochasticDurationTest.json");
        JastAddList<ConcreteSolution> concreteSolutions = solution.getConcreteSolutions();

        List<Integer> endTimes = new ArrayList<>();
        for (ConcreteSolution concreteSolution : concreteSolutions) {
            endTimes.add(concreteSolution.getMapping(concreteSolution.getNumMapping() - 1).getStartTime());
        }
        assertTrue(new HashSet<>(endTimes).size() > 1);
    }

    @Test
    public void stochasticResourceTest() {
        /* Tests that the model works with stochastic resource availabilities. The capacity of resource R1 is
        stochastically distributed with mean 10. This method checks if there end up being multiple different values for
        the resource capacity. */
        Project project = getProject("basicFunctionalityTests/stochasticResourceTest.json", 0);
        List<Double> capacities = project.getResourceGroup(0).getCapacity().unrolledNumbers().getNumbers();

        assertEquals(STOCHASTIC_COUNT, capacities.size());
        assertTrue(new HashSet<>(capacities).size() > 1);
    }

    @Test
    public void stochasticResourceTest2() {
        /* Tests that the model works with stochastic resource availabilities. The capacity of resource R1 is
        stochastically distributed with mean 10. Both J2M1 and J3M1 need 5 of R1. If the availability of R1 is >= 10
        then both modes can run in parallel, otherwise they run sequential. */
        StochasticSolution solution = runStochasticTest("basicFunctionalityTests/stochasticResourceTest.json");
        JastAddList<ConcreteSolution> concreteSolutions = solution.getConcreteSolutions();

        List<Integer> endTimes = new ArrayList<>();
        for (ConcreteSolution concreteSolution : concreteSolutions) {
            endTimes.add(concreteSolution.getMapping(concreteSolution.getNumMapping() - 1).getStartTime());
        }
        // in parallel the modes need a time of 4, sequential they need a time of 7
        assertEquals(new HashSet<Integer>(){{ add(4); add(7);}}, new HashSet<>(endTimes));
    }

    @Test
    public void stochasticSetupTimeTest() {
        /* Tests that the model works with stochastic setup times. The setup time of TR1 is normally distributed with a
        min of 1, a max of 10 and therefore a mean of 5.5. This method checks if there end up being multiple different
        values for the setup time of the type representative. */
        ProductionSystem productionSystem = getProductionSystem("basicFunctionalityTests/stochasticSetupTimeTest.json");
        List<Double> setupTimes = productionSystem.getTypeRepresentative(0).getSetupTime().unrolledNumbers().getNumbers();

        assertEquals(STOCHASTIC_COUNT, setupTimes.size());
        assertTrue(new HashSet<>(setupTimes).size() > 1);
    }

    @Test
    public void stochasticSetupTimeTest2() {
        /* Tests that the model works with stochastic setup times. The setup time of TR1 is normally distributed with a
        min of 1, a max of 10 and therefore a mean of 5.5. In the end there should be multiple different end times of
        the simulation based on the setup time of the type representative. */
        StochasticSolution solution = runStochasticTest("basicFunctionalityTests/stochasticSetupTimeTest.json");
        JastAddList<ConcreteSolution> concreteSolutions = solution.getConcreteSolutions();

        List<Integer> endTimes = new ArrayList<>();
        for (ConcreteSolution concreteSolution : concreteSolutions) {
            endTimes.add(concreteSolution.getMapping(concreteSolution.getNumMapping() - 1).getStartTime());
        }
        assertTrue(new HashSet<>(endTimes).size() > 1);
    }

    @Test
    public void stochasticDistributionsTest() {
        /* Tests that all unrolled values are correctly distributed. */
        ProductionSystem productionSystem = getProductionSystem("basicFunctionalityTests/stochasticDistributionsTest.json");
        Project project = productionSystem.getProject(0);

        List<Double> resourceDurations = project.getJob(1).getMode(0).getDuration().unrolledNumbers().getNumbers();
        List<Double> resourceCapacities = project.getResourceGroup(0).getCapacity().unrolledNumbers().getNumbers();
        List<Double> setupTimes = productionSystem.getTypeRepresentative(0).getSetupTime().unrolledNumbers().getNumbers();
        // resource duration should have mean of 3 and cv of 0.2
        double mean = calculateMean(resourceDurations);
        double cv = calculateCV(resourceDurations, mean);
        assertTrue(isClose(mean, 3.0), "Mean should be close to 3 but was " + mean);
        assertTrue(isClose(cv, 0.2), "CV should be close to 0.2 but was " + cv);
        // resource capacity should have mean of 10 and cv of 0.2
        mean = calculateMean(resourceCapacities);
        cv = calculateCV(resourceCapacities, mean);
        assertTrue(isClose(mean, 10.0), "Mean should be close to 10 but was " + mean);
        assertTrue(isClose(cv, 0.2), "CV should be close to 0.2 but was " + cv);
        // setup should have mean of 5.5 and cv of 0.2
        mean = calculateMean(setupTimes);
        cv = calculateCV(setupTimes, mean);
        assertTrue(isClose(mean, 5.5), "Mean should be close to 5.5 but was " + mean);
        assertTrue(isClose(cv, 0.2), "CV should be close to 0.2 but was " + cv);
    }

    private Double calculateMean(List<Double> numbers) {
        double sum = 0;
        for (Double t : numbers) {
            sum += t;
        }
        return sum / numbers.size();
    }

    private Double calculateCV(List<Double> numbers, Double mean) {
        double error = 0;
        for (Double n : numbers) {
            error += Math.pow(n - mean, 2);
        }
        double variance = error / numbers.size();
        return Math.sqrt(variance) / mean;
    }

    private Boolean isClose(Double n1, Double n2) {
        return Math.abs(n1 - n2) < Math.min(n1, n2) / 20;
    }

    @Test
    public void stochasticMultipleObjectivesTest() {
        /* Tests that the Simulation can handle multi-objective scenarios. Since the current scenario is detereministic,
         * the number of values for each objective must be 1.*/
        AbstractSolution solution = runStochasticTest("basicFunctionalityTests/stochasticMultipleObjectivesTest.json");

        SimplePriorityRule strategy = new FIFO();
        DataStrategyInstance dsi = solution.getDataSolution(strategy.getName());

        ProjectResult randomProject = dsi.projects.values().stream().findAny().get();
        assertTrue(randomProject.objectiveResults.size() > 1);
        assertTrue(randomProject.objectiveResults.values().stream().findAny().get().values.size() == 100);
    }
}
