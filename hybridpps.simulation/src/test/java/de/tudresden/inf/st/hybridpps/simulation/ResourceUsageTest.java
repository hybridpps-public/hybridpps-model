package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.jastadd.model.DeterministicSolution;
import de.tudresden.inf.st.hybridpps.jastadd.model.StochasticSolution;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.tuple;

public class ResourceUsageTest extends AbstractSimulationTest {

    @Test
    public void advancedResourceTest() {
        /* In this test there are 3 TRs and 2 Resources, job 7 and 8 can be done in parallel. Job 4 needs no setup. */
        DeterministicSolution solution = runDeterministicTest("resourceUsageTest.json");

        assertThatSolution(solution).containsExactlyInAnyOrder(
                tuple("0", "1", "1", 0),
                tuple("0", "2", "1", 0),  // 1 + 1
                tuple("0", "3", "1", 2),  // 1 + 1
                tuple("0", "4", "1", 4),  // 1
                tuple("0", "5", "1", 5),  // 1 + 5
                tuple("0", "6", "1", 11), // 1 + 5
                tuple("0", "7", "1", 17), // 1 + 10
                tuple("0", "8", "1", 17), // 1 + 1
                tuple("0", "9", "1", 28), // 1 + 10
                tuple("0", "10", "1", 39)
        );

        List<Map<String, List<Integer>>> expectedResourceUsage = new ArrayList<>();

        expectedResourceUsage.add(new HashMap<>());
        expectedResourceUsage.add(createResourceUsageMap("R1", Arrays.asList(0, 1, 2, 3))); // TR1
        expectedResourceUsage.add(createResourceUsageMap("R1", Arrays.asList(4, 5, 0, 1, 2))); // TR1
        // takes 3, 4, 5 as they are the longest in the queue
        expectedResourceUsage.add(createResourceUsageMap("R1", Arrays.asList(3, 4, 5))); // TR1
        expectedResourceUsage.add(createResourceUsageMap("R1", Arrays.asList(0))); // TR2
        expectedResourceUsage.add(createResourceUsageMap("R1", Arrays.asList(0, 1))); // TR2

        expectedResourceUsage.add(createResourceUsageMap(
                Arrays.asList("R1", "R2"),
                Arrays.asList(Arrays.asList(2, 3, 4), Arrays.asList(0, 1)))); // TR3

        expectedResourceUsage.add(createResourceUsageMap(
                Arrays.asList("R1", "R2"),
                Arrays.asList(Arrays.asList(5, 0, 1), Arrays.asList(2)))); // TR1

        expectedResourceUsage.add(createResourceUsageMap(
                Arrays.asList("R1", "R2"),
                Arrays.asList(Arrays.asList(2, 3, 4, 5), Arrays.asList(3, 4, 5)))); // TR3

        expectedResourceUsage.add(new HashMap<>());

        assertResourceUsage(solution, expectedResourceUsage);
    }

    private Map<String, List<Integer>> createResourceUsageMap(String group, List<Integer> resourceIndexes) {
        return createResourceUsageMap(Collections.singletonList(group), Collections.singletonList(resourceIndexes));
    }

    public Map<String, List<Integer>> createResourceUsageMap(List<String> groups, List<List<Integer>> resourceIndexesList) {
        Map<String, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < groups.size(); i++) {
            map.put(groups.get(i), resourceIndexesList.get(i));
        }
        return map;
    }
}
