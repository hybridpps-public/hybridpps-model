package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.extend.ExtensionProcess;
import de.tudresden.inf.st.hybridpps.jastadd.model.*;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.FIFO;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.SchedulingStrategy;
import org.assertj.core.api.AbstractListAssert;
import org.assertj.core.api.ObjectAssert;
import org.assertj.core.groups.Tuple;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class AbstractSimulationTest {

    AbstractListAssert<?, List<? extends Tuple>, Tuple, ObjectAssert<Tuple>> assertThatSolution(AbstractSolution solution) {
        ConcreteSolution concreteSolution = getConcreteSolution(solution);

        return assertThat(concreteSolution.getMappingList()).extracting(
                m -> m.getMode().containingJob().containingProject().getName(),
                m -> m.getMode().containingJob().getName(),
                m -> m.getMode().getName(),
                Mapping::getStartTime
        );
    }

    void assertResourceUsage(AbstractSolution abstractSolution, List<Map<String, List<Integer>>> expectedResourceUsages) {
        ConcreteSolution solution = getConcreteSolution(abstractSolution);
        assertEquals(expectedResourceUsages.size(), solution.getNumMapping());

        for (int i = 0; i < solution.getNumMapping(); i++) {
            JastAddList<ResourceUsage> realResourceUsages = solution.getMapping(i).getResourceUsages();
            Map<String, List<Integer>> expectedMapping = expectedResourceUsages.get(i);

            assertEquals(expectedMapping.size(), realResourceUsages.getNumChild());

            for (int j = 0; j < realResourceUsages.getNumChild(); j++) {
                ResourceUsage realResourceUsage = realResourceUsages.getChild(j);
                String resourceGroup = realResourceUsage.getResourceGroup().getName();
                List<Integer> resources = realResourceUsage.getResourceIndexes();

                assertEquals(expectedMapping.get(resourceGroup), resources);
            }
        }
    }

    ConcreteSolution getConcreteSolution(AbstractSolution solution) {
        if (solution instanceof DeterministicSolution) {
            return ((DeterministicSolution) solution).getConcreteSolution();
        } else {
            return ((StochasticSolution) solution).getConcreteSolution(0);
        }
    }

    public static Project getProject(String inputJsonName, int projectIndex) {
        return getProductionSystem(inputJsonName).getProject(projectIndex);
    }

    public static ProductionSystem getProductionSystem(String inputJsonName) {
        ExtensionProcess process = new ExtensionProcess();
        process.setPathToInputJson(Paths.get("src", "test", "resources", inputJsonName));
        process.run();
        return process.getProductionSystem();
    }



    DeterministicSolution runDeterministicTest(String inputJsonName) {
        return runDeterministicTest(inputJsonName, new FIFO());
    }

    DeterministicSolution runDeterministicTest(String inputJsonName, SchedulingStrategy strategy) {
        Path path = Paths.get("src", "test", "resources", inputJsonName);

        return SolutionGenerator.generateDeterministicSolution(path, strategy);
    }

    DeterministicSolution runDeterministicTest(String inputJsonName, String strategyJsonName) {
        Path inputPath = Paths.get("src", "test", "resources", inputJsonName);
        Path strategyPath = Paths.get("src", "test", "resources", strategyJsonName);

        DeterministicSolution solution;
        try {
            solution = SolutionGenerator.generateDeterministicSolution(inputPath, strategyPath);
        } catch (IOException e) {
            throw new AssertionError();
        }
        return solution;
    }

    StochasticSolution runStochasticTest(String inputJsonName) {
        return runStochasticTest(inputJsonName, new FIFO());
    }

    StochasticSolution runStochasticTest(String inputJsonName, SchedulingStrategy strategy) {
        Path path = Paths.get("src", "test", "resources", inputJsonName);

        return SolutionGenerator.generateStochasticSolution(path, strategy);
    }
}
