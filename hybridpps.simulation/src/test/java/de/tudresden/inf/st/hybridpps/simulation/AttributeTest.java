package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.jastadd.model.ProductionSystem;
import de.tudresden.inf.st.hybridpps.simulation.model.*;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.AttributePool;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.FIFO;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes.QueryableModeAttribute;
import desmoj.core.simulator.Experiment;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AttributeTest extends AbstractSimulationTest {

    private static SimulationJobMode mode1;

    private static SimulationJobMode mode2;

    private static SimulationProblem problem;

    @Test
    public void testDirectSuccessorCount() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("direct_successor_count");
        Objects.requireNonNull(attribute);

        assertEquals(1, attribute.getValue(mode1, problem));
        assertEquals(2, attribute.getValue(mode2, problem));
    }

    @Test
    public void testDirectSuccessorSumMaxProcessingTime() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("direct_successor_sum_max_processing_time");
        Objects.requireNonNull(attribute);

        assertEquals(4, attribute.getValue(mode1, problem));
        assertEquals(12, attribute.getValue(mode2, problem));
    }

    @Test
    public void testDirectSuccessorSumMinProcessingTime() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("direct_successor_sum_min_processing_time");
        Objects.requireNonNull(attribute);

        assertEquals(3, attribute.getValue(mode1, problem));
        assertEquals(12, attribute.getValue(mode2, problem));
    }

    @Test
    public void testDuration() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("duration");
        Objects.requireNonNull(attribute);

        assertEquals(3, attribute.getValue(mode1, problem));
        assertEquals(6, attribute.getValue(mode2, problem));
    }

    @Test
    public void testProjectFinishTime() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("project_finish_time");
        Objects.requireNonNull(attribute);

        assertEquals(10, attribute.getValue(mode1, problem));
        assertEquals(15, attribute.getValue(mode2, problem));
    }

    @Test
    public void testProjectStartTime() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("project_start_time");
        Objects.requireNonNull(attribute);

        assertEquals(0, attribute.getValue(mode1, problem));
        assertEquals(0, attribute.getValue(mode2, problem));
    }

    @Test
    public void testResources() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("resources");
        Objects.requireNonNull(attribute);

        assertEquals(5, attribute.getValue(mode1, problem));
        assertEquals(10, attribute.getValue(mode2, problem));
    }

    @Test
    public void testSetupNeeded() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("setup_needed");
        Objects.requireNonNull(attribute);

        assertEquals(1, attribute.getValue(mode1, problem));
        assertEquals(1, attribute.getValue(mode2, problem));
    }

    @Test
    public void testSetupTime() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("setup_time");
        Objects.requireNonNull(attribute);

        assertEquals(2, attribute.getValue(mode1, problem));
        assertEquals(4, attribute.getValue(mode2, problem));
    }

    @Test
    public void testTotalSuccessorCount() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("total_successor_count");
        Objects.requireNonNull(attribute);

        assertEquals(2, attribute.getValue(mode1, problem));
        assertEquals(4, attribute.getValue(mode2, problem));
    }

    @Test
    public void testTotalSuccessorSumMaxProcessingTime() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("total_successor_sum_max_processing_time");
        Objects.requireNonNull(attribute);

        assertEquals(4, attribute.getValue(mode1, problem));
        assertEquals(19, attribute.getValue(mode2, problem));
    }

    @Test
    public void testTotalSuccessorSumMinProcessingTime() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("total_successor_sum_min_processing_time");
        Objects.requireNonNull(attribute);

        assertEquals(3, attribute.getValue(mode1, problem));
        assertEquals(17, attribute.getValue(mode2, problem));
    }

    @Test
    public void testWaitingJobCount() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("waiting_job_count");
        Objects.requireNonNull(attribute);

        assertEquals(1, attribute.getValue(mode1, problem));
        assertEquals(1, attribute.getValue(mode2, problem));
    }

    @Test
    public void testWaitingTime() {
        // can't really get tested better with the current way of testing
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("waiting_time");
        Objects.requireNonNull(attribute);

        assertEquals(0, attribute.getValue(mode1, problem));
        assertEquals(0, attribute.getValue(mode2, problem));
    }

    @BeforeAll
    private static void setUp() {
        ProductionSystem ps = getProductionSystem("attributeTest/attributeTest.json");
        RCPSP rcpsp = new RCPSP(ps, new FIFO(), null);

        Experiment exp = new Experiment("RCPSPExperiment");
        rcpsp.connectToExperiment(exp);

        rcpsp.init();
        rcpsp.getEventHandler().updateExecutableJobs();
        List<SimulationJobMode> modes = rcpsp.getEventHandler().findSchedulableModes();

        AttributeTest.mode1 = modes.get(0);
        AttributeTest.mode2 = modes.get(1);
        AttributeTest.problem = rcpsp;
    }
}
