package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.jastadd.model.DeterministicSolution;
import de.tudresden.inf.st.hybridpps.jastadd.model.StochasticSolution;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.tuple;

public class DESMOJTest extends AbstractSimulationTest {

   @Test
   public void test1() {
      DeterministicSolution solution = runDeterministicTest("handmadeTest1.json");

      assertThatSolution(solution).containsExactlyInAnyOrder(
          tuple("0", "1", "1", 0),
          tuple("0", "2", "1", 0),
          tuple("0", "3", "1", 3),
          tuple("0", "4", "1", 6)
      );
   }

   @Test
   public void test2() {
      DeterministicSolution solution = runDeterministicTest("handmadeTest2.json");

      assertThatSolution(solution).containsExactlyInAnyOrder(
          tuple("0", "1", "1", 0),
          tuple("0", "5", "1", 0),
          tuple("0", "3", "1", 6),
          tuple("0", "7", "1", 12),
          tuple("0", "2", "1", 15),
          tuple("0", "4", "1", 18),
          tuple("0", "6", "1", 27),
          tuple("0", "8", "1", 30)
      );
   }

   @Test
   public void test3() {
      DeterministicSolution solution = runDeterministicTest("handmadeTest3.json");

      assertThatSolution(solution).containsExactlyInAnyOrder(
          tuple("0", "1", "1", 0),
          tuple("0", "2", "1", 0),
          tuple("0", "3", "1", 0),
          tuple("0", "4", "1", 3),
          tuple("0", "5", "1", 12)
      );
   }

   @Test
   public void test4() {
      DeterministicSolution solution = runDeterministicTest("handmadeTest4.json");

      assertThatSolution(solution).containsExactlyInAnyOrder(
          tuple("0", "1", "1", 0),
          tuple("0", "2", "1", 0),
          tuple("0", "3", "1", 0),
          tuple("0", "4", "1", 2),
          tuple("0", "5", "1", 2),
          tuple("0", "6", "1", 4),
          tuple("0", "7", "1", 4),
          tuple("0", "8", "1", 6)
      );
   }
}
