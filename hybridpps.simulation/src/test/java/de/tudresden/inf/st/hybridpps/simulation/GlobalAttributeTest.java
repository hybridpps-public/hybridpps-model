package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;
import de.tudresden.inf.st.hybridpps.simulation.priority.SimulationState;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.AttributePool;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global.CombinedAttribute;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global.QueryableGlobalAttribute;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global.acc.Accumulator;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global.acc.AccumulatorPool;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes.QueryableModeAttribute;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.FIFO;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.TraceableStrategyContainer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GlobalAttributeTest extends AbstractSimulationTest {

    private static final double eps = 0.000001;

    private List<List<SimulationJobMode>> trace;
    private SimulationProblem simulationProblem;

    @BeforeEach
    void setUp() {
        TraceableStrategyContainer traceableStrategy = new TraceableStrategyContainer(new FIFO());
        runDeterministicTest("schedulingStrategiesTests/metaRuleTests/queueTest.json", traceableStrategy);
        trace = traceableStrategy.getTrace();
        simulationProblem = traceableStrategy.getSimulationProblem();
    }

    @Test
    public void testAccumulatorPool() {
        Map<String, Accumulator> acc = AccumulatorPool.getAccumulators();
        assertNotNull(acc.get("sum"));
        assertNotNull(acc.get("mean"));
        assertNotNull(acc.get("median"));
        assertNotNull(acc.get("span_length"));
        assertNotNull(acc.get("standard_deviation"));
    }

    @Test
    public void testGlobalAttributePoolReflection() {
        int numberGlobalAttributes = AttributePool.globalAttributes.size();
        assertEquals(4, numberGlobalAttributes);
    }

    @Test
    public void testGlobalAttributePoolNames() {
        assertNotNull(AttributePool.getGlobalAttribute("number_executable_modes"));
        assertNotNull(AttributePool.getGlobalAttribute("number_executable_jobs"));
        assertNotNull(AttributePool.getGlobalAttribute("number_executable_projects"));
        assertNotNull(AttributePool.getGlobalAttribute("foreach_setup_needed"));
    }

    @Test
    public void testNumberExecutableModes() {
        QueryableGlobalAttribute attribute = AttributePool.getGlobalAttribute("number_executable_modes");
        double res = attribute.getValue(trace.get(1), null);
        assertEquals(3, res, eps);
    }

    @Test
    public void testNumberExecutableJobs() {
        QueryableGlobalAttribute attribute = AttributePool.getGlobalAttribute("number_executable_jobs");
        double res = attribute.getValue(trace.get(1), null);
        assertEquals(2, res, eps);
    }

    @Test
    public void testNumberExecutableProjects() {
        QueryableGlobalAttribute attribute = AttributePool.getGlobalAttribute("number_executable_projects");
        double res = attribute.getValue(trace.get(1), null);
        assertEquals(1, res, eps);
    }

    @Test
    public void testForeachSetupTime() {
        QueryableGlobalAttribute attribute = AttributePool.getGlobalAttribute("foreach_setup_needed");
        double res = attribute.getValue(trace.get(1), simulationProblem);
        assertEquals(0, res, eps);
    }

    @Test
    public void testCombinedAttribute() {
        QueryableModeAttribute attribute = AttributePool.getModeAttribute("resources");
        Accumulator sum = AccumulatorPool.getAccumulators().get("sum");
        double res = CombinedAttribute.calculate(attribute, sum, trace.get(1), simulationProblem);
        assertEquals(9.5, res, eps);
    }

    @Test
    public void testQuerySimulationState() {
        SimulationState simulationState = new SimulationState(simulationProblem);
        simulationState.update(trace.get(1));
        double res = simulationState.query("sum:resources");
        assertEquals(9.5, res, eps);
    }

}
