package de.tudresden.inf.st.hybridpps.simulation;


import de.tudresden.inf.st.hybridpps.simulation.priority.input.RuleSet;
import de.tudresden.inf.st.hybridpps.simulation.priority.input.Statement;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class MetaRuleParserTest implements MetaRuleLoader {

    private String stripAll(String value){
        value = value.strip();
        value = value.replaceAll("\n", "");
        value = value.replaceAll("\r", "");
        value = value.replaceAll("\t", "");
        return value.replaceAll(" ", "");
    }

    /**
     * Tests a basic rule set with a single statement.
     * Checks that all parts are present in the model and the generated rule is correct.
     */
    @Test
    public void testSimpleStatement(){
        RuleSet ruleSet = load("metaRulesets/alwaysFIFO.json");

        assertEquals(1, ruleSet.getStatements().size());
        Statement statement = ruleSet.getStatements().get(0);

        assertEquals("a", statement.getGate());
        assertEquals(2, statement.getClauses().size());
        assertEquals(1, statement.getConditions().size());

        assertEquals("(($s.query(\"number_executable_modes\")>0.0)&($s.query(\"number_executable_modes\")<1000.0))",
                stripAll(statement.asRule("s")));
    }

    /**
     * Tests that a rule set can contain multiple statements and each results in an separate rule.
     */
    @Test
    public void testMultipleStatements(){
        RuleSet ruleSet = load("metaRulesets/multipleStatements.json");
        assertEquals(3, ruleSet.getStatements().size());
        assertEquals("($s.query(\"number_executable_modes\")>0.0)", stripAll(ruleSet.getStatements().get(0).asRule("s")));
        assertEquals("($s.query(\"number_executable_modes\")>1.0)", stripAll(ruleSet.getStatements().get(1).asRule("s")));
        assertEquals("($s.query(\"number_executable_modes\")>2.0)", stripAll(ruleSet.getStatements().get(2).asRule("s")));
    }

    /**
     * Tests that the rule generation can handle complex conditions.
     * Here the condition is nested two times and each sub-condition contains clauses and other conditions.
     */
    @Test
    public void testComplexCondition(){
        RuleSet ruleSet = load("metaRulesets/complexCondition.json");
        assertEquals("(" +
                        "($s.query(\"number_executable_modes\")>0.0)" +
                        "&" +
                        "!(" +
                            "($s.query(\"number_executable_modes\")<=50.0)" +
                            "^" +
                            "(" +
                                "($s.query(\"number_executable_modes\")>0.0)" +
                                "|" +
                                "($s.query(\"number_executable_modes\")<=50.0)" +
                                "|" +
                                "($s.query(\"number_executable_modes\")==10.12)" +
                            ")" +
                        ")" +
                        ")",
                stripAll(ruleSet.getStatements().get(0).asRule("s")));
    }

    /**
     * The clause array is empty, the returned rule must be null.
     * We expect at least one clause so that a gate can be specified.
     */
    @Test
    public void emptySpec(){
        try {
            String path = "metaRulesets/emptySpec.json";
            Path metaRuleDefinitionURI = Paths.get("src", "test", "resources", path);
            RuleSet ruleSet = RuleSet.fromJson(metaRuleDefinitionURI);
            fail();
        }catch (Exception e){
            assertNotNull(e);
        }
    }

    /**
     * A clause which is used by the gate uses an invalid comparator, the returned rule must be null.
     * Note that if unused clauses are invalid, the statement stays valid!
     */
    @Test
    public void testInvalidClause(){
        RuleSet ruleSet = load("metaRulesets/invalidClause.json");
        assertNull(ruleSet.getStatements().get(0).asRule("s"));
    }

    /**
     * Checks that a condition which references an other clause or condition which is not present to generate a null-rule.
     * Note that if invalid conditions are not used by the gate, the statement stays valid!
     */
    @Test
    public void testInvalidConditionMissingIndex(){
        RuleSet ruleSet = load("metaRulesets/invalidConditionMissingIndex.json");
        assertNull(ruleSet.getStatements().get(0).asRule("s"));
    }

    /**
     * Checks that a condition which uses an invalid operator must result in a null rule.
     * Note that if invalid conditions are not used by the gate, the statement stays valid!
     */
    @Test
    public void testInvalidConditionOperator(){
        RuleSet ruleSet = load("metaRulesets/invalidConditionOperator.json");
        assertNull(ruleSet.getStatements().get(0).asRule("s"));
    }


    /**
     * Checks that an invalid gate (not found index) must result in a null rule.
     */
    @Test
    public void testInvalidStatementGate(){
        RuleSet ruleSet = load("metaRulesets/invalidStatementGate.json");
        assertNull(ruleSet.getStatements().get(0).asRule("s"));
    }

}
