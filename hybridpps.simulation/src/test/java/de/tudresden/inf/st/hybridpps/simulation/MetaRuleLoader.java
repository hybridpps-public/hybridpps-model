package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.simulation.priority.input.RuleSet;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.MetaRuleScheduling;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.SchedulingStrategy;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public interface MetaRuleLoader {

    default RuleSet load(String path) {
       Path metaRuleDefinitionURI = Paths.get("src", "test", "resources", path);
        RuleSet ruleSet = null;
        try{
            ruleSet = RuleSet.fromJson(metaRuleDefinitionURI);
        } catch (IOException e){
            e.printStackTrace();
        }
        assertNotNull(ruleSet);
        return ruleSet;
    }

}
