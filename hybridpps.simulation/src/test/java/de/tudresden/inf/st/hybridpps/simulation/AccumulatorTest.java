package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global.acc.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This Testsuite contains unit-tests for all the different accumulator functions.
 * Reference values are calculated manually.
 * Error-cases are tested as well.
 */
public class AccumulatorTest {

    final double eps = 0.00001;
    double[] empty;
    double[] one;
    double[] values;

    @BeforeEach
    public void setup(){
        empty = new double[0];
        one = new double[]{5.12};
        values = new double[]{-1.5, 1, 2.9, 3, 4, 5, -7};
    }

    @Test
    public void sumTest(){
        Sum sum = new Sum();
        assertEquals("sum", sum.getName());
        assertEquals(7.4, sum.calculate(values), eps);
        assertEquals(0, sum.calculate(empty), eps);
    }

    @Test
    public void spanLengthTest(){
        SpanLength spanLength = new SpanLength();
        assertEquals("span_length", spanLength.getName());
        assertEquals(0, spanLength.calculate(one), eps);
        assertEquals(12, spanLength.calculate(values), eps);
        try{
            spanLength.calculate(empty);
            fail();
        }catch (IllegalArgumentException e){
            assertNotNull(e);
        }
    }

    @Test
    public void medianTest(){
        Median median = new Median();
        assertEquals("median", median.getName());
        assertEquals(5.12, median.calculate(one), eps);
        assertEquals(2.9, median.calculate(values), eps);
        try{
            median.calculate(empty);
            fail();
        }catch (IllegalArgumentException e){
            assertNotNull(e);
        }
    }

    @Test
    public void meanTest(){
        Mean mean = new Mean();
        assertEquals("mean", mean.getName());
        assertEquals(5.12, mean.calculate(one), eps);
        assertEquals(1.0571428, mean.calculate(values), eps);
        try{
            mean.calculate(empty);
            fail();
        }catch (IllegalArgumentException e){
            assertNotNull(e);
        }
    }

    @Test
    public void standardDeviationTest(){
        StandardDeviation standardDeviation = new StandardDeviation();
        assertEquals("standard_deviation", standardDeviation.getName());
        assertEquals(0, standardDeviation.calculate(one), eps);

        //compared with result from ms excel function STABW.N()
        assertEquals(3.83288669, standardDeviation.calculate(values), eps);

        try{
            standardDeviation.calculate(empty);
            fail();
        }catch (IllegalArgumentException e){
            assertNotNull(e);
        }
    }

}
