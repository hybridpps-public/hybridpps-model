package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.jastadd.model.DeterministicSolution;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.FIFO;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.MetaRuleScheduling;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.SchedulingStrategy;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MetaRuleSchedulingTest extends AbstractSimulationTest {

    /**
     * This tests if the basic MetaRule environment works, which means that:
     * - a rule definitions is loaded from a JSON file
     * - the parsed ruleset creates a representing evrete rule-engine model
     * - the evrete engine is loaded with the rule representation
     * - QueryableGlobalAttributes work and can be processed from the current simulation state
     * - the whole integration of MetaRuleScheduling as an independent SchedulingStrategy works
     * - the rule engine is able to decide on the next concrete SchedulingStrategy and sets it dynamically
     */
    @Test
    public void setupTest() {

        Path metaRuleDefinitionURI = Paths.get("src", "test", "resources", "metaRulesets/alwaysFIFO.json");
        SchedulingStrategy strategy = null;
        try{
            strategy = new MetaRuleScheduling(metaRuleDefinitionURI);
        } catch (IOException e){
            e.printStackTrace();
        }
        assertNotNull(strategy);

        DeterministicSolution solution = runDeterministicTest("schedulingStrategiesTests/fifoTest.json", strategy);

        assertThatSolution(solution).containsExactly(
                tuple("0", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("0", "3", "1", 3),
                tuple("0", "4", "1", 6)
        );
    }

    @Test
    public void simpleStrategyChange0Test() {

        Path metaRuleDefinitionURI = Paths.get("src", "test", "resources", "metaRulesets/simpleStrategyChange0.json");
        SchedulingStrategy strategy = null;
        try{
            strategy = new MetaRuleScheduling(metaRuleDefinitionURI);
        } catch (IOException e){
            e.printStackTrace();
        }
        assertNotNull(strategy);

        DeterministicSolution solution = runDeterministicTest("schedulingStrategiesTests/metaRuleTests/simpleChange1.json", strategy);

        assertThatSolution(solution).containsExactly(
                tuple("0", "1", "1", 0),
                tuple("0", "2", "1", 0),
                tuple("0", "3", "1", 3),
                tuple("0", "4", "1", 6),
                tuple("0", "5", "1", 9)
        );
    }

    @Test
    public void simpleStrategyChange1Test() {

        Path metaRuleDefinitionURI = Paths.get("src", "test", "resources", "metaRulesets/simpleStrategyChange1.json");
        SchedulingStrategy strategy = null;
        try{
            strategy = new MetaRuleScheduling(metaRuleDefinitionURI);
        } catch (IOException e){
            e.printStackTrace();
        }
        assertNotNull(strategy);

        DeterministicSolution solution = runDeterministicTest("schedulingStrategiesTests/metaRuleTests/simpleChange1.json", strategy);

        assertThatSolution(solution).containsExactly(
                tuple("0", "1", "1", 0),
                tuple("0", "4", "1", 0),
                tuple("0", "2", "1", 3),
                tuple("0", "3", "1", 6),
                tuple("0", "5", "1", 9)
        );
    }

    @Test
    public void simpleStrategyChange2Test() {

        Path metaRuleDefinitionURI = Paths.get("src", "test", "resources", "metaRulesets/simpleStrategyChange1.json");
        SchedulingStrategy strategy = null;
        try{
            strategy = new MetaRuleScheduling(metaRuleDefinitionURI);
        } catch (IOException e){
            e.printStackTrace();
        }
        assertNotNull(strategy);

        DeterministicSolution solution = runDeterministicTest("schedulingStrategiesTests/metaRuleTests/simpleChange2.json", strategy);

        assertThatSolution(solution).containsExactly(
                tuple("0", "1", "1", 0),
                tuple("0", "4", "1", 0),
                tuple("0", "2", "1", 3),
                tuple("0", "3", "1", 6),

                tuple("0", "5", "1", 9),

                tuple("0", "8", "1", 9),
                tuple("0", "6", "1", 12),
                tuple("0", "7", "1", 15),
                tuple("0", "9", "1", 18)
        );
    }

    @Test
    public void cprRuleTest(){

        Path metaRuleDefinitionURI = Paths.get("src", "test", "resources", "metaRulesets/cprRuleset.json");
        SchedulingStrategy strategy = null;
        try{
            strategy = new MetaRuleScheduling(metaRuleDefinitionURI);
        } catch (IOException e){
            e.printStackTrace();
        }
        assertNotNull(strategy);

        DeterministicSolution solution = runDeterministicTest("schedulingStrategiesTests/cprTest.json", strategy);

        assertThatSolution(solution).containsExactly(
                tuple("0", "1", "1", 0),
                tuple("0", "6", "1", 0),
                tuple("0", "4", "1", 0),
                tuple("0", "5", "1", 3),
                tuple("0", "2", "1", 5),
                tuple("0", "3", "1", 11),
                tuple("0", "7", "1", 21),
                tuple("0", "8", "1", 22)
        );
    }

}
