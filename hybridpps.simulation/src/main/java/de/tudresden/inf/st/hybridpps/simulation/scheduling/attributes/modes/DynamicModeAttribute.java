package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * This class models a dynamic attribute whose values can change even while an event is processed.
 *
 * @author Erik Schönherr
 */
public abstract class DynamicModeAttribute implements QueryableModeAttribute {

    @Override
    public double getValue(SimulationJobMode mode, SimulationProblem problem) {
        return getAttributeValue(mode, problem);
    }

    /**
     * Compute and return the value of the attribute for the given mode.
     *
     * @param mode the {@link SimulationJobMode} to compute the value for
     * @param problem simulation model
     * @return value of the attribute
     */
    abstract double getAttributeValue(SimulationJobMode mode, SimulationProblem problem);
}
