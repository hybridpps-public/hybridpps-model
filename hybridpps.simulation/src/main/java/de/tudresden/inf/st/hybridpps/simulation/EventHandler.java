package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.jastadd.model.Mapping;
import de.tudresden.inf.st.hybridpps.jastadd.model.Resource;
import de.tudresden.inf.st.hybridpps.jastadd.model.ResourceUsage;
import de.tudresden.inf.st.hybridpps.simulation.model.*;
import desmoj.core.simulator.TimeSpan;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class used to handle the events throughout the simulation
 *
 * @author Erik Schönherr
 */
public class EventHandler {

    private final static Logger logger = LogManager.getLogger(EventHandler.class);

    private SimulationProblem model;

    public EventHandler(SimulationProblem model) {
        this.model = model;
    }

    /**
     * Called after a {@link StartNextJobEvent} is triggered. First the queue of all executable jobs is updated, then
     * the routine for scheduling new jobs is called.
     */
    public void handleJobEvent() {
        // outside of routine as it only needs to be run one time per event
        updateExecutableJobs();
        scheduleJobRoutine();
    }

    /**
     * This method firstly computes all schedulable modes, then chooses one of them and schedules it.
     */
    public void scheduleJobRoutine() {
        List<SimulationJobMode> schedulableModes = findSchedulableModes();

        if (schedulableModes.isEmpty()) {
            // currently no job can be scheduled
            return;
        }

        SimulationJobMode nextMode = model.getSchedulingStrategy().getNextMode(schedulableModes, model);

        scheduleSelectedMode(nextMode);
        model.getExecutableJobs().remove(nextMode.getJob());

        // try to schedule jobs until no more jobs can be scheduled at this moment
        scheduleJobRoutine();
    }

    /**
     * Calculates which modes are schedulable at the current simulation time by first finding all jobs whose
     * predecessors are finished. Then filters out the modes of the jobs for which the needed resources aren't available.
     *
     * @return List of all schedulable {@link SimulationJobMode}
     */
    public List<SimulationJobMode> findSchedulableModes() {
        List<SimulationJobMode> schedulableModes = new ArrayList<>();

        // for all the job modes who's predecessors are finished, check for resource constraints
        for (SimulationJob job : model.getExecutableJobs()) {
            for (SimulationJobMode mode : job.getModes()){
                boolean resourceCondition = true;
                Map<SimulationResourceGroup, Integer> requiredResources = mode.getRequiredResources();
                for (Map.Entry<SimulationResourceGroup, Integer> entry : requiredResources.entrySet()) {

                    SimulationResourceGroup resourceGroup = entry.getKey();

                    if (getCapacity(resourceGroup) < entry.getValue()) {
                        resourceCondition = false;
                    }
                }
                if (resourceCondition) {
                    schedulableModes.add(mode);
                }
            }
        }
        return schedulableModes;
    }

    /**
     * Adds all newly executable jobs to the executableJobs queue of the simulation problem. Has to be called ones for
     * each event.
     */
    public void updateExecutableJobs() {
        // iterate through job queue
        // if job can be executed (precedence constraints), put it in executable jobs queue
        for (SimulationJob j : model.getJobs()) {
            if (j.getStatus() != SimulationJob.Status.NOT_STARTED) {
                continue;
            }

            // check precedence constraint
            boolean precedenceCondition = true;
            for (SimulationJob predecessor : j.getPredecessors()) {
                if (predecessor.getStatus() != SimulationJob.Status.FINISHED) {
                    precedenceCondition = false;
                    break;
                }
            }

            if (precedenceCondition) {
                // job only gets inserted if not already in queue
                model.getExecutableJobs().insert(j);
            }
        }
    }

    /**
     * Schedules the given mode. Firstly calculates on which resources the mode is supposed to be run and if setup is
     * needed, then schedules a {@link StartNextJobEvent} at the end of the job, which is:
     *      [current time] + [mode duration] + [setup time]
     *
     * @param nextMode the {@link SimulationJobMode} that needs to be scheduled
     */
    protected void scheduleSelectedMode(SimulationJobMode nextMode) {
        // block resource capacity
        Map<SimulationResourceGroup, Integer> requiredResources = nextMode.getRequiredResources();

        SimulationTypeRepresentative nextTR = nextMode.getJob().getTypeRepresentative();
        int setupTime = 0;

        for (Map.Entry<SimulationResourceGroup, Integer> entry : requiredResources.entrySet()) {
            SimulationResourceGroup group = entry.getKey();

            boolean setupNeeded = ResourceUtils.blockCapacity(group, entry.getValue(), nextMode, model);

            if (setupNeeded && nextTR.getSetupTime() > setupTime) {
                setupTime = nextTR.getSetupTime();
            }
        }
        // set job status to "started"
        nextMode.getJob().setStatus(SimulationJob.Status.STARTED);

        // console output
        String jobName = nextMode.getJob().getName();
        String modeName = nextMode.getName();
        logger.info(
                "Start Job " + jobName.replace(jobName.substring(jobName.indexOf("#")), "") +
                        " with Mode " + modeName.replace(modeName.substring(modeName.indexOf("#")), "") +
                        " at Time " + model.getExperiment().getSimClock().getTime() +
                        " and a setup time of " + setupTime
        );

        Mapping mapping = new Mapping();
        mapping.setMode(nextMode.getJastAddMode());
        mapping.setStartTime(Math.toIntExact(model.getExperiment().getSimClock().getTime().getTimeRounded()));
        // collect resource usages by the next mode
        for (SimulationResourceGroup group : nextMode.getRequiredResources().keySet()) {
            ArrayList<Resource> resources = (ArrayList<Resource>) nextMode
                    .getActiveResourcesForGroup(group)
                    .stream()
                    .map(SimulationResource::getJastAddResource)
                    .collect(Collectors.toList());
            if (resources.size() == 0) {
                // skip resource groups that aren't actually used
                continue;
            }
            ResourceUsage resourceUsage = new ResourceUsage();
            resourceUsage.setResourceGroup(group.getJastAddResourceGroup());
            resources.forEach(resourceUsage::addResource);
            mapping.addResourceUsage(resourceUsage);
        }

        model.getSolution().addMapping(mapping);

        // create a new StartNextJobEvent
        StartNextJobEvent startNextJob = new StartNextJobEvent(model, "StartNextJobEvent", true);
        // and schedule it for the end of the current job
        startNextJob.schedule(nextMode, new TimeSpan(nextMode.getDuration() + setupTime));
    }

    private int getCapacity(SimulationResourceGroup group) {
        return model.getResourceManager().getFreeResourcesForGroup(group).size();
    }
}
