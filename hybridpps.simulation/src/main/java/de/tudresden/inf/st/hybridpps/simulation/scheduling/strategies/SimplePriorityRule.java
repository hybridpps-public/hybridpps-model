package de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies;

public abstract class SimplePriorityRule implements SchedulingStrategy {

    /**
     * Get the name of the priority rule.
     *
     * @return the name of the rule
     */
    public abstract String getName();
}
