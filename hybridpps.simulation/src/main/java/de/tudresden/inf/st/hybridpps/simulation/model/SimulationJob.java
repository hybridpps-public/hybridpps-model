package de.tudresden.inf.st.hybridpps.simulation.model;

import de.tudresden.inf.st.hybridpps.jastadd.model.Job;
import desmoj.core.simulator.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class representing a single job in the simulation model.
 *
 * @author Sophie Ziemann
 */
public class SimulationJob extends Entity {

  private SimulationTypeRepresentative typeRepresentative;

  private ArrayList<SimulationJobMode> modes = new ArrayList<>();

  private List<SimulationJob> predecessors = new ArrayList<>();

  private List<SimulationJob> successors = new ArrayList<>();

  private Set<SimulationJob> totalSuccessors;

  private Status status = Status.NOT_STARTED;

  private Job jastAddJob;

  /**
   * Constructor of the job entity.
   *
   * @param owner the model this entity belongs to
   * @param name this job's name
   * @param showInTrace flag to indicate if this entity shall produce output
   *                    for the trace
   */
  public SimulationJob(Model owner, String name, boolean showInTrace) {
    super(owner, name, showInTrace);
  }

  /**
   * Returns a set of all successors of the job, meaning the direct successors, their successors and so on.
   *
   * @return Set containing all successors
   */
  public Set<SimulationJob> getAllSuccessors() {
    if (totalSuccessors != null) {
      return totalSuccessors;
    }

    Set<SimulationJob> successors = new HashSet<>(getSuccessors());
    for (SimulationJob job : getSuccessors()) {
      successors.addAll(job.getAllSuccessors());
    }
    totalSuccessors = successors;
    return successors;
  }

  /**
   * Represents all possible states a job can be in.
   */
  public enum Status {
    NOT_STARTED, // job isn't scheduled yet
    STARTED,     // job is scheduled but not finished yet
    FINISHED     // job is finished
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status newStatus) {
    status = newStatus;
  }

  public SimulationTypeRepresentative getTypeRepresentative() {
    return typeRepresentative;
  }

  public void setTypeRepresentative(SimulationTypeRepresentative typeRepresentative) {
    this.typeRepresentative = typeRepresentative;
  }

  public ArrayList<SimulationJobMode> getModes(){
    return modes;
  }

  public void addMode(SimulationJobMode newMode){
    modes.add(newMode);
  }

  public List<SimulationJob> getPredecessors() {
    return predecessors;
  }

  public void addPredecessor(SimulationJob job) {
    predecessors.add(job);
  }

  public List<SimulationJob> getSuccessors() {
    return successors;
  }

  public void addSuccessor(SimulationJob successor) {
    successors.add(successor);
  }

  public Job getJastAddJob(){
    return jastAddJob;
  }

  public void setJastAddJob(Job job){
    jastAddJob = job;
  }
}

