package de.tudresden.inf.st.hybridpps.simulation.priority.input;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

/**
 * Root class representing the meta-rule JSON DSL.
 * The RuleSet contains a list of Statements.
 * <p>
 * The RuleSet can be serialized and deserialized directly by passing a file Path to read from or write to.
 * Note that the RuleSet class and its members do only check for structural correctness and do not validate the
 * rule logic which is expected to be provided well-defined and without cycles!
 * <p>
 * The RuleSet itself does not implement the RuleCreator interface because each member Statement is evaluated separately.
 * While Statements are dependent on another, they are evaluated lazy. That means a Statement is only checked if it needs to be.
 * <strong>The order of Statements has no impact on the execution order. There must always be exactly one Statement
 * with the index "root" which is evaluated first. All other evaluations are depending on the root Statement.</strong>
 * <p>
 * example:
 *
 * <pre>{@code
 *
 * {
 *   "statements": []
 * }
 *
 * }</pre>
 */
public class RuleSet {

    private List<Statement> statements;

    @JsonCreator
    public RuleSet(@JsonProperty("statements") List<Statement> statements) {
        this.statements = statements;
    }

    public List<Statement> getStatements() {
        return statements;
    }

    public void setStatements(List<Statement> statements) {
        this.statements = statements;
    }

    /**
     * Reads a RuleSet from a given file represented by a Path.
     * Note that parsed Rulesets are not verified apart from their syntax!
     *
     * @param path of the meta-ruleset file
     * @return CreatedRuleset.
     * @throws IOException if Path does not provide a file or the file is inaccessible
     */
    public static RuleSet fromJson(Path path) throws IOException {
        return new ObjectMapper().readValue(path.toFile(), RuleSet.class);
    }

    public static void toJson(RuleSet ruleSet, String uri) throws IOException {
        new ObjectMapper().writeValue(new File(uri), ruleSet);
    }

    /**
     * Provides a Statement by its index. By specification, there is always one Statement with the index "root" to
     * begin with.
     *
     * @param index of the Statement to fetch
     * @return Optional Statement if present, empty if not present
     */
    public Optional<Statement> getStatement(String index) {
        return statements.stream().filter(s -> s.getIndex().equals(index)).findFirst();
    }
}
