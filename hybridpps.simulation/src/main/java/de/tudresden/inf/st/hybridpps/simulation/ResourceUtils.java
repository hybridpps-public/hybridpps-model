package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationResource;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationResourceGroup;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationTypeRepresentative;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class offering utility methods to block and free resources when jobs are scheduled or finish.
 *
 * @author Erik Schönherr
 */
public class ResourceUtils {

    /**
     * Blocks resources required by a mode that will get scheduled. First the method computes which resources will be
     * blocked. Preferred are resources with the last type representative equal to the tr of the next mode. Second are
     * resources without a type representative. At the end the chosen resources are removed from their queue and added
     * to the mode. Also computes whether or not setup time is needed.
     *
     * @param resourceGroup {@link SimulationResourceGroup} for which the resources are required
     * @param requiredCapacity amount of resources required
     * @param nextMode next mode that will be scheduled
     * @param problem the simulation model
     * @return true if setup time is needed
     */
    public static boolean blockCapacity(SimulationResourceGroup resourceGroup, Integer requiredCapacity, SimulationJobMode nextMode, SimulationProblem problem) {
        ResourceManager manager = problem.getResourceManager();
        List<SimulationResource> chosenResources;
        boolean needSetup;

        SimulationTypeRepresentative nextTR = nextMode.getJob().getTypeRepresentative();

        List<SimulationResource> resourcesWithoutSetup = new ArrayList<>(
                manager.getFreeResourcesWithLastTR(resourceGroup, nextTR)
        );

        if (resourcesWithoutSetup.size() >= requiredCapacity) {
            // there are enough resources with the required type representative -> not setup time needed
            chosenResources = resourcesWithoutSetup.subList(0, requiredCapacity);
            needSetup = false;
        } else {
            // use all resources without tr first, then the ones with the correct tr, then fill up with other resources
            List<SimulationResource> orderedFreeResources = new ArrayList<>();

            orderedFreeResources.addAll(manager.getFreeResourcesWithLastTR(resourceGroup, null));
            orderedFreeResources.addAll(resourcesWithoutSetup);

            for (SimulationResource res : manager.getFreeResourcesForGroup(resourceGroup)) {
                if (!orderedFreeResources.contains(res)) orderedFreeResources.add(res);
            }
            chosenResources = orderedFreeResources.subList(0, requiredCapacity);

            needSetup = true;
        }

        chosenResources.forEach(r -> {
            manager.removeFreeResource(r);
            nextMode.addActiveResource(r);
            r.setLastTypeRepresentative(nextTR);
        });

        return needSetup;
    }

    /**
     * Frees all resources used by a finished mode by adding them back to their respective queues.
     *
     * @param completedMode finished {@link SimulationJobMode}
     * @param problem the simulation model
     */
    public static void freeResources(SimulationJobMode completedMode, SimulationProblem problem){
        List<SimulationResource> resources = completedMode.getActiveResources();
        resources.forEach(r -> problem.getResourceManager().insertFreeResource(r));
    }

    /**
     * Checks for a given mode if it would need setup to be scheduled.
     *
     * @param mode the {@link SimulationResourceGroup} to check for
     * @param model the simulation model
     * @return true if the mode needs setup
     */
    public static boolean needsSetup(SimulationJobMode mode, SimulationProblem model) {
        for (Map.Entry<SimulationResourceGroup, Integer> requiredResource : mode.getRequiredResources().entrySet()) {
            SimulationResourceGroup group = requiredResource.getKey();
            int freeResources = model.getResourceManager().getFreeResourcesWithLastTR(group, mode.getJob().getTypeRepresentative()).size();

            if (freeResources < requiredResource.getValue()) {
                return true;
            }
        }
        return false;
    }
}
