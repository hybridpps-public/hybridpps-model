package de.tudresden.inf.st.hybridpps.simulation.scheduling;

import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global.QueryableGlobalAttribute;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes.QueryableModeAttribute;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

/**
 * This class holds references to all non-abstract classes implementing {@link QueryableModeAttribute} and {@link QueryableGlobalAttribute}. It can be used to
 * translate attribute names to actual attribute implementations.
 */
public class AttributePool {

    public static final Set<QueryableModeAttribute> modeAttributes = new HashSet<>();
    public static final Set<QueryableGlobalAttribute> globalAttributes = new HashSet<>();

    static {
        Reflections reflections = new Reflections("de.tudresden.inf.st.hybridpps");
        Set<Class<? extends QueryableModeAttribute>> attributeClasses = reflections.getSubTypesOf(QueryableModeAttribute.class);
        attributeClasses.forEach(c -> {
            try {
                if (Modifier.isAbstract(c.getModifiers())) {
                    return;
                }
                modeAttributes.add(c.getDeclaredConstructor().newInstance());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    static {
        Reflections reflections = new Reflections("de.tudresden.inf.st.hybridpps");
        Set<Class<? extends QueryableGlobalAttribute>> attributeClasses = reflections.getSubTypesOf(QueryableGlobalAttribute.class);
        attributeClasses.forEach(c -> {
            try {
                if (Modifier.isAbstract(c.getModifiers())) {
                    return;
                }
                globalAttributes.add(c.getDeclaredConstructor().newInstance());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Translates a given attribute name to an attribute object.
     *
     * @param name name of the attribute
     * @return found {@link QueryableModeAttribute}
     */
    public static QueryableModeAttribute getModeAttribute(String name) {
        for (QueryableModeAttribute attribute : modeAttributes) {
            if (attribute.getName().equals(name)) {
                return attribute;
            }
        }
        return null;
    }

    public static QueryableGlobalAttribute getGlobalAttribute(String name) {
        for (QueryableGlobalAttribute attribute : globalAttributes) {
            if (attribute.getName().equals(name)) {
                return attribute;
            }
        }
        return null;
    }
}
