package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * Expresses the start time of the project the mode belongs to. Will always be zero right now as the start time of the
 * project is always set to zero in the extension process.
 *
 * @author Erik Schönherr
 */
public class ProjectStartTime extends StaticModeAttribute {

    @Override
    public String getName() {
        return "project_start_time";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode) {
        return mode.getJob().getJastAddJob().containingProject().getEarliestStartTime();
    }
}
