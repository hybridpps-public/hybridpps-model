package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global.acc;

/**
 * Concrete Accumulator implementation to calculate the standard deviation of a double-valued array.
 */
public class StandardDeviation implements Accumulator {

    @Override
    public String getName() {
        return "standard_deviation";
    }

    /**
     * Calculate the standard deviation of the provided array.
     * <p>This method uses a common numeric implementation which is fast but may lead to growing errors with growing value-number.
     * See the concrete implementation for more information.</p>
     * <p>
     *     This algorithm follows the implementation from  https://www.geeksforgeeks.org/java-program-to-calculate-standard-deviation/
     * </p>
     * @param values array of values to calculate the standard deviation. Must at least contain a single element.
     * @return standard-deviation of values.
     */
    @Override
    public double calculate(double[] values) {
        if (values.length == 0)
            throw new IllegalArgumentException("Cannot calculate standard deviation of zero sized array");

        double sum = 0.0;
        double standardDeviation = 0.0;
        double mean = 0.0;
        double res = 0.0;
        double sq = 0.0;

        int n = values.length;

        for (double value : values) {
            sum = sum + value;
        }
        mean = sum / n;

        for (double value : values) {
            standardDeviation = standardDeviation + Math.pow((value - mean), 2);
        }
        sq = standardDeviation / n;
        res = Math.sqrt(sq);
        return res;
    }

}
