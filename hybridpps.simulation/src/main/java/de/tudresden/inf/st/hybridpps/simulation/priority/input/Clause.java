package de.tudresden.inf.st.hybridpps.simulation.priority.input;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Part of a Statement.
 * The Clause contains a logical operation with a left side attribute, a comparator and a right side value.
 * This class is provided directly by the json input and can create its own evrete-rule representation.
 * <p>
 * example:
 *
 * <pre>{@code
 * "clauses": [
 *         {
 *           "index": "c0",
 *           "left": "number_executable_modes",
 *           "clauseComparator": "GT",
 *           "right": 100
 *         },
 * ]
 * }</pre>
 * <p>
 * <strong>The "left" attribute can either be a "queryable_global_attribute" or a "accumulator:queryable_mode_attribute".</strong>
 * <p>
 * A clause must be defined by an unique String index, a left side GlobalAttribute or CombinedAttribute,
 * a Comparator (LE, GE, LT, GT, EQ, NE) and a right side double value to compare with.
 */
public class Clause implements RuleCreator, Expression {

    private String index;
    private String left;
    private String clauseComparator;
    private double right;

    /**
     * JSON constructor, always called by the json library.
     *
     * @param index            unique identifier of the Clause as an arbitrary String value
     * @param left             value to query from the simulation state in the form "queryable_global_attribute" or "accumulator:queryable_mode_attribute"
     * @param clauseComparator numeric comparator (LE, GE, LT, GT, EQ, NE). Note double equality issues.
     * @param right            double value to compare to. Note double equality issues.
     */
    @JsonCreator
    public Clause(
            @JsonProperty("index") String index,
            @JsonProperty("left") String left,
            @JsonProperty("clauseComparator") String clauseComparator,
            @JsonProperty("right") double right
    ) {
        this.index = index;
        this.left = left;
        this.clauseComparator = clauseComparator;
        this.right = right;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getClauseComparator() {
        return clauseComparator;
    }

    public void setClauseComparator(String clauseComparator) {
        this.clauseComparator = clauseComparator;
    }

    public double getRight() {
        return right;
    }

    public void setRight(double right) {
        this.right = right;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    /**
     * Transforms the comparator abbreviation of the rule DSL to the according java sign.
     * LT, LE, EQ, GE, GT
     *
     * @return java specific comparator sign of the current Clause object
     */
    private String comparatorToString() {
        switch (clauseComparator) {
            case "LE":
                return "<=";
            case "GE":
                return ">=";
            case "GT":
                return ">";
            case "LT":
                return "<";
            case "NE":
                return "!=";
            case "EQ":
                return "==";
            default:
                return null;
        }
    }

    /**
     * Translates the clause to an evrete/JAVA string representation which can be used to construct bigger rules.
     *
     * @param factDomain the variable (fact state) which contains the query method to get attribute values
     * @return evrete specific string representation of the clause
     */
    @Override
    public String asRule(String factDomain) {
        if (left == null || left.isBlank() || comparatorToString() == null) return null;
        return "(" + "$" + factDomain + ".query(\"" + left + "\") " + comparatorToString() + " " + right + ")";
    }

    @Override
    public String getId() {
        return index;
    }

    /**
     * Provides this instance as a ruleCreator which omits the need to cast if required.
     *
     * @return RuleCreator of the current Clause
     */
    @Override
    public RuleCreator ruleCreator() {
        return this;
    }
}
