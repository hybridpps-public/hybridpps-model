package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * Expresses the processing time of the job. Setup time is NOT taken into account.
 *
 * @author Erik Schönherr
 */
public class Duration extends StaticModeAttribute {

    @Override
    public String getName() {
        return "duration";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode) {
        return mode.getDuration();
    }
}
