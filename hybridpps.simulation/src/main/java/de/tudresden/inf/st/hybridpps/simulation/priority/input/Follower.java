package de.tudresden.inf.st.hybridpps.simulation.priority.input;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * The Follower is part of a Statement in the meta-rule definition.
 * By default, objects of this class are always provided by deserializing rule specifications directly.
 * <p>
 * A Follower holds information how the rule-engine must proceed depending on the outcome of the current Statement.
 * The "next" attribute must be a string in the format "[statement|rule]:value". The allowed statements are all statements
 * by their index defined in the ruleset. Allowed rules are all simple priority rules provided by the value of their
 * toString() method. In those cases, the params array remains empty but must be defined.
 * example:
 * <pre>{@code
 *  "onTrue": {
 *      "next": "rule:fifo",
 *      "params": []
 *  },
 *  "onFalse": {
 *      "next": "statement:s2",
 *      "params": []
 *  }
 * }</pre>
 * <p>
 * If a combined priority is used as a following rule, the params array contain the complete CPR model as one string with
 * escaped syntax.
 * example:
 * <pre>{@code
 *  "onTrue": {
 *      "next": "rule:cpr",
 *      "params": ["{\"duration"\: \"...\"}"]
 *  }
 * }</pre>
 */
public class Follower {

    private String next;
    private List<String> params;

    /**
     * JSON constructor, always called by the json library.
     * @param next String of the following operation in the format "[statement|rule]:value"
     * @param params Array of String params. Must be defined, can be empty.
     */
    @JsonCreator
    public Follower(
            @JsonProperty("next") String next,
            @JsonProperty("params") List<String> params
    ) {
        this.next = next;
        this.params = params;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public List<String> getParams() {
        return params;
    }

    public void setParams(List<String> params) {
        this.params = params;
    }

    /**
     * Check if the operations specified by this Follower is a statement.
     * If not a statement, the operation must be a scheduling rule.
     * @return Boolean - true if statement, false otherwise
     */
    @JsonIgnore
    public boolean isStatement(){
        return next.startsWith("statement");
    }

    /**
     * Get the following operation as String representation.
     * <p> If the Follower contains "rule:fifo" then the value "fifo" is returned.
     * @return String - the value of the following operation or strategy
     */
    @JsonIgnore
    public String getFollower(){
        return next.split(":")[1];
    }

}
