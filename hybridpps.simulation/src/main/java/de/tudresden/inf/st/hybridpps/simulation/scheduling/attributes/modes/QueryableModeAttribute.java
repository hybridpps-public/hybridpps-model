package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

public interface QueryableModeAttribute {

    /**
     * Gives the name of the attribute.
     *
     * @return name of the attribute
     */
    String getName();

    /**
     * Query the value of the attribute for the given mode.
     *
     * @param mode the {@link SimulationJobMode} to get the value from
     * @param problem the simulation model
     * @return value of the attribute
     */
    double getValue(SimulationJobMode mode, SimulationProblem problem);
}
