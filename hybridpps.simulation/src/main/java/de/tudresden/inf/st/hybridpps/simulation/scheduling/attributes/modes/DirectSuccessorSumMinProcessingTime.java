package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJob;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

import java.util.List;

/**
 * Expresses the sum of the processing times of direct successors. If a job has multiple modes with different processing
 * times, the minimum processing time is chosen.
 *
 * @author Erik Schönherr
 */
public class DirectSuccessorSumMinProcessingTime extends StaticModeAttribute {

    @Override
    public String getName() {
        return "direct_successor_sum_min_processing_time";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode) {
        List<SimulationJob> successors = mode.getJob().getSuccessors();
        double sum = 0;
        for (SimulationJob successor : successors) {
            double min = Integer.MAX_VALUE;
            for (SimulationJobMode sucMode : successor.getModes()) {
                min = Math.min(min, sucMode.getDuration());
            }
            sum += min;
        }
        return sum;
    }
}