package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJob;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

import java.util.Set;

/**
 * Expresses the sum of the processing times of all successors. If a job has multiple modes with different processing
 * times, the maximum processing time is chosen.
 *
 * @author Erik Schönherr
 */
public class TotalSuccessorSumMaxProcessingTime extends StaticModeAttribute {

    @Override
    public String getName() {
        return "total_successor_sum_max_processing_time";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode) {
        Set<SimulationJob> successors = mode.getJob().getAllSuccessors();
        double sum = 0;
        for (SimulationJob successor : successors) {
            double max = 0;
            for (SimulationJobMode sucMode : successor.getModes()) {
                max = Math.max(max, sucMode.getDuration());
            }
            sum += max;
        }
        return sum;
    }
}
