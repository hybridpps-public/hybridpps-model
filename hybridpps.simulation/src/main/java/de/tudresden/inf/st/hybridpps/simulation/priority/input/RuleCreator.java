package de.tudresden.inf.st.hybridpps.simulation.priority.input;

/**
 * Interface which all classes representing rules or part of rules/terms must implement.
 */
public interface RuleCreator {

    /**
     * Creates a string representation of the target class.
     * <strong>Note the documentation provided by the concrete implementation!</strong>
     *
     * @param factDomain placeholder for the object to query attributes from
     * @return String representation of the rule, partial rule or term.
     */
    String asRule(String factDomain);
}
