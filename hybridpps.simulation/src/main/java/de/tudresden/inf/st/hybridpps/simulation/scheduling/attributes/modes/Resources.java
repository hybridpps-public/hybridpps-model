package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * Expresses the number of resource units needed to execute the mode.
 *
 * @author Erik Schönherr
 */
public class Resources extends StaticModeAttribute {

    @Override
    public String getName() {
        return "resources";
    }

    public double getAttributeValue(SimulationJobMode mode) {
        return mode.getRequiredResources().values().stream().mapToInt(Integer::intValue).sum();
    }
}
