package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * Expresses the number of all successors, i.e. is the number of modes that have this mode as direct or indirect
 * predecessor.
 *
 * @author Erik Schönherr
 */
public class TotalSuccessorCount extends StaticModeAttribute {

    @Override
    public String getName() {
        return "total_successor_count";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode) {
        return mode.getJob().getAllSuccessors().size();
    }
}
