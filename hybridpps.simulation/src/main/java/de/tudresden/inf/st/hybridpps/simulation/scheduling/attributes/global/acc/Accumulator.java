package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global.acc;

public interface Accumulator {
    String getName();

    double calculate(double[] values);
}
