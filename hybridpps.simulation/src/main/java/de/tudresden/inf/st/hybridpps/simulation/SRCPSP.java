package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.jastadd.model.*;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.SchedulingStrategy;

import java.io.File;

/**
 * Implementation of the sRCPSP (stochastic Resource Constrained Project Scheduling Problem).
 *
 * @author Erik Schoenherr - Initial contribution
 */
public class SRCPSP extends SimulationProblem {

    private final int runNumber;

    public SRCPSP(ProductionSystem inputModel, SchedulingStrategy schedulingStrategy, File problemDirectory,
                  int runNumber) {
        super(inputModel, schedulingStrategy, problemDirectory, "SRCPSP Model");

        this.runNumber = runNumber;
    }

    @Override
    public String description() {
        return "This model describes a stochastic Resource-Constrained Project Scheduling Problem.";
    }

    /**
     * Unrolls a {@link Value} by extracting it's stochastic value for the considered run number.
     *
     * @param value the {@link Value} to unroll
     * @return the unrolled integer
     */
    @Override
    protected Integer getUnrolledValue(Value value) {
        return value.unrolledNumbers().getNumbers().get(runNumber).intValue();
    }

    /**
     * Sets the output directory to a new directory in the given base directory. The name depends on the run number.
     *
     * @param baseDirectory base output directory
     * @return true if the directory was successfully created
     */
    @Override
    boolean setOutputDirectory(File baseDirectory) {
        outputDirectory = baseDirectory.toPath().resolve("Run-" + (runNumber+1)).toFile();
        return outputDirectory.mkdir();
    }
}
