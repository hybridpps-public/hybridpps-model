package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.extend.ExtensionProcess;
import de.tudresden.inf.st.hybridpps.jastadd.model.DeterministicSolution;
import de.tudresden.inf.st.hybridpps.jastadd.model.ProductionSystem;
import de.tudresden.inf.st.hybridpps.jastadd.model.StochasticSolution;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.SchedulingStrategy;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.parameters.CPRLoader;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Generator for deterministic and stochastic solutions of a specified scheduling problem.
 *
 * @author Erik Schoenherr - Initial contribution
 */
public class SolutionGenerator {

    public static DeterministicSolution generateDeterministicSolution(Path modelPath, Path strategyPath) throws IOException {
        return generateDeterministicSolution(modelPath, CPRLoader.load(strategyPath));
    }

    public static DeterministicSolution generateDeterministicSolution(Path modelPath, SchedulingStrategy schedulingStrategy) {
        return generateDeterministicSolution(modelPath, schedulingStrategy, true, true);
    }

    /**
     * Generates a {@link DeterministicSolution} for a scheduling problem given by a path to a .json file. Uses the
     * provided scheduling strategy to decide which mode to schedule next.
     *
     * @param path {@link Path} to the .json file
     * @param schedulingStrategy concrete scheduling strategy to use
     * @param persist whether or not to persist the extended model
     * @return generated deterministic solution
     */
    public static DeterministicSolution generateDeterministicSolution(Path path, SchedulingStrategy schedulingStrategy, boolean persist, boolean report) {
        ExtensionProcess process = new ExtensionProcess();
        process.setPathToInputJson(path);
        process.setPersistModel(persist);
        ExtensionProcess.SolutionProcessor processor = new ExtensionProcess.SolutionProcessor("DESMOJ-Test") {
            @Override
            protected DeterministicSolution process(ProductionSystem productionSystem) {
                return SimulationProblem.startRCPSP(productionSystem, schedulingStrategy, report);
            }
        };
        process.addSolutionProcessor(processor);
        process.run();
        return (DeterministicSolution) processor.getResult();
    }

    public static StochasticSolution generateStochasticSolution(Path modelPath, Path strategyPath) throws IOException {
        return generateStochasticSolution(modelPath, CPRLoader.load(strategyPath));
    }

    public static StochasticSolution generateStochasticSolution(Path modelPath, SchedulingStrategy schedulingStrategy) {
        return generateStochasticSolution(modelPath, schedulingStrategy, true, true);
    }

    /**
     * Generates a {@link StochasticSolution} for a scheduling problem given by a path to a .json file. Uses the
     * provided scheduling strategy to decide which mode to schedule next.
     *
     * @param modelPath {@link Path} to the .json file
     * @param schedulingStrategy concrete scheduling strategy to use
     * @param persist whether or not to persist the extended model
     * @return generated stochastic solution
     */
    public static StochasticSolution generateStochasticSolution(Path modelPath, SchedulingStrategy schedulingStrategy, boolean persist, boolean report) {
        ExtensionProcess process = new ExtensionProcess();
        process.setPathToInputJson(modelPath);
        process.setPersistModel(persist);
        ExtensionProcess.SolutionProcessor processor = new ExtensionProcess.SolutionProcessor("DESMOJ-Test") {
            @Override
            protected StochasticSolution process(ProductionSystem productionSystem) {
                return SimulationProblem.startSRCPSP(productionSystem, schedulingStrategy, report);
            }
        };
        process.addSolutionProcessor(processor);
        process.run();
        return (StochasticSolution) processor.getResult();
    }
}
