package de.tudresden.inf.st.hybridpps.simulation.priority.input;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedList;
import java.util.List;

/**
 * Part of a priority-rule specification. By default, a Statement is deserialized from a rule-specification an does not
 * have to be created manually.
 * <p>
 * A Statement represents a list of Clauses, a list of Conditions which defines logical terms to be evaluated.
 * Evaluating a Statement starts by fetching the gate attribute. If the gate is a Clause, it is checked if the simple
 * expression of the Clause is true. If the gate is a Condition then it constructs the more complex logical expression
 * recursively which es evaluated. The index of a Statement must be unique.
 * <p>
 * The Statement must always define the following actions onTrue and onFalse which are executed depending on the result
 * of evaluating the gate.
 * <p>
 * example:
 *
 * <pre>{@code
 * {
 *       "index": "root",
 *       "clauses": [
 *         {
 *           "index": "c0",
 *           "left": "number_executable_modes",
 *           "clauseComparator": "GT",
 *           "right": 100
 *         },
 *         {
 *           "index": "c1",
 *           "left": "number_executable_modes",
 *           "clauseComparator": "LT",
 *           "right": 200
 *         }
 *       ],
 *       "conditions": [
 *         {
 *           "index": "a",
 *           "operator": "AND",
 *           "operands": ["c0", "c1"],
 *           "yields": true
 *         }
 *       ],
 *       "gate": "a",
 *       "onTrue": {
 *         "next": "rule:fifo",
 *         "params": []
 *       },
 *       "onFalse": {
 *         "next": "statement:s1",
 *         "params": []
 *       }
 *     },
 * }</pre>
 */
public class Statement implements RuleCreator {

    private List<Clause> clauses;
    private List<Condition> conditions;

    private String index;
    private String gate;
    private Follower onTrue;
    private Follower onFalse;

    /**
     * JSON constructor, always called by the json library.
     *
     * @param index      unique identifier of the Statement as an arbitrary String value
     * @param clauses    Array of Clauses. Must have a minimum length of one.
     * @param conditions Array of Conditions. Must be defined, can be empty.
     * @param gate       index of condition or clause which is evaluated to decide the next Follower
     * @param onTrue     Follower specification if gate is true
     * @param onFalse    Follower specification if gate is false
     */
    @JsonCreator
    public Statement(
            @JsonProperty("index") String index,
            @JsonProperty("clauses") List<Clause> clauses,
            @JsonProperty("conditions") List<Condition> conditions,
            @JsonProperty("gate") String gate,
            @JsonProperty("onTrue") Follower onTrue,
            @JsonProperty("onFalse") Follower onFalse
    ) {
        if (clauses.size() < 1) throw new IllegalArgumentException("At least one clause is required");
        this.index = index;
        this.clauses = clauses;
        this.conditions = conditions;
        this.gate = gate;
        this.onTrue = onTrue;
        this.onFalse = onFalse;
    }

    /**
     * Creates a string representation of the combined conditions (Clauses) the rule contains.
     * The clauses are chained by logical operations so that always a single term results.
     *
     * @param factDomain the variable (fact state) which contains the query method to get attribute values
     * @return evrete-rule representation of all combined clauses
     */
    @Override
    public String asRule(String factDomain) {
        if (gate == null || gate.isBlank() || clauses.isEmpty()) return null;

        List<Expression> expressions = new LinkedList<Expression>(clauses);
        expressions.addAll(conditions);
        conditions.forEach(condition -> condition.wire(expressions));

        var clauseOption = clauses.stream().filter(c -> c.getId().equals(gate)).findFirst();
        if (clauseOption.isPresent()) {
            Clause gate = clauseOption.get();
            String rule = gate.asRule(factDomain);
            System.out.println("generated rule: " + rule);
            return rule;
        }

        var conditionOption = conditions.stream().filter(c -> c.getId().equals(gate)).findFirst();
        if (conditionOption.isPresent()) {
            Condition gate = conditionOption.get();
            String rule = gate.asRule(factDomain);
            System.out.println("generated rule: " + rule);
            return rule;
        }

        return null;
    }

    public List<Clause> getClauses() {
        return clauses;
    }

    public void setClauses(List<Clause> clauses) {
        this.clauses = clauses;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public String getGate() {
        return gate;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public Follower getOnTrue() {
        return onTrue;
    }

    public void setOnTrue(Follower onTrue) {
        this.onTrue = onTrue;
    }

    public Follower getOnFalse() {
        return onFalse;
    }

    public void setOnFalse(Follower onFalse) {
        this.onFalse = onFalse;
    }
}
