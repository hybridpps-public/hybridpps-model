package de.tudresden.inf.st.hybridpps.simulation.scheduling.parameters;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.*;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes.QueryableModeAttribute;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.CombinedPriorityRule;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.WeightedAttributes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

/**
 * This class is used to load combined priority rules (CPRs) from a .json file.
 *
 * @author Erik Schönherr
 * @author Karl Kegel
 */
public class CPRLoader {

    /**
     * Loads a JSON-file containing a list of attribute names and weights, creates a {@link WeightedAttributes} instance
     * from that and adds it to a {@link CombinedPriorityRule}.
     *
     * @param pathToInputJson path to the .json file containing the information about weights and attributes
     * @return created instance of {@link CombinedPriorityRule}
     */
    public static CombinedPriorityRule load(Path pathToInputJson) throws IOException {
        return load(pathToInputJson.toFile());
    }

    public static CombinedPriorityRule load(String inputJson) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        DataSchedulingParameter[] dsp = mapper.readValue(inputJson, DataSchedulingParameter[].class);
        return build(dsp);
    }

    public static CombinedPriorityRule load(File inputJson) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        DataSchedulingParameter[] dsp = mapper.readValue(inputJson, DataSchedulingParameter[].class);
        return build(dsp);
    }

    private static CombinedPriorityRule build(DataSchedulingParameter[] dsp){
        // parameters only needed for combined priority rule scheduling strategy
        WeightedAttributes weights = new WeightedAttributes();
        for (DataSchedulingParameter parameter : dsp) {
            QueryableModeAttribute attribute = AttributePool.getModeAttribute(parameter.name);
            Objects.requireNonNull(attribute, "Couldn't load attribute with name " + parameter.name);
            weights.put(attribute, parameter.value);
        }
        return new CombinedPriorityRule(weights);
    }
}
