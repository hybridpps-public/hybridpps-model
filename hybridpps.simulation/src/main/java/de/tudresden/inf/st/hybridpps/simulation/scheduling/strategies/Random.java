package de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

import java.util.List;

/**
 * Implementation of random scheduling.
 *
 * @author Erik Schoenherr
 */
public class Random extends SimplePriorityRule {

    @Override
    public String getName() {
        return "random";
    }

    /**
     * Chooses a random executable job mode.
     *
     * @param executableModes list of executable job modes
     * @return chosen job mode
     */
    @Override
    public SimulationJobMode getNextMode(List<SimulationJobMode> executableModes, SimulationProblem problem) {
        java.util.Random rand = new java.util.Random();
        return executableModes.get(rand.nextInt(executableModes.size()));
    }
}
