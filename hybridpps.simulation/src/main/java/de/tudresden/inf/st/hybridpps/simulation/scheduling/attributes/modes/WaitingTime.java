package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * Expresses the time the job is already waiting in the queue. A job "starts waiting" when all it's predecessors are
 * finished.
 *
 * @author Erik Schönherr
 */
public class WaitingTime extends SemiDynamicModeAttribute {

    @Override
    public String getName() {
        return "waiting_time";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode, SimulationProblem problem) {
        return problem.getExecutableJobs().getCurrentWaitTime(mode.getJob()).getTimeAsDouble();
    }
}
