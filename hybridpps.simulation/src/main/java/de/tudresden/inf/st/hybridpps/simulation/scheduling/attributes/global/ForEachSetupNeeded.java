package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes.SetupNeeded;

import java.util.List;

/**
 * A simple QueryableGlobalAttribute to get the value if for each mode a setup is needed.
 * Returns 0 if there exist a mode without setup
 * Returns 1 if all modes require a setup
 */
public class ForEachSetupNeeded implements QueryableGlobalAttribute {

    @Override
    public String getName() {
        return "foreach_setup_needed";
    }

    @Override
    public double getValue(List<SimulationJobMode> executableModes, SimulationProblem problem) {
        if(executableModes.size() == 0) return 0;
        boolean setupNeeded = true;
        SetupNeeded attribute = new SetupNeeded();
        for(SimulationJobMode mode : executableModes){
            setupNeeded = setupNeeded && (attribute.getValue(mode, problem) > 0.5);
        }
        return setupNeeded ? 1 : 0;
    }
}
