package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

import java.util.List;

/**
 * General interface of an QueryableGlobalAttribute.
 * <p> An attribute following this definition uses the list of executable modes to produce a value.
 */
public interface QueryableGlobalAttribute {

    /**
     * Gives the name of the attribute.
     *
     * @return name of the attribute
     */
    String getName();

    /**
     * Query the value of the attribute for the given mode.
     *
     * @param problem the simulation model
     * @return value of the attribute
     */
    double getValue(List<SimulationJobMode> executableModes, SimulationProblem problem);
}
