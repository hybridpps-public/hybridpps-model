package de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;
import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;

import java.util.List;

/**
 * Implementation of LIFO (Last In First Out) scheduling.
 *
 * @author Erik Schoenherr
 */
public class LIFO extends SimplePriorityRule {

    @Override
    public String getName() {
        return "lifo";
    }

    /**
     * Chooses the last executable job mode.
     *
     * @param executableModes list of executable job modes
     * @return chosen job mode
     */
    @Override
    public SimulationJobMode getNextMode(List<SimulationJobMode> executableModes, SimulationProblem problem) {
        return executableModes.get(executableModes.size() - 1);
    }
}
