package de.tudresden.inf.st.hybridpps.simulation;

import desmoj.core.simulator.*;

/**
 * This class represents the start job event in the problem model.
 * It occurs at the start of the simulation.
 *
 * @author Sophie Ziemann
 */
public class StartFirstJobEvent extends ExternalEvent {

  /**
   * Constructs a new StartFirstJobEvent.
   *
   * @param owner the model this event belongs to
   * @param name this event's name
   * @param showInTrace flag to indicate if this event shall produce output
   *                    for the trace
   */
  public StartFirstJobEvent(SimulationProblem owner, String name, boolean showInTrace) {
    super(owner, name, showInTrace);
  }

  /**
   * This method gets called at the beginning of the simulation. It calls the model to schedule the fist job modes.
   */
  public void eventRoutine() {
    // get a reference to the model
    SimulationProblem model = (SimulationProblem) getModel();
    model.handleJobEvent();
  }
}
