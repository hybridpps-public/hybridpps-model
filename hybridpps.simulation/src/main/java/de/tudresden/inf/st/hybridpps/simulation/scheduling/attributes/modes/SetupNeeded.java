package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.ResourceUtils;
import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * Expresses whether or not setup is needed to schedule the mode at the current time. Equal to one if setup is needed,
 * otherwise zero.
 *
 * @author Erik Schönherr
 */
public class SetupNeeded extends DynamicModeAttribute {

    @Override
    public String getName() {
        return "setup_needed";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode, SimulationProblem problem) {
        return ResourceUtils.needsSetup(mode, problem) ? 1 : 0;
    }
}
