package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

import java.util.List;

/**
 * A simple QueryableGlobalAttribute to get the number of executable modes in the queue, i.e. the queue size.
 * Not that the same job can have multiple executable modes.
 */
public class NumberExecutableModes implements QueryableGlobalAttribute {

    @Override
    public String getName() {
        return "number_executable_modes";
    }

    @Override
    public double getValue(List<SimulationJobMode> executableModes, SimulationProblem problem) {
        return executableModes.size();
    }
}
