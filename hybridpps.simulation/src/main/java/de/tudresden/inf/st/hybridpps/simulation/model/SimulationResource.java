package de.tudresden.inf.st.hybridpps.simulation.model;

import de.tudresden.inf.st.hybridpps.jastadd.model.Resource;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;

/**
 * A single resource for the simulation.
 *
 * @author Erik Schönherr
 */
public class SimulationResource extends Entity {

    private SimulationResourceGroup resourceGroup;

    private SimulationTypeRepresentative lastTypeRepresentative;

    private Resource jastAddResource;

    /**
     * Constructor of the resource entity.
     */
    public SimulationResource(Model model, String name, boolean showInTrace) {
        super(model, name, showInTrace);
    }

    public SimulationResourceGroup getResourceGroup() {
        return resourceGroup;
    }

    public void setResourceGroup(SimulationResourceGroup resourceGroup) {
        this.resourceGroup = resourceGroup;
    }

    public SimulationTypeRepresentative getLastTypeRepresentative() {
        return lastTypeRepresentative;
    }

    public void setLastTypeRepresentative(SimulationTypeRepresentative lastTypeRepresentative) {
        this.lastTypeRepresentative = lastTypeRepresentative;
    }

    public Resource getJastAddResource(){
        return jastAddResource;
    }

    public void setJastAddResource(Resource resource){
        jastAddResource = resource;
    }
}
