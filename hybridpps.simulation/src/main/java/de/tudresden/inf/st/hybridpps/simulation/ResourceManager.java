package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.jastadd.model.Project;
import de.tudresden.inf.st.hybridpps.jastadd.model.Value;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationResource;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationResourceGroup;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationTypeRepresentative;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.Queue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class holding references to and managing all the different resource queues for the different resource groups.
 *
 * @author Erik Schönherr
 */
public class ResourceManager {

    private Map<SimulationResourceGroup, Queue<SimulationResource>> resourceQueues = new HashMap<>();

    /**
     * Query for all free resources for the given group.
     *
     * @param group the {@link SimulationResourceGroup} to find the free resources for
     * @return List of the free resources
     */
    public List<SimulationResource> getFreeResourcesForGroup(SimulationResourceGroup group) {

        List<SimulationResource> resources = new ArrayList<>();
        for (SimulationResource r : resourceQueues.get(group)) {
            resources.add(r);
        }
        return resources;
    }

    /**
     * Query for all free resources for the given group whose last type representative is equal to the given one.
     *
     * @param group the {@link SimulationResourceGroup} to find the free resources for
     * @param str the {@link SimulationTypeRepresentative} to find the free resources for
     * @return List of the free resources
     */
    public List<SimulationResource> getFreeResourcesWithLastTR(SimulationResourceGroup group, SimulationTypeRepresentative str) {
        List<SimulationResource> res = getFreeResourcesForGroup(group);

        return res.stream()
                .filter(r -> r.getLastTypeRepresentative() == str)
                .collect(Collectors.toList());
    }

    /**
     * Inserts a resource into the queue mapped to the resource group the resource belongs to.
     *
     * @param resource the {@link SimulationResource} to insert
     */
    public void insertFreeResource(SimulationResource resource) {
        resourceQueues.get(resource.getResourceGroup()).insert(resource);
    }

    /**
     * Removes a resource from the queue mapped to the resource group the resource belongs to.
     *
     * @param resource the {@link SimulationResource} to remove
     */
    public void removeFreeResource(SimulationResource resource) {
        resourceQueues.get(resource.getResourceGroup()).remove(resource);
    }

    /**
     * Initializes a queue for the given resource group.
     *
     * @param group the {@link SimulationResource} to create the queue for
     * @param model the owner of the queue
     */
    public void putQueue(SimulationResourceGroup group, Model model) {
        String queueName = "Free Resources for Group " + group.getJastAddResourceGroup().getName();
        if (group.isGlobal()) {
            queueName += " (global)";
        } else {
            // direct parent is JastAddList, parent of that is the project
            Project parentProject = (Project) group.getJastAddResourceGroup().getParent().getParent();
            queueName += " (local in P" + parentProject.getName() + ")";
        }
        Queue<SimulationResource> queue = new Queue<>(model, queueName, true, true);
        resourceQueues.put(group, queue);
    }
}
