package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJob;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A simple QueryableGlobalAttribute to get the number of distinct executable jobs in the queue, i.e. the queue size grouped by the job identifier.
 * Not that the same job can have multiple executable modes.
 */
public class NumberExecutableJobs implements QueryableGlobalAttribute {

    @Override
    public String getName() {
        return "number_executable_jobs";
    }

    @Override
    public double getValue(List<SimulationJobMode> executableModes, SimulationProblem problem) {
        Set<SimulationJob> jobs = new HashSet<SimulationJob>();
        executableModes.forEach(mode -> jobs.add(mode.getJob()));
        return jobs.size();
    }
}
