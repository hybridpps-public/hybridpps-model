package de.tudresden.inf.st.hybridpps.simulation.priority.input;

/**
 * The Expression interface defines classes which are identifiable by a unique id and can produce a RuleCreator which
 * can transform the implementing class into a String rule representation.
 */
public interface Expression {

    String getId();

    RuleCreator ruleCreator();

}
