package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * Expresses the supposed finish time of the project the mode belongs to.
 *
 * @author Erik Schönherr
 */
public class ProjectFinishTime extends StaticModeAttribute {

    @Override
    public String getName() {
        return "project_finish_time";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode) {
        return mode.getJob().getJastAddJob().containingProject().getLatestCompletionTime();
    }
}
