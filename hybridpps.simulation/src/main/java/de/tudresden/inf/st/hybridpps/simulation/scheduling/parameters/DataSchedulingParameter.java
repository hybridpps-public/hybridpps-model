package de.tudresden.inf.st.hybridpps.simulation.scheduling.parameters;

public class DataSchedulingParameter {
    public String name;
    public Double value = 0.0;
}
