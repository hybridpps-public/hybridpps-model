package de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;
import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;

import java.util.List;

/**
 * Implementation of FIFO (First In First Out) scheduling.
 *
 * @author Erik Schoenherr
 */
public class FIFO extends SimplePriorityRule {

    @Override
    public String getName() {
        return "fifo";
    }

    /**
     * Chooses the first executable job mode.
     *
     * @param executableModes list of executable job modes
     * @return chosen job mode
     */
    @Override
    public SimulationJobMode getNextMode(List<SimulationJobMode> executableModes, SimulationProblem problem) {
        return executableModes.get(0);
    }
}
