package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJob;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;
import desmoj.core.simulator.*;

/**
 * This class represents the StartNextJobEvent in the problem model.
 * It occurs when a job finished executing and a new job can be executed.
 *
 * @author Sophie Ziemann
 */
public class StartNextJobEvent extends Event<SimulationJobMode> {

  /**
   * A reference to the model this event is a part of.
   * Useful shortcut to access the model's static components
   */
  private final SimulationProblem model;

  /**
   * Constructor of the StartNextJobEvent.
   *
   * @param owner the model this event belongs to
   * @param name this event's name
   * @param showInTrace flag to indicate if this event shall produce output
   *                    for the trace
   */
  public StartNextJobEvent(SimulationProblem owner, String name, boolean showInTrace) {
    super(owner, name, showInTrace);
    // store a reference to the model this event is associated with
    model = owner;
  }

  /**
   * This method gets called when a job has finished. It sets the job status to finished, frees the used resources and
   * then calls the model to schedule new jobs.
   *
   * @param simulationJobMode the finished job mode
   */
  public void eventRoutine(SimulationJobMode simulationJobMode) {
    // start by setting job status to finished and freeing the resource
    simulationJobMode.getJob().setStatus(SimulationJob.Status.FINISHED);

    // free used resources
    ResourceUtils.freeResources(simulationJobMode, model);

    model.handleJobEvent();
  }
}