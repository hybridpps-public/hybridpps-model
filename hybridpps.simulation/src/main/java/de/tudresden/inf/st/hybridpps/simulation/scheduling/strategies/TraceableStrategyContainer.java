package de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

import java.util.LinkedList;
import java.util.List;

/**
 * <p> Decorator class to serve for debugging and testing of SchedulingStrategies.
 * <p> This class takes a SchedulingStrategy as an argument but implements the SchedulingStrategy interface itself.
 * Internally, all getNextMode calls are redirected to the provided SchedulingStrategy. Additionally this class takes
 * a copy of each iteration state of executable modes (the execution queue).
 * <p> After an experiment run, this class contains all snapshots of the queue throughout the simulation and the SimulationProblem itself.
 * In consequence, this class can be used if the behaviour of a SchedulingStrategy or an experiment run should be traced in code
 * and not only by logs.
 * <p> <strong>Apart from test-code and debugging, this class must not be used in production code because it may introduce a memory leak for
 * large problem definitions.</strong>
 */
public class TraceableStrategyContainer implements SchedulingStrategy {

    private final SchedulingStrategy schedulingStrategy;
    private final List<List<SimulationJobMode>> trace;

    private SimulationProblem simulationProblem;

    public TraceableStrategyContainer(SchedulingStrategy schedulingStrategy) {
        this.schedulingStrategy = schedulingStrategy;
        this.trace = new LinkedList<>();
    }

    @Override
    public SimulationJobMode getNextMode(List<SimulationJobMode> executableModes, SimulationProblem problem) {
        if (this.simulationProblem == null) {
            simulationProblem = problem;
        }
        pushTrace(executableModes);
        return schedulingStrategy.getNextMode(executableModes, problem);
    }

    private void pushTrace(List<SimulationJobMode> executableModes) {
        List<SimulationJobMode> newElement = new LinkedList<>(executableModes);
        trace.add(newElement);
    }

    public List<List<SimulationJobMode>> getTrace() {
        return trace;
    }

    public SimulationProblem getSimulationProblem() {
        return simulationProblem;
    }
}
