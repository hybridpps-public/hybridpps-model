package de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes.QueryableModeAttribute;

import java.util.*;

/**
 * Implementation of scheduling based combined weights.
 *
 * @author Erik Schoenherr
 */
public class CombinedPriorityRule implements SchedulingStrategy {

    private WeightedAttributes weightedAttributes;

    public CombinedPriorityRule(WeightedAttributes weightedAttributes) {
        Objects.requireNonNull(weightedAttributes);

        this.weightedAttributes = weightedAttributes;
    }

    /**
     * Chooses an executable job mode based on priorities. The method calculates the priority of each job mode through
     * a weighted sum of attributes and there respective parameters. The job mode if the highest priority is chosen and
     * returned.
     *
     * @param executableModes List of executable job modes
     * @return chosen job mode
     */
    @Override
    public SimulationJobMode getNextMode(List<SimulationJobMode> executableModes, SimulationProblem problem) {
        SimulationJobMode bestMode = null;
        double bestPriority = Integer.MIN_VALUE;

        Map<SimulationJobMode, Map<QueryableModeAttribute, Double>> modeAttributeMap = new HashMap<>();
        for (SimulationJobMode mode : executableModes) {
            Map<QueryableModeAttribute, Double> attributeValueMap = new HashMap<>();

            for (QueryableModeAttribute attribute : weightedAttributes.getWeights().keySet()) {
                attributeValueMap.put(attribute, attribute.getValue(mode, problem));
            }
            modeAttributeMap.put(mode, attributeValueMap);
        }

        for (SimulationJobMode mode : executableModes) {
            Map<QueryableModeAttribute, Double> attributeValueMap = modeAttributeMap.get(mode);

            double jobPriority = 0;
            // calculate the weighted sum of the mode attributes
            for (Map.Entry<QueryableModeAttribute, Double> entry : weightedAttributes.getWeights().entrySet()) {
                QueryableModeAttribute attribute = entry.getKey();
                double factor = entry.getValue();

                double maxAttributeValue = getMaxValue(executableModes, modeAttributeMap, attribute);
                if (maxAttributeValue == 0) {
                    continue;
                }
                // normalize value of the attribute for the mode with the max value over all modes
                jobPriority += factor * (attributeValueMap.get(attribute) / maxAttributeValue);
            }
            // update best mode if the priority of the current mode is higher than the current best
            if (jobPriority > bestPriority) {
                bestPriority = jobPriority;
                bestMode = mode;
            }
        }
        return bestMode;
    }

    private Double getMaxValue(Collection<SimulationJobMode> modes,
                               Map<SimulationJobMode, Map<QueryableModeAttribute, Double>> modeAttributeMap,
                               QueryableModeAttribute attribute) {
        double max = Integer.MIN_VALUE;
        for (SimulationJobMode mode : modes) {
            double value = modeAttributeMap.get(mode).get(attribute);

            if (value > max) {
                max = value;
            }
        }
        return max;
    }

    public WeightedAttributes getWeightedAttributes() {
        return weightedAttributes;
    }

    public void setWeightedAttributes(WeightedAttributes weightedAttributes) {
        this.weightedAttributes = weightedAttributes;
    }
}
