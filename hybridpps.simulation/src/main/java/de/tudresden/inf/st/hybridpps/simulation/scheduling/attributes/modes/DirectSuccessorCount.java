package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * Expresses the number of direct successors, i.e. is the number of modes that have this mode as direct predecessor.
 *
 * @author Erik Schönherr
 */
public class DirectSuccessorCount extends StaticModeAttribute {

    @Override
    public String getName() {
        return "direct_successor_count";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode) {
        return mode.getJob().getJastAddJob().getSuccessors().size();
    }
}
