package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.jastadd.model.*;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.SchedulingStrategy;

import java.io.File;

/**
 * Implementation of the RCPSP (Resource Constrained Project Scheduling Problem).
 *
 * @author Erik Schoenherr - Initial contribution
 */
public class RCPSP extends SimulationProblem {

  public RCPSP(ProductionSystem inputModel, SchedulingStrategy schedulingStrategy, File problemDirectory) {
    super(inputModel, schedulingStrategy, problemDirectory, "RCPSP Model");
  }

  @Override
  public String description() {
    return "This model describes a Resource-Constrained Project Scheduling Problem.";
  }

  /**
   * Unrolls a {@link Value} by extracting it's deterministic value.
   *
   * @param value the {@link Value} to unroll
   * @return the unrolled integer
   */
  @Override
  protected Integer getUnrolledValue(Value value) {
    return value.deterministicValue();
  }

  /**
   * Sets the output directory for the DESMO-J report files to the given base output directory.
   * @param baseDirectory base output directory
   * @return true
   */
  @Override
  boolean setOutputDirectory(File baseDirectory) {
    outputDirectory = baseDirectory;
    return true;
  }
}
