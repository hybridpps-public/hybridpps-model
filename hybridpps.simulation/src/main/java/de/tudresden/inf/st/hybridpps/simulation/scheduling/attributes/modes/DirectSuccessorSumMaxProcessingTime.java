package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJob;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

import java.util.List;

/**
 * Expresses the sum of the processing times of direct successors. If a job has multiple modes with different processing
 * times, the maximum processing time is chosen.
 *
 * @author Erik Schönherr
 */
public class DirectSuccessorSumMaxProcessingTime extends StaticModeAttribute {

    @Override
    public String getName() {
        return "direct_successor_sum_max_processing_time";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode) {
        List<SimulationJob> successors = mode.getJob().getSuccessors();
        double sum = 0;
        for (SimulationJob successor : successors) {
            double max = 0;
            for (SimulationJobMode sucMode : successor.getModes()) {
                max = Math.max(max, sucMode.getDuration());
            }
            sum += max;
        }
        return sum;
    }
}
