package de.tudresden.inf.st.hybridpps.simulation.priority.input;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedList;
import java.util.List;

/**
 * The Condition is part of a Statement in the meta-rule definition.
 * By default, objects of this class are always provided by deserializing rule specifications directly.
 * <p>
 * A Condition describes a logical term. Operands of such a term can be Clauses or other Conditions. Note that this
 * can be used to model nested terms.
 * The Operands are specified by their index, the operator is specified by its name (AND, OR, XOR). The yields
 * attribute is set to the boolean value which the result of the term is compared with.
 * <p>
 * example:
 *
 * <pre>{@code
 *  "clauses": [
 *         {
 *           "index": "c0",
 *           "left": "number_executable_modes",
 *           "clauseComparator": "GT",
 *           "right": 100
 *         },
 *         {
 *           "index": "c1",
 *           "left": "number_executable_modes",
 *           "clauseComparator": "LT",
 *           "right": 200
 *         },
 *         {
 *           "index": "c2",
 *           "left": "number_executable_modes",
 *           "clauseComparator": "EQ",
 *           "right": 42
 *         }
 *       ],
 *       "conditions": [
 *         {
 *           "index": "a",
 *           "operator": "AND",
 *           "operands": ["c0", "c1"],
 *           "yields": true
 *         },
 *         {
 *            "index": "b",
 *            "operator": "XOR",
 *            "operands": ["a", "c2", "c1"],
 *            "yields": false
 *         }
 *       ],
 * }</pre>
 * <p>
 * The array of operands can have any size, but is not allowed to be empty.
 *
 * <strong>A Condition can be wired or not wired which is represented as a boolean attribute. By default, the Condition
 * is not wired. All operands are only stored as their String ids. Before asRule() can be called, wire() must be called with
 * the List of all known expressions (Clauses and Conditions). This will also reference the operands as their objects which
 * are called during rule generation.</strong>
 */
public class Condition implements RuleCreator, Expression {

    private String index;
    private String operator;
    private boolean yields;

    private List<String> operands;

    @JsonIgnore
    private boolean wired;
    @JsonIgnore
    private List<Expression> wiredExpressions;

    /**
     * JSON constructor, always called by the json library.
     *
     * @param index    unique identifier of a Condition as an arbitrary String value
     * @param operator a logical operator (AND, OR, XOR)
     * @param yields   If true, the logical term is used as is. If false, the logical term is negated.
     * @param operands Array of indexes of Clauses and/or other Conditions. At least one operand must be defined.
     */
    @JsonCreator
    public Condition(
            @JsonProperty("index") String index,
            @JsonProperty("operator") String operator,
            @JsonProperty("yields") boolean yields,
            @JsonProperty("operands") List<String> operands) {
        if (operands.size() < 1) throw new IllegalArgumentException("At least one operand is required");
        this.index = index;
        this.operator = operator;
        this.yields = yields;
        this.operands = operands;
        this.wiredExpressions = new LinkedList<>();
        this.wired = false;
    }

    /**
     * Creates a String representation of the rule defined by the Condition. Doing so starts by fetching all
     * all operands and calling asRule on them recursively. The resulting operand-terms are the chained by the logical
     * operator specified. If yields is set to false, the resulting term is negated.
     * <p>
     * If the array of operands does not match the number of wired expressions (not empty, but some provided indexes are
     * invalid and cannot be found) or if the operator cannot be resolved, <strong>null is returned.</strong>
     * If the array of operands is empty, an empty string is returned.
     *
     * @param factDomain the placeholder for the object to retrieve attribute values from
     * @return rule-engine specific representation of the condition.
     */
    @Override
    public String asRule(String factDomain) {
        if (operatorToString() == null || wiredExpressions.size() != operands.size()) return null;
        StringBuilder stringBuilder = new StringBuilder("");
        String op = operatorToString();
        if (wiredExpressions.isEmpty()) return stringBuilder.toString();
        if (!yields) stringBuilder.append("!");
        stringBuilder.append("(");
        for (int i = 0; i < wiredExpressions.size(); i++) {
            RuleCreator creator = wiredExpressions.get(i).ruleCreator();
            String expressionRule = creator.asRule(factDomain);
            if (expressionRule == null) return null;
            stringBuilder.append(expressionRule);
            if (i + 1 < wiredExpressions.size()) {
                stringBuilder.append(" ").append(op).append(" ");
            }
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    public void wire(List<Expression> allExpressions) {
        for (Expression expression : allExpressions) {
            if (operands.contains(expression.getId())) {
                wiredExpressions.add(expression);
            }
        }
        wired = true;
    }

    private String operatorToString() {
        switch (operator) {
            case "AND":
                return "&";
            case "OR":
                return "|";
            case "XOR":
                return "^";
        }
        return null;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public boolean isYields() {
        return yields;
    }

    public void setYields(boolean yields) {
        this.yields = yields;
    }

    public List<String> getOperands() {
        return operands;
    }

    public void setOperands(List<String> operands) {
        this.operands = operands;
    }

    @Override
    public String getId() {
        return index;
    }

    /**
     * Provides the this instance as a ruleCreator which omits the need to cast if required.
     *
     * @return RuleCreator of the current Condition
     */
    @Override
    public RuleCreator ruleCreator() {
        return this;
    }
}
