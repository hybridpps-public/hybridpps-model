package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global.acc;

import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Static pool to provide all concrete Accumulator implementations.
 * <p> This class collects all classes implementing the Accumulator interface using the reflections api at startup.
 * During later program execution, all Accumulators are provided by a public and static get-method.
 */
public class AccumulatorPool {

    private static final Map<String, Accumulator> accumulators = new HashMap<>();

    public static Map<String, Accumulator> getAccumulators() {
        return accumulators;
    }

    static {
        Reflections reflections = new Reflections("de.tudresden.inf.st.hybridpps");
        Set<Class<? extends Accumulator>> accumulatorClasses = reflections.getSubTypesOf(Accumulator.class);
        accumulatorClasses.forEach(c -> {
            try {
                if (Modifier.isAbstract(c.getModifiers())) {
                    return;
                }
                Accumulator instance = c.getDeclaredConstructor().newInstance();
                accumulators.put(instance.getName(), instance);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
