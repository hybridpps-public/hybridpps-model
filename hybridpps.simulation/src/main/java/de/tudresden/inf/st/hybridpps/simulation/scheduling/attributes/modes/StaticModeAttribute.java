package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * This class models a static attribute whose values don't change during the simulation.
 *
 * @author Erik Schönherr
 */
public abstract class StaticModeAttribute implements QueryableModeAttribute {

    @Override
    public double getValue(SimulationJobMode mode, SimulationProblem problem) {
        Double value = mode.getAttributeValue(getName());
        if (value != null) {
            return value;
        }

        value = getAttributeValue(mode);
        mode.putAttributeValue(getName(), value);
        return value;
    }

    /**
     * Compute and return the value of the attribute for the given mode.
     *
     * @param mode the {@link SimulationJobMode} to compute the value for
     * @return value of the attribute
     */
    abstract double getAttributeValue(SimulationJobMode mode);
}
