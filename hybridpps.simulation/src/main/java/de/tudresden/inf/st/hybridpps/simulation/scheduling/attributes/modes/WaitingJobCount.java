package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.jastadd.model.Project;
import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJob;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * Expresses the number of waiting jobs in the project of the considered mode. A job "starts waiting" when all it's
 * predecessors are finished.
 *
 * @author Erik Schönherr
 */
public class WaitingJobCount extends DynamicModeAttribute {

    @Override
    public String getName() {
        return "waiting_job_count";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode, SimulationProblem problem) {
        Project project = mode.getJob().getJastAddJob().containingProject();
        double count = 0.0;

        for (SimulationJob job : problem.getExecutableJobs()) {
            if (job.getJastAddJob().containingProject() == project) {
                count += 1;
            }
        }
        return count;
    }
}
