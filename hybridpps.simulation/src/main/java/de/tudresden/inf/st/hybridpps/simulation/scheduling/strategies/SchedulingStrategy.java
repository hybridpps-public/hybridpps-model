package de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.*;

import java.util.List;

/**
 * Template for concrete scheduling strategies like FIFO or simple priority rules.
 *
 * @author Erik Schoenherr - Initial contribution
 */
public interface SchedulingStrategy {

    /**
     * Here the concrete strategy is implemented. The Method takes a List of modes and then chooses one of them
     * depending on some computations.
     *
     * @param executableModes List of all currently executable/schedulable modes
     * @param problem the simulation model
     * @return the chosen {@link SimulationJobMode}
     */
    SimulationJobMode getNextMode(List<SimulationJobMode> executableModes, SimulationProblem problem);
}
