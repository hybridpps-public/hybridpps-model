package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJob;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global.acc.Accumulator;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global.acc.Mean;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes.QueryableModeAttribute;

import java.util.*;

/**
 * Static class to provide the calculate() method for CombinedAttributes which are composed from a
 * QueryableModeAttribute and an Accumulator function.
 */
public class CombinedAttribute {

    private static final Mean mean = new Mean();

    /**
     * This method calculates a combined attribute which is composed from a QueryableModeAttribute and an Accumulator
     * function.
     * <p>1. The mode-attribute is calculated for each mode in the executableModes queue. The result is a (mode -> value) mapping.</p>
     * <p>2. The results of all modes are grouped by their job and the mode-wise mean for each job is calculated. The result is a (job -> value) mapping.</p>
     * <p>3. The results of each job are combined by the provided Accumulator function. For example "sum" adds all job-wise values. Every Accumulator produces a single scalar which is returned.</p>
     *
     * @param attribute       the QueryableModeAttribute that provides a value for each mode of the queue
     * @param accumulator     the Accumulator used to calculate a single value from all mode attributes
     * @param executableModes the queue of executable modes
     * @param problem         the SimulationProblem specification
     * @return calculated value from provided attribute and accumulator
     */
    public static double calculate(QueryableModeAttribute attribute, Accumulator accumulator, List<SimulationJobMode> executableModes, SimulationProblem problem) {
        //group modes by job
        Map<SimulationJob, Set<SimulationJobMode>> modesByJob = new HashMap<>();
        executableModes.forEach(mode -> {
            Set<SimulationJobMode> modes = modesByJob.computeIfAbsent(mode.getJob(), k -> new HashSet<>());
            modes.add(mode);
        });

        double[] results = new double[modesByJob.keySet().size()];
        Set<SimulationJob> keys = modesByJob.keySet();
        int counter = 0;

        for (SimulationJob key : keys) {
            SimulationJobMode[] modes = modesByJob.get(key).toArray(SimulationJobMode[]::new);
            double[] values = new double[modes.length];
            for (int i = 0; i < modes.length; i++) {
                values[i] = attribute.getValue(modes[i], problem);
            }
            results[counter] = mean.calculate(values);
            counter++;
        }
        //apply the accumulator to the result array
        return accumulator.calculate(results);
    }

}
