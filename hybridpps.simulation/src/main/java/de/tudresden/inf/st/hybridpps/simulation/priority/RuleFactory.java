package de.tudresden.inf.st.hybridpps.simulation.priority;

import de.tudresden.inf.st.hybridpps.simulation.priority.input.RuleSet;
import org.evrete.KnowledgeService;
import org.evrete.api.Knowledge;
import org.evrete.api.RuleBuilder;
import org.evrete.api.StatefulSession;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Class with static factory methods to create rules and extending existent rule models
 */
public class RuleFactory {

    /**
     * Static factory method to initialise the state of the rule engine.
     * <p>
     * Here, all Statements of the provided RuleSet are transformed in their rule-string representations. For each Statement,
     * an individual knowledge (concrete RuleEngine Session) is created and loaded with a rule representing the Statement.
     * The reason that each Statement requires its own knowledge is to fire each Statement independently and only if needed instead
     * of evaluation always all at once.
     * <p>
     * The result is provided as a Map consisting of the Statement indices as keys and the RuleEngine sessions containing the corresponding statement as values.
     * Following the definition of a meta-ruleset, there must be at least one element with the key "root" in the map, which must be evaluated (fired) first.
     *
     * @param knowledgeService RuleEngine factory to create new Sessions
     * @param ruleSet          The ruleset to represent
     * @param executor         A Consumer called by the RuleEngine for each result (if fired later on)
     * @param factDomainName   The placeholder for the object to query Attributes from
     * @param factDomain       The class the placeholder variable is type of (must provide the query(String attr) method)
     * @return Map of the initialised RuleEngine Setup.
     */
    public static Map<String, StatefulSession> create(KnowledgeService knowledgeService, RuleSet ruleSet, Consumer<Boolean> executor,
                                                      String factDomainName, Class<?> factDomain) {
        Map<String, StatefulSession> sessions = new HashMap<>();

        ruleSet.getStatements().forEach(statement -> {
            String index = statement.getIndex();
            Knowledge knowledge = knowledgeService.newKnowledge();
            RuleBuilder<Knowledge> ruleBuilder = knowledge.newRule(index);

            String ruleString = statement.asRule(factDomainName);
            ruleBuilder
                    .forEach("$" + factDomainName, factDomain)
                    .where(ruleString)
                    .execute(rhsContext -> executor.accept(true));
            StatefulSession session = knowledge.newStatefulSession();
            sessions.put(index, session);
        });
        return sessions;
    }

}
