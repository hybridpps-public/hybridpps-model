package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.jastadd.model.*;
import de.tudresden.inf.st.hybridpps.jastadd.model.Resource;
import de.tudresden.inf.st.hybridpps.simulation.model.*;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.SchedulingStrategy;
import desmoj.core.simulator.*;
import desmoj.core.simulator.Queue;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Main class of the entire simulation that represents the simulation model. Responsible for initialising, starting and
 * executing the simulation.
 *
 * @author Erik Schönherr
 */
public abstract class SimulationProblem extends Model {

    private final static Logger logger = LogManager.getLogger(SimulationProblem.class);

    private List<SimulationJob> jobList;

    private Map<SimulationJobMode, Map<String, Double>> attributes;

    private Queue<SimulationJob> executableJobs;

    private ConcreteSolution solution;

    private EventHandler eventHandler;

    private ResourceManager resourceManager;

    private final ProductionSystem inputModel;

    private SchedulingStrategy schedulingStrategy;

    private File problemDirectory;
    protected File outputDirectory;

    /**
     * Constructor of the {@link SimulationProblem}.
     *
     * @param inputModel production system holding all the problem specifications
     * @param schedulingStrategy the {@link SchedulingStrategy} to use
     * @param problemDirectory the base output directory
     * @param modelName name for the model (needed for super constructor call)
     */
    public SimulationProblem(ProductionSystem inputModel,SchedulingStrategy schedulingStrategy, File problemDirectory, String modelName) {
        super(null, modelName, true, true);

        this.inputModel = inputModel;
        this.schedulingStrategy = schedulingStrategy;
        this.problemDirectory = problemDirectory;
        this.attributes = new HashMap<>();
        this.eventHandler = new EventHandler(this);
        this.resourceManager = new ResourceManager();
    }

    /**
     * Activates dynamic model components (events). This method is used to place all events or processes on the internal
     * event list of the simulator which are necessary to start the simulation.
     */
    @Override
    public void doInitialSchedules() {
        // create the StartFirstJobEvent
        StartFirstJobEvent startJob = new StartFirstJobEvent(this, "Start First Job Event", true);

        // schedule for start of simulation
        startJob.schedule(new TimeSpan(0));
    }

    /**
     * Initialises static model components like distributions and queues.
     */
    @Override
    public void init() {

        executableJobs = new Queue<>(this, "Executable Job Queue", true, true);

        jobList = new ArrayList<>();

        // initialise solution
        solution = new ConcreteSolution();

        initWithModel();
    }

    /**
     * Initialises the simulation with data from a {@link ProductionSystem}.
     */
    private void initWithModel() {
        Map<ResourceGroup, SimulationResourceGroup> resourceGroupMap = new HashMap<>();
        Map<Job, SimulationJob> jobMap = new HashMap<>();
        Map<TypeRepresentative, SimulationTypeRepresentative> typeRepresentativeMap = new HashMap<>();
        List<SimulationResourceGroup> resourceGroupList = new ArrayList<>();

        for (TypeRepresentative tr : inputModel.getTypeRepresentatives()) {
            SimulationTypeRepresentative simulationTR = new SimulationTypeRepresentative();
            simulationTR.setSetupTime(getUnrolledValue(tr.getSetupTime()));

            simulationTR.setJastAddTypeRepresentative(tr);
            typeRepresentativeMap.put(tr, simulationTR);
        }

        // global resources
        for (ResourceGroup resourceGroup : inputModel.getResourceGroupList()) {
            SimulationResourceGroup simulationResourceGroup = transform(resourceGroup, resourceGroupMap);
            logger.info("Global Resource " + resourceGroup.getName() + " Capacity: " + simulationResourceGroup.getTotalCapacity());
            resourceGroupList.add(simulationResourceGroup);
        }

        for (Project project : inputModel.getProjectList()) {
            // project-local resources
            for (ResourceGroup resourceGroup : project.getResourceGroupList()) {
                for (SimulationResourceGroup simResourceGroup : resourceGroupList) {
                    if (simResourceGroup.getJastAddResourceGroup().getName().equals(resourceGroup.getName())) {
                        resourceGroupMap.put(resourceGroup, simResourceGroup);
                        break;
                    }
                }
                // if there exists a global simulation resource for the resource group it gets returned by transform
                SimulationResourceGroup simulationResourceGroup = transform(resourceGroup, resourceGroupMap);
                if (!simulationResourceGroup.isGlobal()) {
                    logger.info("Local Resource " + resourceGroup.getName() + " Capacity: " + simulationResourceGroup.getTotalCapacity());
                }
            }

            // project jobs
            for (Job job : project.getJobList()) {
                SimulationJob simulationJob = new SimulationJob(this, "P" + project.getName() + "-J" + job.getName(), true);
                // System.out.println("Jobname: " + job.getName() + " SimJobName: " + simulationJob.getName());
                simulationJob.setJastAddJob(job);
                simulationJob.setTypeRepresentative(typeRepresentativeMap.get(job.getTypeRepresentative()));
                jobMap.put(job, simulationJob);

                // add Modes
                for (int i = 0; i < job.getNumMode(); i++) {
                    Mode mode = job.getMode(i);
                    // get duration
                    Integer duration = getUnrolledValue(mode.getDuration());

                    // create Mode
                    SimulationJobMode simulationJobMode = new SimulationJobMode(this, "P" + project.getName() + "-J" + job.getName() + "-M" + mode.getName(), true);
                    simulationJobMode.setDuration(duration);
                    simulationJobMode.setJob(simulationJob);
                    simulationJobMode.setJastAddMode(mode);
                    // add required resources
                    for (int r = 0; r < mode.getNumResourceRequirement(); r++) {
                        int cap = mode.getResourceRequirement(r).getRequiredCapacity();
                        ResourceGroup res = mode.getResourceRequirement(r).getRequiredResourceGroup();
                        SimulationResourceGroup group = resourceGroupMap.get(res);
                        simulationJobMode.putRequiredResources(group, cap);
                    }

                    logger.info("Job: " + job.getName() + " Mode: " + mode.getName() + " SimulationJob: " + simulationJob.getName() + " SimulationMode: " + simulationJobMode.getName());

                    simulationJob.addMode(simulationJobMode);
                }
                jobList.add(simulationJob);
            }
        }
        // add precedence relations after all jobs are created
        for (SimulationJob simJob: jobList){
            List<Job> predecessorList = simJob.getJastAddJob().getPredecessorList();
            for (Job job: predecessorList){
                SimulationJob precedingJob = jobMap.get(job);
                simJob.addPredecessor(precedingJob);
            }
        }

        // add successors to each job
        for (SimulationJob simJob : jobList) {
            for (Job successor : simJob.getJastAddJob().getSuccessors()) {
                SimulationJob successorSimJob = jobMap.get(successor);
                simJob.addSuccessor(successorSimJob);
            }
        }
    }

    /**
     * Transforms a {@link ResourceGroup} to a {@link SimulationResourceGroup} if it wasn't already transformed.
     * This also adds the transformed resource group to the {@code resourceGroupMap}
     *
     * @param resourceGroup the resource group to transform
     * @param resourceGroupMap a map containing all already initialised {@link SimulationResourceGroup}s with their
     *                         corresponding {@link ResourceGroup}
     * @return the generated {@link SimulationResourceGroup}
     */
    private SimulationResourceGroup transform(ResourceGroup resourceGroup, Map<ResourceGroup, SimulationResourceGroup> resourceGroupMap) {
        return resourceGroupMap.computeIfAbsent(resourceGroup, r -> {
            int capacity = getUnrolledValue(r.getCapacity());
            SimulationResourceGroup simulationResourceGroup = new SimulationResourceGroup();
            simulationResourceGroup.setJastAddResourceGroup(resourceGroup);
            resourceManager.putQueue(simulationResourceGroup, this);
            for (int i = 0; i < capacity; i++) {
                Resource res = new Resource();
                res.setIndex(i);
                resourceGroup.addResource(res);

                SimulationResource simulationResource = new SimulationResource(this, resourceGroup.getName(), false);
                simulationResource.setResourceGroup(simulationResourceGroup);
                simulationResource.setJastAddResource(res);
                simulationResourceGroup.addResource(simulationResource);
                resourceManager.insertFreeResource(simulationResource);
            }


            return simulationResourceGroup;
        });
    }

    /**
     * Unrolls a value. The process will be different depending on whether or not the concrete simulation problem is
     * deterministic or stochastic.
     *
     * @param value the {@link Value} to unroll
     * @return the unrolled integer
     */
    protected abstract Integer getUnrolledValue(Value value);

    /**
     * Solves a resource constrained project scheduling problem based on a {@link ProductionSystem} and a scheduling
     * strategy. The simulation is run once and then the solution gets returned as a {@link DeterministicSolution}.
     *
     * @param inputModel problem model
     * @param schedulingStrategy concrete scheduling strategy
     * @return deterministic solution of problem
     */
    public static DeterministicSolution startRCPSP(ProductionSystem inputModel, SchedulingStrategy schedulingStrategy,
                                                   boolean report) {
        File problemDirectory = report ? createProblemDirectory("RCPSP") : null;
        RCPSP problemModel = new RCPSP(inputModel, schedulingStrategy, problemDirectory);
        ConcreteSolution concreteSolution = problemModel.run();
        concreteSolution.setProductionSystem(inputModel);
        return new DeterministicSolution(concreteSolution);
    }

    /**
     * Solves a stochastic resource constrained project scheduling problem based on a {@link ProductionSystem} and a
     * scheduling strategy. The simulation is run multiple times based on the defined scenario size and then the
     * concrete solutions get returned as a {@link StochasticSolution}.
     *
     * @param inputModel problem model
     * @param schedulingStrategy concrete scheduling strategy
     * @return deterministic solution of problem
     */
    public static StochasticSolution startSRCPSP(ProductionSystem inputModel, SchedulingStrategy schedulingStrategy,
                                                 boolean report) {
        File problemDirectory = report ? createProblemDirectory("SRCPSP") : null;
        StochasticSolution stochasticSolution = new StochasticSolution();
        for (int runNumber = 0; runNumber < inputModel.getScenarioSize(); runNumber++) {
            SRCPSP problemModel = new SRCPSP(inputModel, schedulingStrategy, problemDirectory, runNumber);
            // create model here, then give to start
            ConcreteSolution result = problemModel.run();
            result.setProductionSystem(inputModel);
            stochasticSolution.addConcreteSolution(result);
        }

        return stochasticSolution;
    }

    /**
     * Creates the output directory for the DESMO-J report files.
     *
     * @param baseName name for the output directory
     * @return the created directory as {@link File}
     */
    private static File createProblemDirectory(String baseName) {
        Path experimentsDir = Paths.get("experiments");
        if (experimentsDir.toFile().mkdir()) {
            logger.info("Created experiments directory.");
        };
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd-HH-mm-ss")
                .withZone(ZoneId.systemDefault());
        String now = formatter.format(Instant.now());

        File problemDir = experimentsDir.resolve(baseName + "-" + now).toFile();
        int i = 2;
        while (!problemDir.mkdir()) {
            problemDir = experimentsDir.resolve(baseName + "-" + now + "_" + i++).toFile();
        }
        return problemDir;
    }

    /**
     * Sets the concrete output directory for the DESMO-J report files.
     *
     * @param baseDirectory the base output directory
     * @return whether or not the directory was successfully set
     */
    abstract boolean setOutputDirectory(File baseDirectory);

    /**
     * This method actually starts the simulation experiment, generating a {@link ConcreteSolution} and returning it.
     *
     * @return solution of the experiment
     */
    public ConcreteSolution run() {
        Experiment experiment;
        if (problemDirectory == null) {
            experiment = new Experiment("RCPSPExperiment", false);
        } else {
            setOutputDirectory(problemDirectory);
            experiment = new Experiment("RCPSPExperiment", outputDirectory.toPath().toAbsolutePath().toString());
        }
        // ATTENTION, since the name of the experiment is used in the names of the
        // output files, you have to specify a string that's compatible with the
        // filename constraints of your computer's operating system.

        // connect experiment and model
        this.connectToExperiment(experiment);
        // set experiment parameters
        experiment.setShowProgressBar(false);  // display a progress bar (or not)
        if (LogManager.getRootLogger().getLevel().isMoreSpecificThan(Level.INFO)) {
            experiment.setSilent(true);
        }
        experiment.stop(new StopCondition(this, "Stop Condition", true));
        experiment.tracePeriod(new TimeInstant(0), new TimeInstant(100, TimeUnit.MINUTES));
        // set the period of the trace
        experiment.debugPeriod(new TimeInstant(0), new TimeInstant(50, TimeUnit.MINUTES));   // and debug output
        // ATTENTION!
        // Don't use too long periods. Otherwise a huge HTML page will be created which crashes Netscape
        // start the experiment at simulation time 0.0

        experiment.start();

        // generate the report (and other output files)
        experiment.report();

        // stop all threads still alive and close all output files
        experiment.finish();

        return this.solution;
    }

    /**
     * This method is called when a new job mode might be able to be scheduled. It's possible that none or even multiple
     * modes can be scheduled. When multiple Modes are schedulable, the defined scheduling strategy is used to decide
     * for the next mode. A recursive call is needed to check whether or not more modes can be scheduled.
     */
    public void handleJobEvent() {
        attributes.clear();
        eventHandler.handleJobEvent();
    }

    public List<SimulationJob> getJobs() {
        return jobList;
    }

    public Queue<SimulationJob> getExecutableJobs() {
        return executableJobs;
    }

    public SchedulingStrategy getSchedulingStrategy() {
        return schedulingStrategy;
    }

    public ConcreteSolution getSolution() {
        return solution;
    }

    public Double getAttributeValue(SimulationJobMode mode, String name) {
        Map<String, Double> modeAttributes = attributes.get(mode);
        if (modeAttributes == null) {
            return null;
        }
        return modeAttributes.get(name);
    }

    public void putAttributeValue(SimulationJobMode mode, String name, double value) {
        attributes.putIfAbsent(mode, new HashMap<>());
        attributes.get(mode).put(name, value);
    }

    public EventHandler getEventHandler() {
        return eventHandler;
    }

    public ResourceManager getResourceManager() {
        return resourceManager;
    }
}
