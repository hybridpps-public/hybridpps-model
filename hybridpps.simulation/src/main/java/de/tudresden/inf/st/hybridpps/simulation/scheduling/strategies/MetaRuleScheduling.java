package de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;
import de.tudresden.inf.st.hybridpps.simulation.priority.RuleFactory;
import de.tudresden.inf.st.hybridpps.simulation.priority.SimulationState;
import de.tudresden.inf.st.hybridpps.simulation.priority.input.Follower;
import de.tudresden.inf.st.hybridpps.simulation.priority.input.RuleSet;
import de.tudresden.inf.st.hybridpps.simulation.priority.input.Statement;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.SimplePriorityRulePool;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.parameters.CPRLoader;
import org.evrete.KnowledgeService;
import org.evrete.api.StatefulSession;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;


public class MetaRuleScheduling implements SchedulingStrategy {

    private SimulationState simulationState;
    private Map<String, StatefulSession> sessions;

    private final DynamicConfiguration dynamicConfiguration;
    private final RuleSet ruleSet;

    public MetaRuleScheduling(Path metaRuleDefinition) throws IOException {

        dynamicConfiguration = new DynamicConfiguration();
        ruleSet = RuleSet.fromJson(metaRuleDefinition);

        Consumer<Boolean> executor = e -> {
                dynamicConfiguration.setWasSuccess(true);
        };

        KnowledgeService knowledgeService = new KnowledgeService();
        final String STATE = "s";
        sessions = RuleFactory.create(knowledgeService, ruleSet, executor, STATE, SimulationState.class);
    }

    @Override
    public SimulationJobMode getNextMode(List<SimulationJobMode> executableModes, SimulationProblem problem) {
        if(simulationState == null){
            simulationState = new SimulationState(problem);
            sessions.values().forEach(session -> session.insert(simulationState));
        }
        simulationState.update(executableModes);

        Statement currentStatement = ruleSet.getStatement("root").get();
        String nextStrategyName;
        List<String> nextStrategyParams;

        while (true){
            dynamicConfiguration.setExecutedStatement(currentStatement);
            dynamicConfiguration.setWasSuccess(false);

            //At this point fire() triggers the rule engine which reasons over the simulation state and writes a new
            //dynamic Configuration synchronously. So when the call returns the new configuration is available
            sessions.get(currentStatement.getIndex()).clear();
            sessions.get(currentStatement.getIndex()).insertAndFire(simulationState);

            Follower follower = dynamicConfiguration.isWasSuccess() ? currentStatement.getOnTrue() : currentStatement.getOnFalse();
            if(!follower.isStatement()){
                nextStrategyName = follower.getFollower();
                nextStrategyParams = follower.getParams();
                break;
            }
            String nextStatement = follower.getNext();
            currentStatement = ruleSet.getStatement(nextStatement).get();
        }

        if(nextStrategyName.equals("cpr")){
            String cprConf = nextStrategyParams.get(0);
            SchedulingStrategy nextStrategy = null;
            try {
                nextStrategy = CPRLoader.load(cprConf);
            } catch (IOException e) {
                e.printStackTrace();
            }
            assert (nextStrategy != null);
            return nextStrategy.getNextMode(executableModes, problem);
        }else {
            SchedulingStrategy nextStrategy = SimplePriorityRulePool.getRule(nextStrategyName);
            assert (nextStrategy != null);
            return nextStrategy.getNextMode(executableModes, problem);
        }
    }

}
