package de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies;

import de.tudresden.inf.st.hybridpps.simulation.priority.input.Follower;
import de.tudresden.inf.st.hybridpps.simulation.priority.input.Statement;

public class DynamicConfiguration {

    private Statement executedStatement = null;
    private boolean wasSuccess = false;

    public Statement getExecutedStatement() {
        return executedStatement;
    }

    public void setExecutedStatement(Statement executedStatement) {
        this.executedStatement = executedStatement;
    }

    public boolean isWasSuccess() {
        return wasSuccess;
    }

    public void setWasSuccess(boolean wasSuccess) {
        this.wasSuccess = wasSuccess;
    }

}
