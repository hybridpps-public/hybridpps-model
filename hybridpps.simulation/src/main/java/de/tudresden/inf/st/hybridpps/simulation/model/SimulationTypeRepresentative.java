package de.tudresden.inf.st.hybridpps.simulation.model;

import de.tudresden.inf.st.hybridpps.jastadd.model.TypeRepresentative;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;

/**
 * A type representative for the simulation.
 *
 * @author Erik Schoenherr
 */
public class SimulationTypeRepresentative {

    private int setupTime;

    private TypeRepresentative jastAddTypeRepresentative;

    public int getSetupTime() {
        return setupTime;
    }

    public void setSetupTime(int setupTime) {
        this.setupTime = setupTime;
    }

    public TypeRepresentative getJastAddTypeRepresentative() {
        return jastAddTypeRepresentative;
    }

    public void setJastAddTypeRepresentative(TypeRepresentative jastAddTypeRepresentative) {
        this.jastAddTypeRepresentative = jastAddTypeRepresentative;
    }
}
