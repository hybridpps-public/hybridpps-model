package de.tudresden.inf.st.hybridpps.simulation.model;

import de.tudresden.inf.st.hybridpps.jastadd.model.Mode;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class representing a single mode in the simulation model.
 *
 * @author Sophie Ziemann
 */
public class SimulationJobMode extends Entity {

    private SimulationJob job;

    private int duration;

    private Map<SimulationResourceGroup, Integer> requiredResources = new HashMap<>();

    private Map<String, Double> attributes = new HashMap<>();

    List<SimulationResource> activeResources = new ArrayList<>();

    private Mode jastAddMode;

    /**
     * Constructor of the job mode entity.
     *
     * @param owner the model this entity belongs to
     * @param name this mode's name
     * @param showInTrace flag to indicate if this entity shall produce output
     *                    for the trace
     */
    public SimulationJobMode(Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
    }

    public SimulationJob getJob(){
        return job;
    }

    public void setJob(SimulationJob newJob){
        job = newJob;
    }

    public int getDuration(){
        return duration;
    }

    public void setDuration(int newDuration){
        duration = newDuration;
    }

    public Map<SimulationResourceGroup, Integer> getRequiredResources(){
        return requiredResources;
    }

    public void putRequiredResources(SimulationResourceGroup group, int amount){
        requiredResources.put(group, amount);
    }

    public Double getAttributeValue(String name) {
        return attributes.get(name);
    }

    public void putAttributeValue(String name, double value) {
        attributes.put(name, value);
    }

    public List<SimulationResource> getActiveResources() {
        return activeResources;
    }

    public void addActiveResource(SimulationResource resource) {
        activeResources.add(resource);
    }

    /**
     * Finds the resources currently active on the mode for a given resource group.
     *
     * @param group the {@link SimulationResourceGroup} to look for
     * @return List of the active resources belonging to the group
     */
    public List<SimulationResource> getActiveResourcesForGroup(SimulationResourceGroup group) {
        return activeResources.stream()
                .filter(r -> r.getResourceGroup().equals(group))
                .collect(Collectors.toList());
    }

    public Mode getJastAddMode(){
        return jastAddMode;
    }

    public void setJastAddMode(Mode mode){
        jastAddMode = mode;
    }
}
