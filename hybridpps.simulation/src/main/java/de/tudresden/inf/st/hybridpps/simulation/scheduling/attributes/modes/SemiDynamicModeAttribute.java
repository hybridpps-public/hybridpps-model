package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * This class models a semi-dynamic attribute whose values can change between events.
 *
 * @author Erik Schönherr
 */
public abstract class SemiDynamicModeAttribute implements QueryableModeAttribute {

    @Override
    public double getValue(SimulationJobMode mode, SimulationProblem problem) {
        Double value = problem.getAttributeValue(mode, getName());
        if (value != null) {
            return value;
        }

        value = getAttributeValue(mode, problem);
        problem.putAttributeValue(mode, getName(), value);
        return value;
    }

    /**
     * Compute and return the value of the attribute for the given mode.
     *
     * @param mode the {@link SimulationJobMode} to compute the value for
     * @param problem simulation model
     * @return value of the attribute
     */
    abstract double getAttributeValue(SimulationJobMode mode, SimulationProblem problem);
}
