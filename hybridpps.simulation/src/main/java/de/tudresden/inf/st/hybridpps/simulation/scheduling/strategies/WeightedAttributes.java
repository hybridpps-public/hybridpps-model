package de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies;

import de.tudresden.inf.st.hybridpps.simulation.scheduling.AttributePool;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes.QueryableModeAttribute;

import java.util.HashMap;
import java.util.Map;

/**
 * Class used to hold the attributes and weights for usage in a {@link CombinedPriorityRule}.
 *
 * @author Erik Schönherr
 */
public class WeightedAttributes {

    private final Map<QueryableModeAttribute, Double> weights = new HashMap<>();

    public void put(String name, double weight) {
        this.put(AttributePool.getModeAttribute(name), weight);
    }

    public void put(QueryableModeAttribute attribute, double weight) {
        this.weights.put(attribute, weight);
    }

    public Map<QueryableModeAttribute, Double> getWeights() {
        return weights;
    }

    public Double getWeightForAttribute(String name) {
        return getWeightForAttribute(AttributePool.getModeAttribute(name));
    }

    public Double getWeightForAttribute(QueryableModeAttribute attribute) {
        return this.weights.get(attribute);
    }
}
