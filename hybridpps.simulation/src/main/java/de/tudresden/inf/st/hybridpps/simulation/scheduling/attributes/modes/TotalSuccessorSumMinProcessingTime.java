package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJob;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

import java.util.Set;

/**
 * Expresses the sum of the processing times of all successors. If a job has multiple modes with different processing
 * times, the minimum processing time is chosen.
 *
 * @author Erik Schönherr
 */
public class TotalSuccessorSumMinProcessingTime extends StaticModeAttribute {

    @Override
    public String getName() {
        return "total_successor_sum_min_processing_time";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode) {
        Set<SimulationJob> successors = mode.getJob().getAllSuccessors();
        double sum = 0;
        for (SimulationJob successor : successors) {
            double min = Integer.MAX_VALUE;
            for (SimulationJobMode sucMode : successor.getModes()) {
                min = Math.min(min, sucMode.getDuration());
            }
            sum += min;
        }
        return sum;
    }
}
