package de.tudresden.inf.st.hybridpps.simulation.scheduling;

import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.SimplePriorityRule;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

/**
 * This class holds references to all non-abstract classes implementing {@link SimplePriorityRule}. It can be used to
 * translate simple priority rule names to actual simple priority rule implementations.
 */
public class SimplePriorityRulePool {

    private static final Set<SimplePriorityRule> sprs = new HashSet<>();

    static {
        Reflections reflections = new Reflections("de.tudresden.inf.st.hybridpps");
        Set<Class<? extends SimplePriorityRule>> attributeClasses = reflections.getSubTypesOf(SimplePriorityRule.class);

        attributeClasses.forEach(c -> {
            try {
                if (Modifier.isAbstract(c.getModifiers())) {
                    return;
                }
                sprs.add(c.getDeclaredConstructor().newInstance());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Translates a given simple priority rule name to a simple priority rule object.
     *
     * @param name name of the rule
     * @return found {@link SimplePriorityRule}
     */
    public static SimplePriorityRule getRule(String name) {
        for (SimplePriorityRule spr : sprs) {
            if (spr.getName().equals(name)) {
                return spr;
            }
        }
        return null;
    }
}
