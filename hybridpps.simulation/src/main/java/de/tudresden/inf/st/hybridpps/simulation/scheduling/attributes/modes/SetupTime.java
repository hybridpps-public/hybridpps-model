package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.modes;

import de.tudresden.inf.st.hybridpps.simulation.ResourceUtils;
import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

/**
 * Expresses the time needed to set up for the mode. Equal to zero if no setup is currently needed.
 *
 * @author Erik Schönherr
 */
public class SetupTime extends DynamicModeAttribute {

    @Override
    public String getName() {
        return "setup_time";
    }

    @Override
    public double getAttributeValue(SimulationJobMode mode, SimulationProblem problem) {
        return ResourceUtils.needsSetup(mode, problem) ? mode.getJob().getTypeRepresentative().getSetupTime() : 0;
    }
}
