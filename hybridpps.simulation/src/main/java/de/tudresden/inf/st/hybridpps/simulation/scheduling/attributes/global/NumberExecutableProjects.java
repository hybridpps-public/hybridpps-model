package de.tudresden.inf.st.hybridpps.simulation.scheduling.attributes.global;

import de.tudresden.inf.st.hybridpps.jastadd.model.Project;
import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A simple QueryableGlobalAttribute to get the number of projects with currently executable modes in the queue.
 */
public class NumberExecutableProjects implements QueryableGlobalAttribute {

    @Override
    public String getName() {
        return "number_executable_projects";
    }

    @Override
    public double getValue(List<SimulationJobMode> executableModes, SimulationProblem problem) {
        Set<Project> projects = new HashSet<Project>();
        executableModes.forEach(mode -> projects.add(mode.getJob().getJastAddJob().containingProject()));
        return projects.size();
    }
}
