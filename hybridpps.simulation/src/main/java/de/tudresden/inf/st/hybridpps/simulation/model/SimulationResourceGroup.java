package de.tudresden.inf.st.hybridpps.simulation.model;

import de.tudresden.inf.st.hybridpps.jastadd.model.ResourceGroup;
import desmoj.core.simulator.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class representing a resource group in the simulation model.
 *
 * @author Sophie Ziemann
 */
public class SimulationResourceGroup {

  private List<SimulationResource> resources;

  private ResourceGroup jastAddResourceGroup;

    /**
     * Constructor of the resource entity.
     */
  public SimulationResourceGroup() {
    this.resources = new ArrayList<>();
  }

  /**
   * Returns the total number of resources in the group.
   *
   * @return total number of resources
   */
  public Integer getTotalCapacity() {
    return resources.size();
  }

  public List<SimulationResource> getResources() {
    return resources;
  }

  public void addResource(SimulationResource resource) {
      this.resources.add(resource);
  }

  /**
   * Checks whether or not the resource group is global, i.e. it doesn't belong to a single project but directly to the
   * production system.
   *
   * @return true if the resource group is global
   */
  public boolean isGlobal() {
        return jastAddResourceGroup.isGlobal();
    }

  public ResourceGroup getJastAddResourceGroup(){
    return jastAddResourceGroup;
  }

  public void setJastAddResourceGroup(ResourceGroup resourceGroup){
    jastAddResourceGroup = resourceGroup;
  }
}
