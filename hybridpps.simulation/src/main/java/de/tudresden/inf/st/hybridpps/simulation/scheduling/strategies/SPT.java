package de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies;

import de.tudresden.inf.st.hybridpps.simulation.ResourceUtils;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;
import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies.SimplePriorityRule;

import java.util.List;

/**
 * Implementation of SPT (Shortest Processing Time) scheduling.
 *
 * @author Erik Schoenherr - Initial contribution
 */
public class SPT extends SimplePriorityRule {

    @Override
    public String getName() {
        return "spt";
    }

    /**
     * Chooses the executable job mode with the shortest processing time. Takes setup time into account. If two modes
     * share the same processing time the first mode in the list will be chosen.
     *
     * @param executableModes List of executable job modes
     * @return chosen job mode
     */
    @Override
    public SimulationJobMode getNextMode(List<SimulationJobMode> executableModes, SimulationProblem problem) {
        SimulationJobMode bestMode = null;
        int spt = Integer.MAX_VALUE;

        for (SimulationJobMode mode : executableModes) {
            int duration = mode.getDuration();

            // add setup time if the mode needs setup
            if (ResourceUtils.needsSetup(mode, problem)) {
                duration += mode.getJob().getTypeRepresentative().getSetupTime();
            }
            // update best mode if the duration of the current mode is lower than the current best
            if (duration < spt) {
                spt = duration;
                bestMode = mode;
            }
        }
        return bestMode;
    }
}
