package de.tudresden.inf.st.hybridpps.simulation.scheduling.strategies;

import de.tudresden.inf.st.hybridpps.jastadd.model.Project;
import de.tudresden.inf.st.hybridpps.simulation.SimulationProblem;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJob;
import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJobMode;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of MWKR (Most work remaining) scheduling.
 *
 * @author Erik Schoenherr
 */
public class MWKR extends SimplePriorityRule {

    @Override
    public String getName() {
        return "mwkr";
    }

    /**
     * Chooses the executable job mode which is in the project with the most work remaining.
     * IMPORTANT: If there are multiple job modes in the same project, the first one in the input list will be taken
     * (which equals the first mode of the first job in the waiting queue).
     *
     * @param executableModes list of executable job modes
     * @return chosen job mode
     */
    @Override
    public SimulationJobMode getNextMode(List<SimulationJobMode> executableModes, SimulationProblem problem) {
        List<SimulationJob> jobs = problem.getJobs().stream()
                .filter(j -> j.getStatus() == SimulationJob.Status.NOT_STARTED)
                .collect(Collectors.toList());


        SimulationJobMode bestMode = null;
        int mwkr = Integer.MIN_VALUE;

        for (SimulationJobMode mode : executableModes) {
            Project containingProject = mode.getJastAddMode().containingJob().containingProject();
            List<SimulationJob> jobsInSameProject = jobs.stream()
                    .filter(j -> j.getJastAddJob().containingProject().equals(containingProject))
                    .collect(Collectors.toList());

            int remainingWork = getSumMinProcessingTime(jobsInSameProject);

            // update best mode if the duration of the current mode is lower than the current best
            if (remainingWork > mwkr) {
                mwkr = remainingWork;
                bestMode = mode;
            }
        }
        return bestMode;
    }

    public int getSumMinProcessingTime(List<SimulationJob> jobs) {
        int sum = 0;
        for (SimulationJob job : jobs) {
            int min = Integer.MAX_VALUE;
            for (SimulationJobMode modes : job.getModes()) {
                min = Math.min(min, modes.getDuration());
            }
            sum += min;
        }
        return sum;
    }
}
