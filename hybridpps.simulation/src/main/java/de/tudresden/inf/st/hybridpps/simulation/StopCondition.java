package de.tudresden.inf.st.hybridpps.simulation;

import de.tudresden.inf.st.hybridpps.simulation.model.SimulationJob;
import desmoj.core.simulator.ModelCondition;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the condition on which the simulation is supposed to stop.
 *
 * @author Erik Schönherr
 */
public class StopCondition extends ModelCondition {

    List<SimulationJob> unfinishedJobs;

    /**
     * Constructor of the {@link StopCondition}.
     *
     * @param model the simulation model
     * @param name name of the condition entity
     * @param showInTrace whether the condition should appear in the trace
     * @param objects Collection of additional objects (only needed for super constructor)
     */
    public StopCondition(SimulationProblem model, String name, boolean showInTrace, Object... objects) {
        super(model, name, showInTrace, objects);

        unfinishedJobs = new ArrayList<>(model.getJobs());
    }

    /*
     * TODO implement custom Debug stop condition to stop with a fixed queue configuration!!
     */

    /**
     * Checks if the simulation run is finished and should be stopped. This is the case if all jobs of the model are finished.
     *
     * @return true if all jobs are finished
     */
    @Override
    public boolean check() {
        List<SimulationJob> finishedJobs = new ArrayList<>();
        for (SimulationJob job : unfinishedJobs) {
            if (job.getStatus() == SimulationJob.Status.FINISHED) {
                finishedJobs.add(job);
            }
        }
        unfinishedJobs.removeAll(finishedJobs);

        return unfinishedJobs.isEmpty();
    }
}
