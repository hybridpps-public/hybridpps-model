# HybridPPS

This is the documentation of the [model implementation in HybridPPS](https://git-st.inf.tu-dresden.de/hybridpps/hybridpps-model).
It is based on [Reference Attribute Grammars](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.108.8792&rep=rep1&type=pdf) 
and [Relational Reference Attribute Grammars](https://doi.org/10.1016/j.cola.2019.100940), and built with [JastAdd](http://jastadd.org/) and [Gradle](https://gradle.org/).
