# Extension-Process

![](img/model-generation.png)

## Normal KolischInstances

Specification of a project, its jobs and resources is specified in a KolischInstance.
Combining multiple of them yields a first problem model.

The files are either reused from disk, or generated using the problem generator "ProGen".

## Extended KolischInstances

For *HybridPPS* our problem model is slightly more difficult:

- Jobs can have modes, each having a different duration and resource requirements.
- Jobs additionally have a type representative requiring a certain setup time. In our case, this time is chosen randomly based on normal distribution.
- Job have a stochastically distributed duration, which are persisted using scenarios, see below.
- Resources have stochastically distributed capacity, which are persisted using scenarios, see below.

### Scenarios

To achieve reproducible solutions, distributions must be made explicit.
In the extended format, a fixed and sufficiently large number (e.g., 1000) of values following this distribution are provided for each stochastic value.

## The class `ExtensionProcess`

The process is set up and runs as follows:

```java
Path p = Paths.get("src", "main", "resources", handmade.json);
ExtensionProcess process = new ExtensionProcess();
process.setPathToInputJson(p);
// optional: output base directory. default: "models"
process.setOutputDirectory("myModels");
// optional: output meta filename. default: "meta.json"
process.setMetaFileName("myMeta.json");
process.run();
```

Internally, the process consists of two steps: extending the base problem, and printing the new, extended problem.

### Extending the base problem

Main method is `addExtensions()`. Here, the [input JSON file][extension-json-description] is read into the appropriate data structures, the Kolisch instances of all referenced projects are parsed, and this information are combined.

More specifically, the following things happen:

- read the input JSON file (referred to as "input")
- create a new problem model (`ProductionSystem`) and initialize some of its values from the input
- create type representatives if specified in the input
- iterate over the project extensions, i.e.
    - parse the referenced Kolisch instance
    - include the parsed information of the project into the created problem model
    - set objective and apply extensions for job/resource at the project
- perform sanity checks, e.g., if there are at least one job and one resource defined

Resources are handled as follows:

- A global resource G replaces a local resource L (in some project P) if
  - G and L have the same name
  - G is defined in the [input JSON file][extension-json-description] as a global resource
- The capacity of a global resource is the maximum capacity among the capacities of all project resource (with the same name).

### Printing the new, extended problem

Main method is `createExtendedKolischInstances`. With the extended problem model, the following directory/file structure will be created (whether the timestamp is added, can be configured):

```
${outputDirectory}/
 |
 |-- ${model.name}-${timestamp}/
 |    |
 |    |-- ${metaFile}
 |    |-- ${project0.name}.emm
 |    |       ...
 |    |-- ${projectN.name}.emm
```

To create the `*.emm` file for each project, the method `Project.extendedPrint()` is used, which is defined in [Printing.jrag][printing.jrag].
The format of such a file is similar to a Kolisch instance and comprises the following sections (differences highlighted):

- file info section (seed, version)
- project info section (job count, resource count) (**new in extended:** global resources are not listed)
- project metrics (properties like rel.date, horizon)
- job precedence relations (**new in extended:** new column to specify type representative for each job)
- resource requests (**new in extended:** column for mode duration removed, resource column names without space)
- **new in extended:** stochastic job durations, i.e., for each job/mode the deterministic value and *n* unrolled values of its stochastical distribution
- **new in extended:** stochastic resource availabilities, i.e., in addition to normal resource availabilities the *n* unrolled values of its stochastical distribution, one row per resource

## Important attributes

Attributes separated by their defining file/aspect.

### Aspect: Navigation

- Several attributes to resolve the main types (job, mode, resource, type representatives). They return `null` (and print to stderr) if no result was found. Signature is always `resolve${TypeName}(String name)`
- Several attributes to navigate upwards in the tree, e.g., `containingProject` defined for Job and ResourceRequirement

### Aspect: Computing

- `Project.criticalPathMaxModi` is a rather complex attribute to compute the critical path for a project using a simple forward scheduling strategy ignoring resource capacities and maximizing resource requests. The mode for each job is selected based on the given parameter (a resource). If the no-arg attribute is used, the mode with the highest sum of resource requests is used. If a resource is given, the mode with the highest resource request for the given resource is used. The return value is a `Solution` with mappings of the special type `CriticalPathMapping`. This attribute used the helper attribute `Job.maxResourceMode` (also with either no parameters or one resource parameter) to select the mode for a job
- `Solution.lengthCriticalPath` determines the length of the critical path. This is only valid, if the solution is obtained from `Project.criticalPathMaxModi` as it uses `Mapping.isOnCriticalPath` which only is defined in a meaningful way for `CriticalPathMapping` used by the former
- `Resource.minimumCapacity` determines the minimum capacity of one resource based on the maximum of a) the single highest request value for this resource, and b) the average resource demand (sum of the resource requests divided by the length of the critical path)

### Aspect: Unrolling

- `Value.unrolledNumbers` computes a NTA containing computed values for a stochastically distributed values, or the same value of deterministic values. If used for resource capacity, each value adheres to the [minimum capacity](#aspect-computing)
- `Project.allResources` returns a list of resources (both global and project-local) used in this project

### Aspect: Validation

- `Solution.validate(boolean failAtFirstError)` checks, whether a solution is valid, i.e., if the following assumptions are true:
    - precendence of jobs is obeyed to
    - all jobs have a start time, i.e., there is no job without a mapping
    - resource are not overbooked, i.e., their capacity is never lower than the sum of requests at a given time

### Aspect: Printing

- `*.prettyPrint` (defined on most types, but intended to be used only on `ProductionSystem` and `Solution`) prints all information in a human-readable way
- `Project.extendedPrint` is described [above](#printing-the-new-extended-problem)

[printing.jrag]: https://git-st.inf.tu-dresden.de/hybridpps/hybridpps-model/-/blob/master/hybridpps.base/src/main/jastadd/Printing.jrag
[extension-json-description]: /extension-json-description
