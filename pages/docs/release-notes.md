# Release Notes


# Version 0.4.0

## What's New

1. Problem descriptions with support for multiple Objectives
* Refined Objective and ObjectiveFunction definitions
* Modified solution format to represent multiple Objectives
* Objective-calculation as part of the JRAG definitions

## Gradle Setup

Gradle version and wrapper were not modified. The run-configuration was not target for changes.

## Changes in Detail

### Multi-Objective Problems

The problem description format was updated to support arrays of objective specifications:

```JSON
"system_default": {
    "objective": [{"type": "project delay", "function": "average"}],
    "cv": 0.2,
    "default_job_extension": {"tr": "TR1", "mode": {"duration": {"type": "normal"}}}
  }
```

```json
"project_default": {
    "objective": [
      {"type": "project delay", "function": "none"},
      {"type": "makespan", "function": "none"}
    ],
    "cv": 0.3,
    "default_job_extension": {"tr": "TR2"}
  }
```

As seen in the example specifications, it is now possible to specify multiple objectives.
**This is a breaking change and old problem descriptions do not work with this version of the simulator. However, old problem-descriptions only need to wrap their objective in an array.**

#### Resolving Objectives

Resolving and processing objectives from the problem-descriptions is implemented via the following steps:

1. Extend each project by its *project_default*. If no objective is provided by the *project_default* (empty array or no array), the *system_default* is used to extend the project.
2. The simulation is executed. This step is objective-independent.
3. The solution file is generated (see Solution Format). Therefore, the objectives for each project are calculated as specified. A project can have duplicate objectives with different objective-functions. For example median makespan and deviation makespan are possible at the same time.

The supported Objectives are:
* project delay
* makespan

The supported ObjectiveFunctions are:
* average (mean)
* median
* standard deviation
* span
* none

### Solution Format

The solution format (solution.json) was changed in large parts to support multi-objective outputs.

```JSON
[ {
  "problem" : "5P50J2M2R",
  "strategies" : [ {
    "strategy" : "metarule",
    "total_makespan" : [ 74, 83, 79, 71],
    "projects" : {
      "0" : {
        "objective_result" : {
          "project_delay_none" : {
            "function" : "none",
            "values" : [ 22, 35, 31, 12]
          },
          "makespan_none" : {
            "function" : "none",
            "values" : [ 70, 83, 79, 60]
          }
        }
      },
      "1" : {
        "objective_result" : {
          "makespan_average" : {
            "function" : "average",
            "values" : [ 68 ]
          },
          "makespan_deviation" : {
            "function" : "deviation",
            "values" : [ 5 ]
          }
        }
      }
    }
  }]
}]
```

The output contains for each problem:

* problem name
* strategy runs (typically only one strategy per run)
   * strategy name
   * array of total_makespan (always part of the output for verification)
   * project-wise results
      * key is project name
      * objective_result contains results for every objective
         * "objective"_"function" is key
         * if functions is not none, values are accumulated. If none, all values are provided in case of a stochastic run. In case of a deterministic run, the function is ignored and the only value is reported.


# Version 0.3.0

## What's New

1. **MetaRuleScheduling** to support hyper-heuristics, which comes especially with:
* A MetaRule JSON specification
* A MetaRule attribute specification
* Rule-engine/background implementation for MetaRuleScheduling based on simple and combined priority rules.

## Gradle Setup

The current gradle version and wrapper were not modified but are tested to work also with the newer versions 6.9+

## Changes in Detail

### General Information

This version enables the MetaRuleScheduling strategy which extends the exiting simple and combined priority rules. Until now, it must be specified which strategy is used at simulation start. This strategy is then used for the complete simulation. Changing the strategy based on different attributes or changing the parameters of a CPR during the simulation was not possible.

This functionality is enabled by the MetaRuleScheduling. Here, a ruleset is provided at simulation start which contains logical rules based on runtime parameters of the simulation. Depending on those parameters, the ruleset decides which other strategy is used at which iteration of the simulation. Those can be simple priority strategies (fifo, lifo, random...) or a CPR with a specified set of parameters.


### MetaRule Specification

Meta-rulesets are specified using a JSON format. For this format, no explicit grammar is defined by given by the Jackson parser in code. Structurally wrong metarules throw parser-errors and invalid rules are detected during a preprocessing step at simulation start. However it is still possible to create correct, valid but nonesensical rules the client must take care of themself.

#### Schema

```json
{
  "statements": [
    {
      "clauses": [
        {
          "index": "STRING",
          "left": "ATTRIBUTE_DESCRIPTOR",
          "clauseComparator": "[GT|LT|EQ|LE|GE|NE]",
          "right": "DOUBLE (raw)"
        }
      ],
      "conditions": [
        {
          "index": "STRING",
          "operator": "[AND|OR|XOR]",
          "operands": ["[CLAUSE_INDEX|CONDITION_INDEX]"],
          "yields": "BOOLEAN (raw)"
        }
      ],
      "index": "STRING",
      "gate": "[CLAUSE_INDEX|CONDITION_INDEX]",
      "onTrue": {
        "next": "[RULE_DESCRIPTOR|STATEMENT_INDEX]",
        "params": ["STRING"]
      },
      "onFalse": {
        "next": "[RULE_DESCRIPTOR|STATEMENT_INDEX]",
        "params": ["STRING"]
      }
    }
  ]
}
```

##### Statements

A Statement represents a list of Clauses, a list of Conditions which defines logical terms to be evaluated.
Evaluating a Statement starts by fetching the gate attribute. If the gate is a Clause, it is checked if the simple
expression of the Clause is true. If the gate is a Condition then it constructs the more complex logical expression
recursively which es evaluated. The index of a Statement must be unique.
The Statement must always define the following actions onTrue and onFalse which are executed depending on the result
of evaluating the gate.

* **index** is an arbitrary but unique string. One statement must have the index "root" which is evaluated first independent of the order of statements.
* **gate** is the element which is evaluated as root to be true or false
* **onTrue** defines the behavior if the gate element is evaluated true. See Follower.
* **onFalse** defines the behavior of the gate element is evaluated false See Follower.

###### Follower

A Follower holds information how the rule-engine must proceed depending on the outcome of the current Statement.
The "next" attribute must be a string in the format "[statement|rule]:value". The allowed statements are all statements
by their index defined in the ruleset. Allowed rules are all simple priority rules provided by the value of their
toString() method. In those cases, the params array remains empty but must be defined.

```json
"onTrue": {
  "next": "rule:fifo",
  "params": []
},
"onFalse": {
  "next": "statement:s2",
  "params": []
}
```

If a combined priority is used as a following rule, the params array contain the complete CPR model as one string with
escaped syntax.

```json
"onTrue": {
  "next": "rule:cpr",
  "params": ["{\"duration\": \"...\"}"]
}
```

##### Clauses

Part of a Statement.
The Clause contains a logical operation with a left side attribute, a comparator and a right side value.

```JSON
"clauses": [
   {
     "index": "c0",
     "left": "number_executable_modes",
     "clauseComparator": "GT",
     "right": 100
   },
]
```

The "left" attribute can either be a "queryable_global_attribute" or a "accumulator:queryable_mode_attribute".

> See the later Global Attributes section for more information.

A clause must be defined by an unique String index, a left side GlobalAttribute or CombinedAttribute,
a Comparator (LE, GE, LT, GT, EQ, NE) and a right side double value to compare with.

##### Conditions

The Condition is part of a Statement in the meta-rule definition.
A Condition describes a logical term. Operands of such a term can be Clauses or other Conditions. Note that this
can be used to model nested terms.
The Operands are specified by their index, the operator is specified by its name (AND, OR, XOR). The yields
attribute is set to the boolean value which the result of the term is compared with.
The array of operands can have any size, but is not allowed to be empty.

```json
{
   "clauses": [
         {
           "index": "c0",
           "left": "number_executable_modes",
           "clauseComparator": "GT",
           "right": 100
         },
         {
           "index": "c1",
           "left": "number_executable_modes",
           "clauseComparator": "LT",
           "right": 200
         },
         {
           "index": "c2",
           "left": "number_executable_modes",
           "clauseComparator": "EQ",
           "right": 42
         }
       ],
   "conditions": [
         {
           "index": "a",
           "operator": "AND",
           "operands": ["c0", "c1"],
           "yields": true
         },
         {
            "index": "b",
            "operator": "XOR",
            "operands": ["a", "c2", "c1"],
            "yields": false
         }
       ],
 }
 ```



### Global Attributes

MetaRules require so called global-attributes to make decisions. Until now, only mode-attributes were implemented.
The present mode-rules are listed below with their corresponding string representation (DirectSuccessorCount = "direct_successor_count"):
* DirectSuccessorCount
* DirectSuccessorSumMaxProcessingTime
* DirectSuccessorSumMinProcessingTime
* Duration
* ProjectFinishTime
* ProjectStartTime
* Resources
* SetupNeeded
* SetupTime
* TotalSuccessorCount
* TotalSuccessorSumMaxProcessingTime
* TotalSuccessorSumMinProcessingTime
* WaitingJobCount
* WaitingTime

Starting from those mode-attributes we can define global attributes as follows:

#### CombinedAttributes

Each simulation iteration defines a queue of executables modes. For each executable mode, the system can query a mode-attribute.
This defines a mapping (mode -> {attributes}). However, because modes are grouped by their jobs (only one mode is actually executed to complete one job), the modes must be grouped: (job -> {(mode -> {attribute})}). This three-dimensional mapping can then be broken down by averaging the attributes of one job, which removes the mode dimension: (job -> {avg(attribute)}).
In consequence, the queue of executable modes can be transformed to a list of job-attributes.

From this list of double-values, different accumulations can be calculated:

* sum
* mean
* median
* span_length
* standard_deviation

We can now define CombinedAttributes as a pair of accumulator and mode-attribute:

```
COMBINED_GLOBAL_ATTRIBUTE = "accumulator:mode-attribute"
```

z.b. ``"sum:direct_successor_count"``

#### QueryableGlobalAttributes

QueryableGlobalAttributes do not use an accumulator but depend directly on the the configuration of the queue and the simulation state:

* number_executable_modes: queue size
* number_exectuable_jobs: number of jobs with at least one executable mode
* number_executable_projects: number of projects with at least one executable job
* foreach_setup_needed: scalar, if all modes in the queue require a setup (obligatory setup required)

### Realisation

> This section does only provide a brief overview. Details can be found in the JavaDoc documentation and sourcecode.

The metarule specification is realized by a direct java mapping with jackson which is implemented in ``simulation/model/priority/input``.

Those classes which have the exact namings of the above specification implement the ``RuleCreator`` interface. By doing so, they recursively implement a method to produce a string serialization of the modelled rule.

The scheduling strategy itself is called ``MetaRuleScheduling`` which can be used like any other strategy but must receive the path to a ruleset specification. During construction, the ruleset is parsed and each statement is transformed to a internal serialization. The ``RuleCreator`` then initializes a rule-engine session for each statement. We use the EVRETE rule-engine. Those sessions are stored in a map.

On each simulation iteration, the ``SimulationState`` is updated which all required information like the problem and the current queue of executable modes. Afterwards the session of the root statement is fired which lets the rule-engine reason over the SimulationState. Afterwards, a result is written in the ``DynamicConfiguration``. Depending on the Followers defined in the Statement, either the decision process terminates with a known Strategy to select the next mode or the next Statement is taken and its session is fired.

#### MainClass Modifications

The cmd programm got a new option to specify metarule scheduling and a corresponding json file:

```Java
@Option(names = {"-m", "--metarules"}, description = "define the path to a JSON file containing a meta-ruleset. If given, the use of the MetaRuleSchedulingStrategy is assumed.")
private String metarules;
```

An exemplary run configuration provided in the project is:

```
gradle hybridpps.starter:run --args="C:\...\hybridpps-model\hybridpps.starter\src\main\resources\5P50J2M2R.json -m C:\...\hybridpps-model\hybridpps.starter\src\main\resources\alwaysFIFO.json"
```

#### Testsetup and Debugging: TraceableStrategyContainer

For testing and debugging it is required to analyze and use the state of the experiment at a given iteration point. For example the queue of executable modes after four execution points should be analyzed.
To enable this functionality apart from logs which need to be read manually, the ``TraceableStrategyContainer`` was implemented.

The TraceableStrategyContainer is a decorator class to serve for debugging and testing of SchedulingStrategies.
This class takes a SchedulingStrategy as an argument but implements the SchedulingStrategy interface itself.
Internally, all getNextMode calls are redirected to the provided SchedulingStrategy. Additionally this class takes
a copy of each iteration state of executable modes (the execution queue).

After an experiment run, this class contains all snapshots of the queue throughout the simulation and the SimulationProblem itself.
In consequence, this class can be used if the behavior of a SchedulingStrategy or an experiment run should be traced in code.
**Apart from test-code and debugging, this class must not be used in production code because it my introduce a memory leak for
large problem definitions.**

## Following Work

* The MetaRuleScheduling is not yet optimized in performance aspects. Runtime especially in stochastic runs needs to be benchmarked and compared for scaling problem definitions.
* The integration of GlobalAttributes is not yet tested sufficiently.
* The reconfiguration and pre-loading of CPR rules needs to be considered.
* Different parallel report strategies should be taken into account.

---

# Version 0.2.0

## What's New

The new hybridpps simulation release follows the latest implementations by Erik Schönherr. In this work, multiple major updates took place:

1. StopConditions
2. TypeRepresentatives in Resource Management
3. Deterministic and Stochastic RCPSP Simulations
4. Scheduling Priority Rules
* Based on Weighted Attributes
* Attribute Configurations

## Gradle Setup

The current gradle version and wrapper were not modified but are tested to work also with the newer versions 6.9+

## Changes in Detail

### 1. Stop Conditions

The simulator does no longer terminate after a fixed amount of time but after a certain implemented condition is reached. Therefore the new ``StopCondition`` class with the ``check`` method has to be refined. In the default implementation, the simulation stops after all input jobs are completed i.e. the pending job queues are empty.

### 2. TypeRepresentatives in Resource Management

A new feature is the optional support of type respresentatives which can bes epcified in the extension JSON instance. As shown below different type representatives can be named and given a ``setupMin`` and ``setupMax`` time as positive integers.

```json
"type_representatives": [
    {"name": "TR1", "setupMin": 1, "setupMax": 1},
    {"name": "TR2", "setupMin": 5, "setupMax": 5}
]
```

Also in the extension file it can be specified which type representative a certain job requires. Here a simplification took place that a job can only have one TR which is the applied to all resources it uses.

```json
"projects": [
    {
      "project": "0",
      "job_extensions": [
        {"job": "2", "tr": "TR1"},
        {"job": "3", "tr": "TR2"}
      ]
    }
  ]
```

The TR assignment happens as follows:
1. Determine all required TRs and all currently available resources and their current TRs
2. If there are enough free resources which already have the required TR, the job uses them without setup time.
3. If 2. is not present:
    * Take as many TR-free resources as available and assign them the new TR (setup time required)
    * If resources are still missing, take resources with an other current TR and reassign them the new TR (setup time required, but no more then already in 3.1)

# 3. Deterministic and Stochastic RCPSP Simulations

To support deterministic and stochastic problem instances, the old ``RCPSP`` class was elevated to be now the abstract ``SimulationProblem`` class. This ``SimulationProblem`` class implements all functionality to initialize start and report model operation, it also extends DESMO-J ``Model``. To reduce class-size the always bidirectional associated class ``EventHandler`` is responsible for the functionality which handles dynamic model changes during an experiment (job and queue handling).

The two new classes ``RCPSP`` ( [Deterministic] Resource Constrained Project Scheduling Problem) and ``SRCPSP`` (Stochastic Resource Constrained Project Scheduling Problem) now extend ``SimulationProblem`` and enable both deterministic and stochastic simulations.

* Deterministic experiments work as in the old version, everything is interpreted as defined in the model instance. The experiment runs once.
* Stochastic experiments have an additional extension specification (see below) to vary specified model parameters following a distribution (``normal`` only for now). The experiment runs a defined amount of times and produces a respective amount of result files in a subfolder of the output directory.

The ``unrolling`` attribute defines a ``count`` of generated values and a ``precision`` for numeric generation.

```json
"unrolling": {"count": 10, "precision": 2},
```

The concrete job attributes which should be varied are specified in the ``job_extensions``. In the following example, the ``duration`` value of mode 1 in job 2 is normal-distributed with a coefficient of derivation (cv) of 0.2
```json
"job_extensions": [{
    "job": "2",
    "tr": "TR1",
    "modes":  [{
        "mode":  "1",
        "duration":  {
            "type":  "normal", "cv":  0.2
        }
    }]
}]
```

### 4. Scheduling Priority Rules

The update implements different strategies to prioritize scheduled jobs and modes i.e. determining the next candidate to be executed.
The implemented interface supports custom strategies as well as trivial default implementations and attribute-based strategies (see 4.1 Weighted Attributes).
The implemented simple strategies are:
* Random (chooses randomly one of the other strategies)
* FIFO
* LIFO
* SPT (shortest processing time)
* MWKR (most work remaining)

> Note that scheduling strategies can not be specified in the model instances or extensions but must be defined in code.

#### 4.1. Weighted Attributes

The additional CPR (combined priority rule) scheduling strategy uses different pre-implemented weighted attributes. The following attributes are implemented:
* DirectSuccessorCount
* DirectSuccessorSumMaxProcessingTime
* DirectSuccessorSumMinProcessingTime
* Duration
* ProjectFinishTime
* ProjectStartTime
* Resources
* SetupNeeded
* SetupTime
* TotalSuccessorCount
* TotalSuccessorSumMaxProcessingTime
* TotalSuccessorSumMinProcessingTime
* WaitingJobCount
* WaitingTime


Detailed attribute information are available in the Erik Schönherr's bachelor thesis which is part of the project documentation.

#### 4.2. Attribute Configurations - Combined Priority Rule

The combined priority rule implementation allows to use those weighted attributes to calculate the job executed next. However this is also not yet supported directly by the extension configuration but must be specified manually in code:

```java
WeightedAttributes weightedAttributes = new WeightedAttributes();
weightedAttributes.put("direct_successor_count", 0.5);
weightedAttributes.put("duration", -0.2);
weightedAttributes.put("setup_needed", -2.0);

DeterministicSolution solution = runDeterministicTest("[extension].json",
  new CombinedPriorityRule(weightedAttributes));
```

Alternatively, the implementation provides a ``CPRLoader`` class which can parse json files as shown below to simplify the setup process.
```JSON
[
    {
      "name": "duration",
      "value": -0.2
    },
    {
      "name": "setup_needed",
      "value": -2.0
    },
    {
      "name": "direct_successor_count",
      "value": 0.5
    }
]
```
