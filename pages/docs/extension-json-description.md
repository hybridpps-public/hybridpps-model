# JSON documentation for the extension process

An example for input JSON file for the [extension-process][extension-process] looks as follows.

```json
{
  "name": "handmade",
  "seed": 0,
  "unrolling": {"count": 100, "precision": 2},
  "type-representatives": [
    {"name": "TR1", "setupMin": 1, "setupMax": 4},
    {"name": "TR2", "setupMin": 2, "setupMax": 2}
  ],
  "system-default": {
    "objective": {"type": "project delay", "function": "average"},
    "cv": 0.2,
    "default-job-extension": {"tr": "TR1", "mode": {"duration": {"type": "normal"}}}
  },
  "projects": [
    {
      "project": "0",
      "filename": "handmade.mm",
      "project-default": {
        "objective": {"type": "project delay", "function": "mean"},
        "cv": 0.3,
        "default-job-extension": {"tr": "TR2"}
      },
      "job-extensions": [
        {"job": "2", "tr": "TR2", "modes": [{"mode": "1", "duration": {"type": "normal", "cv": 0.4}}]},
        {"job": "4", "tr": "TR1"}
      ],
      "resource-extensions": [
        {"resource": "R1", "capacity": {"type": "normal", "cv": 0.2}}
      ]
    },
    {
      "project": "1",
      "filename": "handmade.mm"
    }
  ],
  "global-resources": [
    {"resource": "R2", "capacity": {"type": "normal"}}
  ]
}
```

The basic idea is to specify only the information needed to extend a given problem model, and to use default where applicable.

A list of available elements is as follows. If the default value is *none*, then this element is required:

|        Location        |                             Description                              |                     Type                    | Default |
|------------------------|----------------------------------------------------------------------|---------------------------------------------|---------|
| `name`                 | Name of the problem model                                            | String                                      | `PS`    |
| `seed`                 | Initial seed to use for random numbers                               | long                                        | `0`     |
| `unrolling.count`      | Number of calculated scenarios for stochastically distributed values | int                                         | 10      |
| `unrolling.precision`  | Number of decimal places for all float numbers                       | int                                         | 2       |
| `type-representatives` | List of type representatives                                         | [`TypeRepresentative`](#typerepresentative) | *empty* |
| `system-default`       | Default values for the whole problem model                           | [`DefaultValues`](#defaultvalues)           | *empty* |
| `projects`             | List of projects (required to be present)                            | [`ProjectExtension`](#projectextension)     | *none*  |
| `global-resources`     | List of global resources                                             | [`ResourceExtension`](#resourceextension)   | *empty* |

## TypeRepresentative

See DataTypeRepresentative class.

|    Name    |         Description         |  Type  | Default |
|------------|-----------------------------|--------|---------|
| `name`     | Name of type representative | String | *none*  |
| `setupMin` | Minimum of setup time       | int    | *none*  |
| `setupMax` | Maximum of setup time       | int    | *none*  |

## Value

See DataValue, DataConcreteValue, DataNormallyDistributed classes.

|   Name   |                    Description                    |  Type  | Default |
|----------|---------------------------------------------------|--------|---------|
| `type`   | One of ["fixed", "normal"]                        | String | *none*  |
| `number` | If type is "fixed", the deterministic value       | int    | *none*  |
| `cv`     | If type is "normal", the coefficient of variation | double | See [Handling of default values](#handling-of-default-values)  |

## DefaultValues

See DataDefaultValues, DataObjective, DataDefaultJobExtension, DataModeExtension classes.
See also [Handling of default values](#handling-of-default-values)

|                  Name                 |                               Description                                |        Type       |                            Default                            |
|---------------------------------------|--------------------------------------------------------------------------|-------------------|---------------------------------------------------------------|
| `objective.type`                      | One of ["project delay", "cycle time"]                                   | String            | *none*                                                        |
| `objective.function`                  | One of ["average", "deviation", "mean", "span", "average and deviation"] | String            | *none*                                                        |
| `cv`                                  | Coefficient of variation, used to determine standard deviation           | double            | See [Handling of default values](#handling-of-default-values) |
| `default-job-extension.tr`            | Default type representative for all job extensions                       | String            | *none*                                                        |
| `default-job-extension.mode.duration` | Default duration of modes for all job extensions                         | [`Value`](#value) | See [Handling of default values](#handling-of-default-values) |

## ProjectExtension

See DataProjectExtension, DataJobExtension, DataModeExtension, DataResourceExtension classes and [DefaultValues](#defaultvalues).

|                  Name                 |                                                       Description                                                        |                    Type                   |                            Default                            |
|---------------------------------------|--------------------------------------------------------------------------------------------------------------------------|-------------------------------------------|---------------------------------------------------------------|
| `project`                             | Name of the project                                                                                                      | String                                    | *none*                                                        |
| `filename`                            | Most important parameter, the file containing the KolischInstance, relative paths use the directory of this JSON as base | String                                    | *none*                                                        |
| `project-default`                     | Default values for the project                                                                                           | [`DefaultValues`](#defaultvalues)         | *empty*                                                       |
| `job-extensions`                      | List of job extensions                                                                                                   | See below                                 | *empty*                                                       |
| `job-extensions[i].job`               | Name/ID of the referenced job (must exist in the KolischInstance)                                                        | String                                    | *none*                                                        |
| `job-extensions[i].tr`                | Name/ID of a type representative defined before                                                                          | String                                    | *none*                                                        |
| `job-extensions[i].modes`             | List of mode extensions                                                                                                  | See below                                 | *empty*                                                       |
| `job-extensions[i].modes[j].mode`     | Name/ID of the referenced mode (must exist in the KolischInstance)                                                       | String                                    | *none*                                                        |
| `job-extensions[i].modes[j].duration` | Duration of the referenced mode                                                                                          | [`Value`](#value)                         | See [Handling of default values](#handling-of-default-values) |
| `resource-extensions`                 | List of resources local for this project                                                                                 | [`ResourceExtension`](#resourceextension) | *empty*                                                       |

## ResourceExtension

See DataResourceExtension class and [`Value`](#value)

|    Name    |             Description             |        Type       |                            Default                            |
|------------|-------------------------------------|-------------------|---------------------------------------------------------------|
| `resource` | Name/ID of the referenced resource  | String            | *none*                                                        |
| `capacity` | Capacity of the referenced resource | [`Value`](#value) | See [Handling of default values](#handling-of-default-values) |

## Handling of default values

As a general rule, the most specific value takes always precedence.
That means, if for a concrete mode, the duration is set, then this value is taken. Otherwise, the default job extension of the project is used (if specified for this project). If the project default is not specified, the system default will be used. If none of these three values are specified, an error is raised for this mode.

For the value of `cv`, a similar process is used (first local, then project, lastly system). However, here the mode itself can also specify this value. This results in the following (hopefully intuitive) order of precedence:

1. `root.projects.project1.job1.mode1.duration.cv`
1. `root.projects.project1.project-default.default-project-extension.mode.duration.cv`
1. `root.projects.project1.project-default.cv`
1. `root.system-default.default-job-extension.mode.duration.cv`
1. `root.system-default.cv`

[extension-process]: /ExtensionProcess
