# Grammar Description

As of writing this, the problem grammar is defined as follows.
The current versions of the grammar for [problem][problem.relast] and [solution][solution.relast] are in the models repository.
Please refer to the [JastAdd documentation][jastadd-manual] and its [relational extension](runtime-rags-journal) concerning the syntax of the grammar.

```java
// General
abstract Nameable ::= <Name:String> ;
ProductionSystem : Nameable ::= <Seed:long> <ScenarioSize:int> <RoundingPrecision:int>
                                <Horizon:int> Project* Resource* TypeRepresentative* ;
Project : Nameable ::= Job* Objective Resource*
                       <Horizon:int> <EarliestStartTime:int> <LatestCompletionTime:int> ;
Objective ::= <Function:ObjectiveFunction> <Type:ObjectiveType> ;

// Jobs
Job : Nameable ::= Mode* ;
rel Job.Successor* <-> Job.Predecessor* ;
rel Job.TypeRepresentative? -> TypeRepresentative ;

Mode : Nameable ::= Duration:Value ResourceRequirement* ;

abstract Value ;
ConcreteValue : Value ::= <Number:int> ;
abstract StochasticValue : Value ::= DeterministicValue:ConcreteValue ;
NormallyDistributedValue : StochasticValue ::= <Mean:double> <StandardDeviation:double> ;

UnrolledNumbers ::= <Numbers:List<Double>> ;  // type of NTAs for StochasticValue

ResourceRequirement ::= <RequiredCapacity:int> ;
rel ResourceRequirement.RequiredResourceGroup -> ResourceGroup ;

// Resources
ResourceGroup : Nameable ::= Capacity:Value Resource* ;

// Type Representative
TypeRepresentative : Nameable ::= <SetupTime:int> ;
```

## Overview

Note that in general, times, resource requests and resource capacities are specified using the type `int` (if they are deterministic values).

The problem model has a root named `ProductionSystem` comprising (besides some settings later discussed) a list of projects to schedule and a list of global resources.

A `Project` defines its objective (a function to optimize), a list of jobs and a list of resources local to this project. Furthermore, optimization parameters like its horizon, earliest starting time and latest completion time are available.

A `Job` is the entity to be scheduled. In a single-mode setting, all properties of the one mode are basically those of the job, i.e., duration and resource requests. In a multi-mode setting, the solution includes the selection of one mode per job.
Additionally, a job has successors (other jobs), and therefore also predecessors.

A `Mode` defines the actual duration and resource requests of a job. Those resource request (or resource requirements) target one resource and require a given amount of it.

A `ResourceGroup` is an overarching entity comprising resource instances `Resource` (see [Solution grammar](Solution grammar)). It can be either global 
(if defined on the production system with a non-zero capacity) or project-local 
(if defined on the project and with no global resource having the same name). 
Furthermore, it has a certain capacity (a number of resource instances) available in each time slot.



## Special settings for the extension process

During the [extension-process][extension-process], stochastic distributions and type representatives are introduced.

Therefore, both duration of a mode and capacity of a resource have the abstract type `Value`. This can either be deterministic with a concrete number, or stochastically distributed. Currently only normal distribution are supported having mean and standard derivation as defining parameters.

A `TypeRepresentative` adds a given amount of "setup time" to a job. This could be due to preparation needed for a kind of job or setting up a resource before the job can start.

During the storage of the extended problem model, the stochastically distributed values are being "unrolled", i.e., actual numbers are calculated based on their distribution to ensure reproducability. The value `ScenarioSize` defined on a production system determines how many values are being calculated.
Furthermore, the `RoundingPrecision` defines the number of decimal places of those calculated numbers. If set to `0`, only integer numbers will be used during printing.

## Solution grammar

The grammar to specify a solution is the following.

```java
abstract AbstractSolution ;

DeterministicSolution: AbstractSolution ::= ConcreteSolution ;
StochasticSolution : AbstractSolution ::= ConcreteSolution* ;

ConcreteSolution ::= Mapping* ;
rel ConcreteSolution.ProductionSystem -> ProductionSystem ;

Mapping ::= <StartTime:int> ResourceUsage*;
rel Mapping.Mode -> Mode ;

ResourceUsage ;
rel ResourceUsage.ResourceGroup -> ResourceGroup ;
rel ResourceUsage.Resource* -> Resource ;

Resource ::= <Index:int> ;

CriticalPathMapping : Mapping ::= <EarliestFinish:int > ;
```

`ConcreteSolution` is the main entity describing a solution to an (s)RCPSP. It contains a list of mappings, 
where each `Mapping` specifies the start time and has a relation to the mode it defines this start time for.

Two variants of the solution are available: `DeterministicSolution` and `StochasticSolution`. The former contains a 
single `ConcreteSolution` to RCPSP, while the latter consists of multiple solutions for different stochastic values.

`Resource` represents a single resource entity, comprising the `ResourceGroup`. Resources within the `ResourceGroup` are 
recognized using their indices.

A special mapping - the `CriticalPathMapping` - is created, when calculating the critical path and it contains additionally the earliest finishing time needed for the [critical path calculation][ciritical-path-calculation].


[problem.relast]: https://git-st.inf.tu-dresden.de/hybridpps/hybridpps-model/-/blob/master/hybridpps.base/src/main/jastadd/problem.relast
[solution.relast]: https://git-st.inf.tu-dresden.de/hybridpps/hybridpps-model/-/blob/master/hybridpps.base/src/main/jastadd/solution.relast
[extension-process]: /ExtensionProcess
[critical-path-calculation]: /ExtensionProcess#aspect-computing
[jastadd-manual]: http://jastadd.org/web/documentation/reference-manual.php
[runtime-rags-journal]: https://doi.org/10.1016/j.cola.2019.100940
