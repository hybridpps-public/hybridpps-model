# Hybrid-PPS Initial Model Implementation

This repository contains the implementation of the initial model for the project HybridPPS using [*Relational RAGs*](https://git-st.inf.tu-dresden.de/jastadd/relast).

For project documentation, please see the [link](https://hybridpps.pages.st.inf.tu-dresden.de/hybridpps-model/).

For basic information on relational RAGs, please read
- [Mey et. al., "Continuous model validation using reference attribute grammars", 2018](https://doi.org/10.1145/3276604.3276616)

## Running the example with gradle

:warning: Unfortunately, the gradle build system does not have much backwards-compatibility. Therefore, a suitable version of gradle is included that can be run in the main directory with

- using Windows `gradlew.bat <task>`
- using Mac/Linux `./gradlew <task>`

This gradle project uses subprojects, the which themselves contain a number of executable tasks

- `hybridpps.base`
    - this subproject contains the model and some analysis
- `hybridpps.extend`
    - this subproject defines the structure of the input JSON format
- `hybridpps.starter`
    - this subproject contains the code to run the main application, i.e., reading an extension specification JSON and related input problem definitions to finally output an extended problem definition
    - to run the program in simulation mode, run `./gradlew :hybridpps.starter:run [--args="$INPUT_JSON_PATH"]`
    - to run the program in extension mode, run:`./gradlew :hybridpps.starter:run [--args="$INPUT_JSON_PATH -o $OUTPUT_DIR --extend"]`
